<?php

class CAFSharingLinksWidget extends WP_Widget {

	function CAFSharingLinksWidget() {
		// Instantiate the parent object
		parent::__construct( false, 'CAF Sharing Links Widget' );
	}

	function update( $new_instance, $old_instance ) {
		// Save widget options
		$instance = array();
		$instance['sharing_sendtoafriend'] = ( ! empty( $new_instance['sharing_sendtoafriend'] ) ) ? strip_tags( $new_instance['sharing_sendtoafriend'] ) : '';
		$instance['sharing_sms'] = ( ! empty( $new_instance['sharing_sms'] ) ) ? strip_tags( $new_instance['sharing_sms'] ) : '';
		$instance['sharing_facebook'] = ( ! empty( $new_instance['sharing_facebook'] ) ) ? strip_tags( $new_instance['sharing_facebook'] ) : '';
		$instance['sharing_print'] = ( ! empty( $new_instance['sharing_print'] ) ) ? strip_tags( $new_instance['sharing_print'] ) : '';
		return $instance;
	}

	function form( $instance ) {
		global $CAF_Settings;

		// Output admin widget options form
		$sharing_sendtoafriend = isset($instance['sharing_sendtoafriend']) ? $instance['sharing_sendtoafriend'] : '';
		$sharing_sms = isset($instance['sharing_sms']) ? $instance['sharing_sms'] : '';
		$sharing_facebook = isset($instance['sharing_facebook']) ? $instance['sharing_facebook'] : '';
		$sharing_print = isset($instance['sharing_print']) ? $instance['sharing_print'] : '';

		// Widget admin form
		?>
		<p><a href="/wp-admin/admin.php?page=caf_options&tab=0">Update defaults here</a></p>
		<p>
			<label for="<?php echo $this->get_field_id( 'sharing_sendtoafriend' ); ?>"><?php _e( 'Sent to a friend:' ); ?>
			<input class="widefat" id="<?php echo $this->get_field_id( 'sharing_sendtoafriend' ); ?>" name="<?php echo $this->get_field_name( 'sharing_sendtoafriend' ); ?>" type="checkbox" value="on" <?php if($sharing_sendtoafriend) echo 'checked="checked"'; ?> /></label><br />

			<label for="<?php echo $this->get_field_id( 'sharing_sms' ); ?>"><?php _e( 'Share via SMS:' ); ?>
			<input class="widefat" id="<?php echo $this->get_field_id( 'sharing_sms' ); ?>" name="<?php echo $this->get_field_name( 'sharing_sms' ); ?>" type="checkbox" value="on" <?php if($sharing_sms) echo 'checked="checked"'; ?> /></label><br />

			<label for="<?php echo $this->get_field_id( 'sharing_facebook' ); ?>"><?php _e( 'Share via Facebook:' ); ?>
			<input class="widefat" id="<?php echo $this->get_field_id( 'sharing_facebook' ); ?>" name="<?php echo $this->get_field_name( 'sharing_facebook' ); ?>" type="checkbox" value="on" <?php if($sharing_facebook) echo 'checked="checked"'; ?> /></label><br />

			<label for="<?php echo $this->get_field_id( 'sharing_print' ); ?>"><?php _e( 'Print Page:' ); ?>
			<input class="widefat" id="<?php echo $this->get_field_id( 'sharing_print' ); ?>" name="<?php echo $this->get_field_name( 'sharing_print' ); ?>" type="checkbox" value="on" <?php if($sharing_print) echo 'checked="checked"'; ?> /></label>
		</p>
		<?php
	}

	function widget( $args, $instance ) {
		global $CAF_Settings, $post;
		$output = '';

		// Widget output
		$sharing_sendtoafriend = !empty($instance['sharing_sendtoafriend']) ? $instance['sharing_sendtoafriend'] : null;
		$sharing_sms = !empty($instance['sharing_sms']) ? $instance['sharing_sms'] : null;
		$sharing_facebook = !empty($instance['sharing_facebook']) ? $instance['sharing_facebook'] : null;
		$sharing_print = !empty($instance['sharing_print']) ? $instance['sharing_print'] : null;

		// These are only partial options - they have -text, -link appended in Redux settings. Careful!
		if( $sharing_sendtoafriend) $redux_partial_opts[] = 'opt-caf-sharing-widget-sendtoafriend';
		if( $sharing_sms) $redux_partial_opts[] = 'opt-caf-sharing-widget-sms';
		if( $sharing_facebook) $redux_partial_opts[] = 'opt-caf-sharing-widget-facebook';
		if( $sharing_print) $redux_partial_opts[] = 'opt-caf-sharing-widget-print';


		$iconpos = $CAF_Settings['opt-caf-sharing-widget-icon-align'];

		$output .= '<div class="widget caf-widget caf-sharing-widget clearfix">
			<ul class="caf-sharing-widget-list icon-pos-'.$iconpos.'">';
		foreach($redux_partial_opts as $opt){

			// Set up icons
			$icon = !empty($CAF_Settings[$opt.'-icon']) ? $CAF_Settings[$opt.'-icon'] : null;
			$link = do_shortcode($CAF_Settings[$opt.'-link'] );
			$button_type = isset($CAF_Settings[$opt.'-button-type']) ? $CAF_Settings[$opt.'-button-type'] : null;
			$link_fb_data = $link_class = '';

			if( $button_type == 'form'){
				$link_class = 'caf-fancybox iframe';
				$link_fb_type = 'data-fancybox-type="iframe"';
				$link_fb_height = 'data-fancybox-height="'.trim($CAF_Settings[$opt.'-fancybox-size']['height'], 'px').'"';
				$link_fb_width = 'data-fancybox-width="'.trim($CAF_Settings[$opt.'-fancybox-size']['width'], 'px').'"';
				$link_fb_data = implode(' ', array( $link_fb_type, $link_fb_height, $link_fb_width) );

				$link .= '?'
					.'vehicle_title='.urlencode($post->post_title).''
					.'&vehicle_url='.urlencode(get_permalink($post->ID ) ).''
					.'&vehicle_vin='.urlencode(get_field('caf_vin') ).'';

			} else if($button_type == 'link'){
				$link_class = 'caf-link';
			}

			if( !empty($CAF_Settings[$opt.'-text']) ){

				$output .= '<li class="share-link '.$opt.'">';

				if( $link) $output .= '<a href="'.$link.'" '.$link_fb_data.' class="'.$link_class.'" >';

				if( $icon && $iconpos == 'left') $output .= '<span class="caf-sharing-icon">'.$icon.'</span>';

				// visible text output
				$output .= $CAF_Settings[$opt.'-text'];

				if( $icon && $iconpos == 'right') $output .= '<span class="caf-sharing-icon">'.$icon.'</span>';

				if( $link) $output .= '</a>';

				$output .= '</li>';
			} // if opt exists
		} // foreach opts
		$output .= '</ul></div>';






		echo $output;
	}
}

function CAFSharingLinksWidget_register_widgets() {
	register_widget( 'CAFSharingLinksWidget' );
}

add_action( 'widgets_init', 'CAFSharingLinksWidget_register_widgets' );