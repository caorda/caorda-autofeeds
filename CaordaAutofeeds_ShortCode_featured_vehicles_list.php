<?php
include_once('CaordaAutofeeds_ShortCodeScriptLoader.php');

class CaordaAutofeeds_ShortCode_featured_vehicles_list extends CaordaAutofeeds_ShortCodeScriptLoader {

	static $addedAlready = false;
	public function addScript() {
		if (!self::$addedAlready) {
			self::$addedAlready = true;
			//wp_register_script('my-script', plugins_url('js/my-script.js', __FILE__), array('jquery'), '1.0', true);
			//wp_print_scripts('my-script');
		}
	}

	public function handleShortcode($atts, $content) {
		global $CAF_Settings, $post;
		$output = '';

		$atts = extract( shortcode_atts( array(
			'blink' => "",
			'posts_per_page' => 10,
			'offset' => 0,
			'post_type' => 'caf_inventory',
			'category' => '',
			'orderby' => 'menu_order',
			'order' => 'DESC',
			'include' => '',
			'exclude' => '',
			'meta_key' => '',
			'meta_value' => '',
			'post_mime_type' => '',
			'post_parent' => '',
			'post_status' => 'publish',
			'suppress_filters' => true
		),$atts ) );


		$details = array(
			'caf_model' => 'Model',
			'caf_year'	=> 'Year',
			'caf_trim'	=> 'Trim',
			'caf_summary_features' => array(
				'odometer' => 'Odometer',
				'transmission' => 'Trans',
				'exterior_color' => 'Exterior Color',
				'interior_color' => 'Interior Color',
				'doors' => 'Doors',
			),
		);



			$img_size = $CAF_Settings['opt-caf-list-image-size'] ? $CAF_Settings['opt-caf-list-image-size'] : 'caf_vehicle_list';
			$img_src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $img_size);
			$sale_item = get_field('caf_sale_price', $post->ID);
			$sale_item = $sale_item ? 'sale-item' : '';

			// Set up title
			if( get_field('caf_year', $post->ID) || get_field('caf_make', $post->ID) || get_field('caf_model', $post->ID)){
				$title = '<span class="caf-year">'.get_field('caf_year', $post->ID).'</span>
		        			<span class="caf-make">'.get_field('caf_make', $post->ID).'</span>
		        			<span class="caf-model">'.get_field('caf_model', $post->ID).'</span>
		        			<span class="caf-trim">'.get_field('caf_trim', $post->ID).'</span>';
			} else{
				$title = '<span class="caf-generic-title">'.$post->post_title.'</span>';
			}

			$output .= '<article class="'.implode(" ", get_post_class('post-entry post-entry-type-caf-used-vehicle' )).'">
			<header>
				<div class="caf-vehicle-intro clearfix">
					<div class="caf-float-left">
	        			<h2 class="caf-vehicle-title">'.$title.'</h2>
	        		</div>
	        		<div class="caf-price caf-float-right '.$sale_item.'">
	        			<h3>
	        				<span class="caf-sale-price">'.caf_cur_format(get_field('caf_sale_price', $post->ID), '<span class="prefix">SALE</span>' ).'</span>
	        				<span class="caf-regular-price">'.caf_cur_format(get_field('caf_regular_price', $post->ID) ).'</span>
	        			</h3>
	        		</div>
				</div>
			</header>
			<div id="list-id'.$post->ID.'" class="entry-content vehicle caf-row caf-clearfix">
				<div class="caf-col-3 caf-photo">
					<a href="'.get_permalink($post->ID).'">
						<span class="vehicle-img" style="background-image:url('.$img_src[0].');"></span>
					</a>';

			if( ($carproof_url = get_field('caf_carproof_link', $post->ID) ) && $CAF_Settings['opt-caf-enable-carproof']){
				$output .= '<div class="caf-carproof-link-container">
						<a href="'.$carproof_url.'" target="_blank" class="caf-carproof-link-container">';

				if($cpimg = $CAF_Settings['opt-caf-list-carproof-logo'] ){
					$output .= '<span class="caf-carproof-logo-wrap">
						<img class="caf-carproof-logo" src="'.$cpimg['url'].'" alt="CarProof Report" />
					</span>';
				}
				if($cptext = $CAF_Settings['opt-caf-list-carproof-text'] ){
					$output .= '<span class="caf-carproof-text">'.$cptext.'</span>';
				}

				$output .= '</a>
					</div>';
			} // if carproof available

			$output .= '</div>
				<div class="caf-col-3 caf-details">
					'.caf_compile_details_list( $CAF_Settings['opt-caf-list-details'] , $post).'
				</div>
				<div class="caf-col-3 caf-description">
					'.caf_content(get_field('caf_description', $post->ID), 30, 'read more', $post->ID).'
					<a class="caf-view-vehicle-link" href="'.get_permalink($post->ID).'">View this Vehicle</a>
				</div>
			</div>
			</article>';



		$output = '<div class="caf-featured-list"><div class="caf-vehicle-list">'.$output .'</div></div>';

		return $output;
	}

} // class