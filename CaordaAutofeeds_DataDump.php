<?php

/**
*
* Fancy formatting of JSON document
*
**/

echo 'Add "json_assc=true" to the URL for alternate JSON decoding.';

require('assets/kint/Kint.class.php');
if( !isset($_REQUEST['file'])){
	echo 'No ?file= provided'; exit();
}

$file = $_SERVER['DOCUMENT_ROOT'].$_REQUEST['file'];
$json_assc = isset($_REQUEST['json_assc']) ? true : false;

if( !file_exists($file)){
	echo 'File "'.$file.'" error'; exit();
}

$data = file_get_contents($file);
$json = json_decode($data, $json_assc);


d($json);