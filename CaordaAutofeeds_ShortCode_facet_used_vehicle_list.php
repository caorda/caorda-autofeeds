<?php
include_once('CaordaAutofeeds_ShortCodeScriptLoader.php');

class CaordaAutofeeds_ShortCode_facet_used_vehicle_list extends CaordaAutofeeds_ShortCodeScriptLoader {

	static $addedAlready = false;
	public function addScript() {
		if (!self::$addedAlready) {
			self::$addedAlready = true;

			//wp_register_script('fancybox3', plugins_url('assets/fancybox3/jquery.fancybox.js', __FILE__), array('jquery'), '3.0', true);
			//wp_print_scripts('fancybox3');
		}
	}

	public function handleShortcode($atts, $content) {
		global $CAF_Settings, $post, $wp_query;
		$output = '';

		// Check for themed files, fallback to local $output

		$theme_file = get_template_directory().'/caf-templates/template-vehicle-list-vehicle.php';
		$plugin_file = plugin_dir_path(__FILE__).'/caf-templates/template-vehicle-list-vehicle.php';

		if( file_exists($theme_file)){

			echo '<!-- template: theme -->';
			require_once($theme_file);
			$output = caf_get_vehicle_list_template($post);

		} else if( file_exists($plugin_file)){

			echo '<!-- template: plugin -->';
			require_once($plugin_file);
			$output = caf_get_vehicle_list_template($post);

		} else{
			echo '<!-- template: inline -->';
			// Images
			$img_size = $CAF_Settings['opt-caf-list-image-size'] ? $CAF_Settings['opt-caf-list-image-size'] : 'caf_vehicle_list';
			$img_src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $img_size);
			if( !$img_src[0]) $img_src[0] = $CAF_Settings['opt-caf-list-default-image']['url'];

			$sale_item = get_field('caf_sale_price', $post->ID);
			$sale_item = $sale_item ? 'sale-item' : '';

			// Handle currency formatting
			$price = get_field('caf_regular_price', $post->ID);
			if( (int)$price == 0) $price = $CAF_Settings['opt-caf-vehicle-list-unknown-price'];
			else $price = caf_cur_format(get_field('caf_regular_price', $post->ID) );


			// Set up title
			if( get_field('caf_year', $post->ID) || get_field('caf_make', $post->ID) || get_field('caf_model', $post->ID)){
				$title = '<span class="caf-year">'.get_field('caf_year', $post->ID).'</span>
		        			<span class="caf-make">'.get_field('caf_make', $post->ID).'</span>
		        			<span class="caf-model">'.get_field('caf_model', $post->ID).'</span>
		        			<span class="caf-trim">'.get_field('caf_trim', $post->ID).'</span>';
			} else{
				$title = '<span class="caf-generic-title">'.$post->post_title.'</span>';
			}

			if( $CAF_Settings['opt-caf-enable-carproof'] ){
				$carproof_url = caf_get_carproof_url($post->ID, $CAF_Settings['opt-caf-list-carproof-link-action'] );
			} // if carproof links enabled

			// Generate HTML!
			$output .= '<article class="'.implode(" ", get_post_class('post-entry post-entry-type-caf-used-vehicle' )).'">
			<header>
				<div class="caf-vehicle-intro clearfix">
					<div class="caf-float-left">
	        			<a href="'.get_permalink($post->ID).'"><h2 class="caf-vehicle-title">'.$title.'</h2></a>
	        		</div>
	        		<div class="caf-price caf-float-right '.$sale_item.'">
	        			<h3>
	        				<span class="caf-sale-price">'.caf_cur_format(get_field('caf_sale_price', $post->ID), '<span class="prefix">SALE</span>' ).'</span>
	        				<span class="caf-regular-price">'.$price.'</span>
	        			</h3>
	        		</div>
				</div>
			</header>
			<div id="list-id'.$post->ID.'" class="entry-content vehicle caf-row caf-clearfix">
				<div class="caf-col-3 caf-photo">
					<a href="'.get_permalink($post->ID).'">
						<span class="vehicle-img" style="background-image:url('.$img_src[0].');"></span>
					</a>';

			if( $carproof_url['url']){
				$output .= '<div class="caf-carproof-link-container">
						<a href="'.$carproof_url['url'].'" '.$carproof_url['data'].' class="'.$carproof_url['class'].' caf-carproof-link-container">';

				if($cpimg = $CAF_Settings['opt-caf-list-carproof-logo'] ){
					$output .= '<span class="caf-carproof-logo-wrap">
						<img class="caf-carproof-logo" src="'.$cpimg['url'].'" alt="CarProof Report" />
					</span>';
				}
				if($cptext = $CAF_Settings['opt-caf-list-carproof-text'] ){
					$output .= '<span class="caf-carproof-text">'.$cptext.'</span>';
				}

				$output .= '</a>
					</div>';
			} // if carproof available

			$output .= '</div>
				<div class="caf-col-3 caf-details">
					'.caf_compile_details_list( $CAF_Settings['opt-caf-list-details'] , $post).'
				</div>
				<div class="caf-col-3 caf-description">
					'.caf_content(get_field('caf_description', $post->ID), 30, 'read more', $post->ID).'
					<a class="caf-view-vehicle-link" href="'.get_permalink($post->ID).'">View this Vehicle</a>
				</div>
			</div>';


			$output .= '</article>';
			$output = '<div class="caf-featured-list"><div class="caf-vehicle-list">'.$output .'</div></div>';

		} // if no template files exist

		return $output;
	}

} // class