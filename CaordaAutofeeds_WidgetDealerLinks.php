<?php

class CAFDealerLinksWidget extends WP_Widget {

	function CAFDealerLinksWidget() {
		// Instantiate the parent object
		parent::__construct( false, 'CAF Dealer Links Widget' );
	}

	function update( $new_instance, $old_instance ) {
		// Save widget options
		$instance = array();
		$instance['dealer_phone'] = ( ! empty( $new_instance['dealer_phone'] ) ) ? strip_tags( $new_instance['dealer_phone'] ) : '';
		$instance['dealer_email'] = ( ! empty( $new_instance['dealer_email'] ) ) ? strip_tags( $new_instance['dealer_email'] ) : '';
		$instance['dealer_appraisal'] = ( ! empty( $new_instance['dealer_appraisal'] ) ) ? strip_tags( $new_instance['dealer_appraisal'] ) : '';
		$instance['dealer_finance'] = ( ! empty( $new_instance['dealer_finance'] ) ) ? strip_tags( $new_instance['dealer_finance'] ) : '';
		return $instance;
	}

	function form( $instance ) {
		global $CAF_Settings;

		// Output admin widget options form
		$dealer_phone = isset($instance['dealer_phone']) ? $instance['dealer_phone'] : '';
		$dealer_email = isset($instance['dealer_email']) ? $instance['dealer_email'] : '';
		$dealer_appraisal = isset($instance['dealer_appraisal']) ? $instance['dealer_appraisal'] : '';
		$dealer_finance = isset($instance['dealer_finance']) ? $instance['dealer_finance'] : '';

		// Widget admin form
		?>
		<p><a href="/wp-admin/admin.php?page=caf_options&tab=0">Update defaults here</a></p>
		<p>
			<label for="<?php echo $this->get_field_id( 'dealer_phone' ); ?>"><?php _e( 'Display Phone:' ); ?>
			<input class="widefat" id="<?php echo $this->get_field_id( 'dealer_phone' ); ?>" name="<?php echo $this->get_field_name( 'dealer_phone' ); ?>" type="checkbox" value="on" <?php if($dealer_phone) echo 'checked="checked"'; ?> /></label><br />

			<label for="<?php echo $this->get_field_id( 'dealer_email' ); ?>"><?php _e( 'Display Email:' ); ?>
			<input class="widefat" id="<?php echo $this->get_field_id( 'dealer_email' ); ?>" name="<?php echo $this->get_field_name( 'dealer_email' ); ?>" type="checkbox" value="on" <?php if($dealer_email) echo 'checked="checked"'; ?> /></label><br />

			<label for="<?php echo $this->get_field_id( 'dealer_appraisal' ); ?>"><?php _e( 'Display Appraisal:' ); ?>
			<input class="widefat" id="<?php echo $this->get_field_id( 'dealer_appraisal' ); ?>" name="<?php echo $this->get_field_name( 'dealer_appraisal' ); ?>" type="checkbox" value="on" <?php if($dealer_appraisal) echo 'checked="checked"'; ?> /></label><br />

			<label for="<?php echo $this->get_field_id( 'dealer_finance' ); ?>"><?php _e( 'Display Finance:' ); ?>
			<input class="widefat" id="<?php echo $this->get_field_id( 'dealer_finance' ); ?>" name="<?php echo $this->get_field_name( 'dealer_finance' ); ?>" type="checkbox" value="on" <?php if($dealer_finance) echo 'checked="checked"'; ?> /></label>
		</p>
		<?php
	}

	function widget( $args, $instance ) {
		global $CAF_Settings, $post;
		$output = $linkimg = '';
		$redux_partial_opts = array();

		// Widget output
		$dealer_phone = !empty($instance['dealer_phone']) ? $instance['dealer_phone'] : null;
		$dealer_email = !empty($instance['dealer_email']) ? $instance['dealer_email'] : null;
		$dealer_appraisal = !empty($instance['dealer_appraisal']) ? $instance['dealer_appraisal'] : null;
		$dealer_finance = !empty($instance['dealer_finance']) ? $instance['dealer_finance'] : null;

		// These are only partial options - they have -text, -link appended in Redux settings. Careful!
		if( $dealer_phone) $redux_partial_opts[] = 'opt-caf-dealer-widget-phone';
		if( $dealer_email) $redux_partial_opts[] = 'opt-caf-dealer-widget-email';
		if( $dealer_appraisal) $redux_partial_opts[] = 'opt-caf-dealer-widget-appraisal';
		if( $dealer_finance) $redux_partial_opts[] = 'opt-caf-dealer-widget-finance';


		$iconpos = $CAF_Settings['opt-caf-dealer-widget-icon-align'];

		$output .= '<div class="widget caf-widget caf-dealer-widget clearfix">
			<ul class="caf-dealer-widget-list icon-pos-'.$iconpos.'">';

		foreach( (array) $redux_partial_opts as $opt){

			// Set up icons
			$icon = !empty($CAF_Settings[$opt.'-icon']['url']) ? $CAF_Settings[$opt.'-icon']['url'] : null;
			$link = $CAF_Settings[$opt.'-link'];
			$button_type = isset($CAF_Settings[$opt.'-button-type']) ? $CAF_Settings[$opt.'-button-type'] : null;
			$link_fb_type = $link_fb_height = $link_fb_width = $link_fb_data = $link_class =  '';

			if( $icon && $CAF_Settings['opt-caf-dealer-widget-icon-display'] == 'background'){
				$linkimg = 'style="background-image:url('.$icon.');"';
				$bgimg = '';

			} else{
				$bgimg = '';
			}

			if( $button_type == 'form'){
				$link_class = 'caf-fancybox iframe';
				$link_fb_type = 'data-fancybox-type="iframe"';
				$link_fb_height = 'data-fancybox-height="'.trim($CAF_Settings[$opt.'-fancybox-size']['height'], 'px').'"';
				$link_fb_width = 'data-fancybox-width="'.trim($CAF_Settings[$opt.'-fancybox-size']['width'], 'px').'"';
				$link_fb_data = implode(' ', array( $link_fb_type, $link_fb_height, $link_fb_width) );

				$link .= '?'
					.'vehicle_title='.urlencode($post->post_title).''
					.'&vehicle_url='.urlencode(get_permalink($post->ID ) ).''
					.'&vehicle_vin='.urlencode(get_field('caf_vin') ).'';

			} else if($button_type == 'link'){
				$link_class = 'caf-link';
			}


			if( !empty($CAF_Settings[$opt.'-text']) ){

				$output .= '<li '.$bgimg.' class="dealer-link '.$opt.'">';

				if( $link) $output .= '<a href="'.$link.'" '.$linkimg.' class="'.$link_class.'" '.$link_fb_data.' >';

				if( $icon && $CAF_Settings['opt-caf-dealer-widget-icon-display'] == 'image')
					$output .= '<img src="'.$icon.'" alt="Dealer Link" />';

				$output .= $CAF_Settings[$opt.'-text'];
				if( $link) $output .= '</a>';

				$output .= '</li>';
			} // if opt exists
		} // foreach opts
		$output .= '</ul></div>';






		echo $output;
	}
}

function CAFDealerLinksWidget_register_widgets() {
	register_widget( 'CAFDealerLinksWidget' );
}

add_action( 'widgets_init', 'CAFDealerLinksWidget_register_widgets' );