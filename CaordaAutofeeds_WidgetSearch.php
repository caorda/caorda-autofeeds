<?php

class CAFSearchWidget extends WP_Widget {

	function CAFSearchWidget() {
		// Instantiate the parent object
		parent::__construct( false, 'CAF Search Widget' );
	}

	function update( $new_instance, $old_instance ) {
		// Save widget options
		$instance = array();
		$instance['placeholder'] = ( ! empty( $new_instance['placeholder'] ) ) ? strip_tags( $new_instance['placeholder'] ) : '';
		return $instance;
	}

	function form( $instance ) {
		// Output admin widget options form
		if ( isset( $instance[ 'placeholder' ] ) ) {
			$placeholder = $instance[ 'placeholder' ];
		}
		else {
			$placeholder = __( '', 'wpb_widget_domain' );
		}

		// Widget admin form
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'placeholder' ); ?>"><?php _e( 'Placeholder:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'placeholder' ); ?>" name="<?php echo $this->get_field_name( 'placeholder' ); ?>" type="text" value="<?php echo esc_attr( $placeholder ); ?>" />
		</p>
		<?php 
	}

	function widget( $args, $instance ) {
		// Widget output

		$s = !empty($_GET['s']) ? get_search_query() : '';

		$output .= '<form action="'.home_url( '/' ).'" id="searchform" method="get" class="">
			<div>
				<input type="submit" value="" id="searchsubmit" class="button avia-font-entypo-fontello" />
				<input type="text" id="s" name="caf-search" value="'.$s.'" placeholder="'.$instance['placeholder'].'" />
				
				<input type="hidden" name="post_type[]" value="caf_featured_vehicle" />
				<input type="hidden" name="post_type[]" value="caf_inventory" />
			</div>
		</form>';



		echo $output;
	}
}

function CAFSearchWidget_register_widgets() {
	register_widget( 'CAFSearchWidget' );
}

add_action( 'widgets_init', 'CAFSearchWidget_register_widgets' );