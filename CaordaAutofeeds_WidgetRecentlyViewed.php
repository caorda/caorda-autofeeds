<?php

class CAFRecentlyViewedWidget extends WP_Widget {

	function CAFRecentlyViewedWidget() {
		// Instantiate the parent object
		parent::__construct( false, 'CAF Recently Viewed' );
	}

	function update( $new_instance, $old_instance ) {
		// Save widget options
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['num_links'] = ( ! empty( $new_instance['num_links'] ) ) ? strip_tags( $new_instance['num_links'] ) : '';
		return $instance;
	}

	function form( $instance ) {
		global $CAF_Settings;

		// Output admin widget options form
		$title = isset($instance['title']) ? $instance['title'] : '';
		$num_links = isset($instance['num_links']) ? $instance['num_links'] : '';

		// Widget admin form
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></label><br />

			<label for="<?php echo $this->get_field_id( 'num_links' ); ?>"><?php _e( 'Number of links to show:' ); ?>
			<input class="widefat" id="<?php echo $this->get_field_id( 'num_links' ); ?>" name="<?php echo $this->get_field_name( 'num_links' ); ?>" type="number" step="1" min="0" value="<?php echo $num_links; ?>" /></label><br />
		</p>
		<?php
	}

	function widget( $args, $instance ) {
		global $CAF_Settings, $post;
		$output = '';

		// Widget vars
		$widget_class = implode(' ', array( $args['widget_id'], $args['id'], $args['class']));
		$title = !empty($instance['title']) ? $instance['title'] : null;
		$num_links = !empty($instance['num_links']) ? $instance['num_links'] : null;
		$recent = json_decode( stripslashes( $_COOKIE['caf_recentview']), true);

		// Begin output
		$output .= $args['before_widget'];
		$output .= '<div id="'.$args['widget_id'].'" class="widget '.$widget_class.'">';

		if( $title)
			$output .= '<div class="caf-widget-title">'.$title.'</div>';

		if( $recent && count($recent)){

			// Truncate array if needed

			if( count($recent) > $num_links){
				$recent = array_splice($num_links, 0);

				// Possibly redundant, rush job
				setcookie('caf_recentview',  json_encode($recent ), time()+(86700*7), '/' );
				$_COOKIE['caf_recentview'] = json_encode($recent);
			}

			$output .= '<div class="widget caf-widget caf-recentview-widget clearfix">
				<ul class="caf-recentview-widget-list">';

			foreach($recent as $k=>$url){
				$output .= '<li><a href="'.urldecode($url).'">'.base64_decode($k).'</a></li>';
			}

			$output .= '</ul></div>';
		}

		$output .= '</div><!-- .end widget -->';
		$output .= $args['after_widget'];


		echo $output;
	}
}

function CAFRecentlyViewedWidget_register_widgets() {
	register_widget( 'CAFRecentlyViewedWidget' );
}

add_action( 'widgets_init', 'CAFRecentlyViewedWidget_register_widgets' );