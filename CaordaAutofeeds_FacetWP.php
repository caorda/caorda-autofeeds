<?php

/**
*
* FacetWP Modifications
*
**/

/*=====================================================
=            CAF Settings Scrolltop option            =
=====================================================*/

add_action('wp_head', 'caf_fwp_scrolltop_js');
function caf_fwp_scrolltop_js(){
	$CAF_Settings = get_option('CAF_Settings', array());

	if( $CAF_Settings['opt-caf-fwp-scrolltop-refresh']){
		echo '<script type="text/javascript">var CAF_FWP_scrolltop = true;</script>';
	} else{
		echo '<script type="text/javascript">var CAF_FWP_scrolltop = false;</script>';
	}

}

/*===========================================
=            Altering Pager Text            =
===========================================*/

add_filter( 'facetwp_pager_html', 'caf_facetwp_pager_html', 10, 2 );
function caf_facetwp_pager_html( $output, $params ) {
	global $CAF_Settings;

	if( $first_text = $CAF_Settings['opt-caf-fwp-pager-first-text'])
		$output = str_replace('&lt;&lt;', $first_text, $output);

	if( $last_text = $CAF_Settings['opt-caf-fwp-pager-last-text'])
		$output = str_replace('&gt;&gt;', $last_text, $output);

	return $output;
}
/*
{"price_desc": { "label": "Price (Lowest)", "query_args": { "orderby": "meta_value", "meta_key":"caf_regular_price", "order": "DESC" } } }
{"price_asc": { "label": "Price (Highest)", "query_args": { "orderby": "meta_value", "meta_key":"caf_regular_price", "order": "ASC" } } }
{"year_desc": { "label": "Year (Oldest)", "query_args": { "orderby": "meta_value", "meta_key":"caf_year", "order": "DESC" } } }
{"year_asc": { "label": "Year (Newest)", "query_args": { "orderby": "meta_value", "meta_key":"caf_year", "order": "ASC" } } }
*/
/*==============================================================
=            Alter Sort Results, Add Items Per Page            =
==============================================================*/

//add_filter( 'facetwp_sort_html', 'caf_facetwp_sort_html', 10, 2 );
function caf_facetwp_sort_html( $html, $params ) {
	global $CAF_Settings;

	$html = '<select class="facetwp-sort-select">';
	foreach ( $params['sort_options'] as $key => $atts ) {
		$html .= '<option value="' . $key . '">' . $atts['label'] . '</option>';
	}
	$html .= '</select>';

	return $html;
}

/*=======================================================
=            Custom Items Per Page Shortcode            =
=======================================================*/
add_shortcode( 'caf_facetwp','caf_facetwp_custom' );
function caf_facetwp_custom( $atts ) {
	global $CAF_Settings;
	$html = '';

	$atts = extract( shortcode_atts( array(
		'itemsperpage'=>false
	),$atts ) );


	if( $itemsperpage){
		$html .= '<div class="facetwp-itemsperpage caf-facetwp"><select class="caf-fwp-items-per-page">';
		foreach($CAF_Settings['opt-caf-fwp-items-per-page'] as $item){
			$val = explode(':', $item);
			$label = isset($val[1]) ? $val[1] : $val[0];
			$html .= '<option value="'.$val[0].'">'.$label.'</option>';
		}
		$html .= '</select></div>';
	}

	return $html;
	// do shortcode actions here
}




/*=============================================
=            Altering Sort Options            =
=============================================*/

add_filter( 'facetwp_sort_options', 'caf_facetwp_sort_options', 10, 2 );
function caf_facetwp_sort_options( $options, $params ) {
	global $CAF_Settings;

	if( !empty($CAF_Settings['opt-caf-fwp-sort-options'])){
		$options = array();

		foreach($CAF_Settings['opt-caf-fwp-sort-options'] as $opt){
			if( $option = json_decode($opt, true) ){

				// Get first key of object
				$keys = array_keys($option);

				// Add to options array
				$options[$keys[0]] = $option[$keys[0]];

			} // if decode successful

		} // foreach search option
	} // if search options set

	//echo '<pre>facetwp_sort_options(): $options = '.print_r($options, true).'</pre>';
	return $options;
}





/*======================================================
=            Alter Query for Items per Page            =
======================================================*/

add_filter( 'facetwp_query_args', 'caf_facetwp_query_args', 10, 2 );
function caf_facetwp_query_args( $query_args, $class ) {
	if ( isset( $class->http_params['per_page'] ) ) {
		$query_args['posts_per_page'] = (int) $class->http_params['per_page'];
	}
	return $query_args;
}


/*===========================================
=            Altering facet HTML            =
===========================================*/

add_filter( 'facetwp_facet_html', 'caf_facetwp_facet_html', 10, 2 );
function caf_facetwp_facet_html( $output, $params ) {

	if ( 'keyword' == $params['facet']['name'] ) {
		$output .= '<button class="caf-fwp-searchbtn" onClick="FWP.refresh();"><span class="avia-font-entypo-fontello "></span></button>';
	}

	return $output;
}

/*====================================================
=            Formatting Filter Label Text            =
====================================================*/

add_filter( 'facetwp_selections_html', 'caf_facetwp_selections_html', 10, 2 );
function caf_facetwp_selections_html( $output, $params ) {
	//echo '<pre>'.print_r($params, true).'</pre>';

	// Format prices
	if( isset($params['selections']['price_range'])){
		$val = explode(' to ', trim($params['selections']['price_range'][null], '[]') );

		setlocale(LC_MONETARY, 'en_US');
		foreach($val as &$p){
			$p = money_format('%.0n', $p);
		}
		$params['selections']['price_range'][null] = 'Price: ['.implode(' - ', $val).']';
		//echo '<pre>'.print_r( trim($params['selections']['price_range'][null], '[]'), true).'</pre>';
	}

	// Format year range
	if( isset($params['selections']['year_range'])){
		$val = explode(' to ', trim($params['selections']['year_range'][null], '[]') );
		foreach($val as &$p) $p = (int) $p;
		$params['selections']['year_range'][null] = 'Year: ['.implode(' - ', $val).']';
	}

	// Final render
    $output = '';
    $selections = $params['selections'];
    if ( !empty( $selections ) ) {
        $output .= '<ul>';
        foreach ( $selections as $facet_name => $selection_type ) {
            foreach ( $selection_type as $key => $selection ) {
                $output .= '<li data-facet="' . $facet_name . '" data-value="' . $key . '">';
                $output .= '<span>' . $selection . '</span>';
                $output .= '<span class="facetwp-remove-selection"></span>';
                $output .= '</li>';
            }
        }
        $output .= '</ul>';
    }
    return $output;
}

