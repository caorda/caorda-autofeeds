<?php

class CAFFilterWidget extends WP_Widget {

	function CAFFilterWidget() {
		// Instantiate the parent object
		parent::__construct( false, 'CAF Filter Widget' );
	}

	function update( $new_instance, $old_instance ) {
		// Save widget options
		$instance = array();
		$instance['widgettitle'] = ( ! empty( $new_instance['widgettitle'] ) ) ? strip_tags( $new_instance['widgettitle'] ) : '';
		$instance['resetbtn'] = ( ! empty( $new_instance['resetbtn'] ) ) ? strip_tags( $new_instance['resetbtn'] ) : '';
		return $instance;
	}

	function form( $instance ) {
		// Output admin widget options form
		$widgettitle = isset($instance[ 'widgettitle' ]) ? $instance[ 'widgettitle' ] : __( 'Filters', 'caf_widget_domain' );
		$resetbtn = isset($instance[ 'resetbtn' ]) ? $instance[ 'resetbtn' ] : __( 'Reset Filters', 'caf_widget_domain' );

		// Widget admin form
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'widgettitle' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'widgettitle' ); ?>" name="<?php echo $this->get_field_name( 'widgettitle' ); ?>" type="text" value="<?php echo esc_attr( $widgettitle ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'resetbtn' ); ?>"><?php _e( 'Reset Button Text:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'resetbtn' ); ?>" name="<?php echo $this->get_field_name( 'resetbtn' ); ?>" type="text" value="<?php echo esc_attr( $resetbtn ); ?>" />
		</p>
		<p>Filters can be specified on the <a href="/wp-admin/admin.php?page=caf_options&tab=8">CAF settings page</a>.</p>
		<?php
	}

	function widget( $args, $instance ) {
		global $CAF_Settings;
		$output = ''; // Widget output

		$widget_class = implode(' ', array( $args['widget_id'], $args['id'], $args['class']));
		$widget_title = isset($instance['widgettitle']) ? $args['before_title'].$instance['widgettitle'].$args['after_title'] : '';
		$reset_url = isset( $CAF_Settings['opt-caf-fwp-filter-widget-filter-reseturl']) ? $CAF_Settings['opt-caf-fwp-filter-widget-filter-reseturl'] : 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];

		$output .= $args['before_widget'];
		$output .= '<div id="'.$args['widget_id'].'" class="widget '.$widget_class.'">';
		$output .= '<div class="caf-widget-title">'.$widget_title.'</div>';

		// Set conditional sexytiem formatting classes
		$show_hide_tits = array_search('external', $CAF_Settings['opt-caf-fwp-title-display']) !== false ? 'show-titles' : 'hide-titles';
		if( $CAF_Settings['opt-caf-fwp-enable-dropfilters'] ) $exp_class = 'expand-enabled';
		else $exp_class = '';

		// Output filters
		$output .= '<div class="caf-widget-filters '.$show_hide_tits.' '.$exp_class.'">';
		foreach( (array) $CAF_Settings['opt-caf-fwp-filter-widget-filter-shortcodes'] as $k=>$filter){

			// Set up title and shortcode
			$filter_exp = explode(':', $filter);
			$title = $filter_exp[0];
			$short = isset($filter_exp[1]) ? $filter_exp[1] : $title;

			$title_class = isset($filter_exp[1]) ? "has-title" : "no-title";

			// Output filter
			$output .= '<div class="caf-fwp-filter-group filter-group-'.$short.' '.$title_class.'">';

			if( count($filter_exp) > 1){
				$output .= '<div class="caf-fwp-filter-title filter-title-'.$short.' '.$exp_class.'">'.$CAF_Settings['opt-caf-fwp-filter-title-icon'].$title.'</div>';
			}

			$output .= '<div class="caf-fwp-filter filter-'.$short.'">';
			if( strpos($short, ']')) $output .= do_shortcode($short);
			else $output .= do_shortcode('[facetwp facet="'.$short.'"]');
			$output .= '</div>'; // .caf-fwp-filter

			$output .= '</div>'; // .caf-fwp-filter-group
		} // foreach filters
		$output .= '</div>';

		$output .= '<div class="caf-fwp-reset-filters">';
		$output .= '<a href="'.$reset_url.'" class="caf-fwp-reset">'.do_shortcode($instance['resetbtn']).'</a>';
		$output .= '</div>';


		$output .= '</div>';
		$output .= $args['after_widget'];
		echo $output;
	}
}

function CAFFilterWidget_register_widgets() {
	register_widget( 'CAFFilterWidget' );
}

add_action( 'widgets_init', 'CAFFilterWidget_register_widgets' );