<?php

/**
*  ReduxFramework Sample Config File
*  For full documentation, please visit: https://docs.reduxframework.com
 * */

if (!class_exists('Redux_Framework_CAF_config')) {

	class Redux_Framework_CAF_config {

		public $args        = array();
		public $sections    = array();
		public $theme;
		public $ReduxFramework;

		public function __construct() {

			if (!class_exists('ReduxFramework')) {
				return;
			}

			// This is needed. Bah WordPress bugs.  ;)
			if (  true == Redux_Helpers::isTheme(__FILE__) ) {
				$this->initSettings();
			} else {
				add_action('plugins_loaded', array($this, 'initSettings'), 10);
			}

		}

		public function initSettings() {

			// Just for demo purposes. Not needed per say.
			//$this->theme = wp_get_theme();

			// Set the default arguments
			$this->setArguments();

			// Set a few help tabs so you can see how it's done
			//$this->setHelpTabs();

			// Create the sections and fields
			$this->setSections();

			if (!isset($this->args['opt_name'])) { // No errors please
				return;
			}

			// If Redux is running as a plugin, this will remove the demo notice and links
			add_action( 'redux/loaded', array( $this, 'remove_demo' ) );

			// Function to test the compiler hook and demo CSS output.
			// Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
			//add_filter('redux/options/'.$this->args['opt_name'].'/compiler', array( $this, 'compiler_action' ), 10, 2);

			// Change the arguments after they've been declared, but before the panel is created
			//add_filter('redux/options/'.$this->args['opt_name'].'/args', array( $this, 'change_arguments' ) );

			// Change the default value of a field after it's been set, but before it's been useds
			//add_filter('redux/options/'.$this->args['opt_name'].'/defaults', array( $this,'change_defaults' ) );

			// Dynamically add a section. Can be also used to modify sections/fields
			//add_filter('redux/options/' . $this->args['opt_name'] . '/sections', array($this, 'dynamic_section'));

			$this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
		}

		/**
		*
		*  This is a test function that will let you see when the compiler hook occurs.
		*  It only runs if a field	set with compiler=>true is changed.
		*
		 * */
		function compiler_action($options, $css) {
			//echo '<h1>The compiler hook has run!';
			//print_r($options); //Option values
			//print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )

			/*
			  // Demo of how to use the dynamic CSS and write your own static CSS file
			  $filename = dirname(__FILE__) . '/style' . '.css';
			  global $wp_filesystem;
			  if( empty( $wp_filesystem ) ) {
				require_once( ABSPATH .'/wp-admin/includes/file.php' );
			  WP_Filesystem();
			  }

			  if( $wp_filesystem ) {
				$wp_filesystem->put_contents(
					$filename,
					$css,
					FS_CHMOD_FILE // predefined mode settings for WP files
				);
			  }
			 */
		}

		/**
		*
		*  Custom function for filtering the sections array. Good for child themes to override or add to the sections.
		*  Simply include this function in the child themes functions.php file.
		*
		*  NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
		*  so you must use get_template_directory_uri() if you want to use any of the built in icons
		*
		 * */
		function dynamic_section($sections) {
			//$sections = array();
			$sections[] = array(
				'title' => __('Section via hook', 'caf-redux-framework'),
				'desc' => __('<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'caf-redux-framework'),
				'icon' => 'el-icon-paper-clip',
				// Leave this as a blank section, no options just some intro text set above.
				'fields' => array()
			);

			return $sections;
		}

		/**
		*
		*  Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
		*
		 * */
		function change_arguments($args) {
			//$args['dev_mode'] = true;

			return $args;
		}

		/**
		*
		*  Filter hook for filtering the default value of any given field. Very useful in development mode.
		*
		 * */
		function change_defaults($defaults) {
			$defaults['str_replace'] = 'Testing filter hook!';

			return $defaults;
		}

		// Remove the demo link and the notice of integrated demo from the redux-framework plugin
		function remove_demo() {

			// Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
			if (class_exists('ReduxFrameworkPlugin')) {
				remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::instance(), 'plugin_metalinks'), null, 2);

				// Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
				remove_action('admin_notices', array(ReduxFrameworkPlugin::instance(), 'admin_notices'));
			}
		}

		public function setSections() {

			/**
			*  Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
			 * */
			// Background Patterns Reader
			$sample_patterns_path   = ReduxFramework::$_dir . '../sample/patterns/';
			$sample_patterns_url    = ReduxFramework::$_url . '../sample/patterns/';
			$sample_patterns        = array();

			if (is_dir($sample_patterns_path)) :

				if ($sample_patterns_dir = opendir($sample_patterns_path)) :
					$sample_patterns = array();

					while (( $sample_patterns_file = readdir($sample_patterns_dir) ) !== false) {

						if (stristr($sample_patterns_file, '.png') !== false || stristr($sample_patterns_file, '.jpg') !== false) {
							$name = explode('.', $sample_patterns_file);
							$name = str_replace('.' . end($name), '', $sample_patterns_file);
							$sample_patterns[]  = array('alt' => $name, 'img' => $sample_patterns_url . $sample_patterns_file);
						}
					}
				endif;
			endif;

			ob_start();

			$ct             = wp_get_theme();
			$this->theme    = $ct;
			$item_name      = $this->theme->get('Name');
			$tags           = $this->theme->Tags;
			$screenshot     = $this->theme->get_screenshot();
			$class          = $screenshot ? 'has-screenshot' : '';

			$customize_title = sprintf(__('Customize &#8220;%s&#8221;', 'caf-redux-framework'), $this->theme->display('Name'));

			?>
			<div id="current-theme" class="<?php echo esc_attr($class); ?>">
			<?php if ($screenshot) : ?>
				<?php if (current_user_can('edit_theme_options')) : ?>
						<a href="<?php echo wp_customize_url(); ?>" class="load-customize hide-if-no-customize" title="<?php echo esc_attr($customize_title); ?>">
							<img src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview'); ?>" />
						</a>
				<?php endif; ?>
					<img class="hide-if-customize" src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview'); ?>" />
				<?php endif; ?>

				<h4><?php echo $this->theme->display('Name'); ?></h4>

				<div>
					<ul class="theme-info">
						<li><?php printf(__('By %s', 'caf-redux-framework'), $this->theme->display('Author')); ?></li>
						<li><?php printf(__('Version %s', 'caf-redux-framework'), $this->theme->display('Version')); ?></li>
						<li><?php echo '<strong>' . __('Tags', 'caf-redux-framework') . ':</strong> '; ?><?php printf($this->theme->display('Tags')); ?></li>
					</ul>
					<p class="theme-description"><?php echo $this->theme->display('Description'); ?></p>
			<?php
			if ($this->theme->parent()) {
				printf(' <p class="howto">' . __('This <a href="%1$s">child theme</a> requires its parent theme, %2$s.') . '</p>', __('http://codex.wordpress.org/Child_Themes', 'caf-redux-framework'), $this->theme->parent()->display('Name'));
			}
			?>

				</div>
			</div>

			<?php
			$item_info = ob_get_contents();

			ob_end_clean();

			$sampleHTML = '';
			if (file_exists(dirname(__FILE__) . '/info-html.html')) {
				/** @global WP_Filesystem_Direct $wp_filesystem  */
				global $wp_filesystem;
				if (empty($wp_filesystem)) {
					require_once(ABSPATH . '/wp-admin/includes/file.php');
					WP_Filesystem();
				}
				$sampleHTML = $wp_filesystem->get_contents(dirname(__FILE__) . '/info-html.html');
			}

			// Init settings for use
			$CAF_Settings = get_option('CAF_Settings', array());


			// ACTUAL DECLARATION OF SECTIONS
			// Icons: http://elusiveicons.dsplayer.com/
			$this->sections[] = array(
				'title'     => __('<i class="el-icon-home"></i> General Settings', 'caf-redux-framework'),
				'desc'      => __('Starting options', 'caf-redux-framework'),
				'icon'      => 'el-icon-home',
				// 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
				'fields'    => array(
					array(
					   'id' => 'section-general-settings-widget',
					   'type' => 'section',
					   //'title' => __('Indented Options', 'redux-framework-demo'),
					   //'subtitle' => __('With the "section" field you can create indent option sections.', 'redux-framework-demo'),
					   'indent' => false
					),
					$fields = array(
						'id'       => 'opt-caf-templates',
						'type'     => 'raw',
						'class'	   => 'test',
						'title'    => __('Output Templates', 'caf-redux-frameork'),
						'subtitle' => __('Template file status', 'caf-redux-frameork'),
						//'desc'     => __('This is the description field for additional info.', 'caf-redux-frameork'),
						'content'  => '<a id="caf-install-files" href="'.plugins_url( 'CaordaAutofeeds_InstallReduxThemeFiles.php?state=interface&height=230', __FILE__ ).'" class="thickbox button-primary" title="Installing CAF Theme Files">Install Theme Files</a>'.get_template_file_list_html(),
					),

					array(
						'id'        => 'opt-caf-currency-symbol',
						'type'      => 'select',
						'title'     => __('Currency Symbol', 'caf-redux-framework'),
						//'subtitle'  => __('', 'caf-redux-framework'),
						'desc'      => __('Selects a currency symbol for various display locations on the site.', 'caf-redux-framework'),
						'default'   => '1',
						'options'	=> array(
							'$' => '$',
							'¥' => '¥',
							'₱' => '₱',
							'€' => '€',
							'£' => '£',
						),
					),
					array(
						'id'        => 'opt-caf-cursymbol-position',
						'type'      => 'switch',
						'title'     => __('Symbol Position', 'caf-redux-framework'),
						//'subtitle' 	=> __('navigation'),
						'desc'      => __('Determines position of currency symbol in displays.', 'caf-redux-framework'),
						'default'   => true,
						'on'        => 'Before Number',
						'off'       => 'After Number',
					),
					array(
						'id'        => 'opt-caf-search-page',
						'type'      => 'select',
						'title'     => __('Search Page', 'caf-redux-framework'),
						'desc'  	=> __('Vehicle Search page URL, used in plugin.', 'caf-redux-framework'),
						'default'   => '/caf-inventory/',
						'data'		=> 'pages',
					),
					array(
						'id'        => 'opt-caf-contact-page',
						'type'      => 'text',
						'title'     => __('Contact Page', 'caf-redux-framework'),
						'desc'  	=> __('Contact page URL, used in widget.', 'caf-redux-framework'),
						'default'   => '#',
					),
					array(
						'id'        => 'opt-caf-tradein-page',
						'type'      => 'text',
						'title'     => __('Trade-In Page', 'caf-redux-framework'),
						'desc'  	=> __('Trade-in page URL, used in widget.', 'caf-redux-framework'),
						'default'   => '#',
					),
					array(
						'id'        => 'opt-caf-appraisal-page',
						'type'      => 'text',
						'title'     => __('Appraisal Page', 'caf-redux-framework'),
						'desc'  	=> __('Appraisal page URL, used in widget.', 'caf-redux-framework'),
						'default'   => '#',
					),
					array(
						'id'        => 'opt-caf-dealer-phone',
						'type'      => 'text',
						'title'     => __('Dealer Phone Number', 'caf-redux-framework'),
						'desc'  	=> __('Dealer Phone Number URL, used in widget.', 'caf-redux-framework'),
						'default'   => '250-555-1234',
					),

					array(
						'id'        => 'opt-caf-enable-fontawesome',
						'type'      => 'switch',
						'title'     => __('Enable FontAwesome Icons', 'caf-redux-framework'),
						'desc'      => __('Allows use of fontawesome icons in theme - global enqueue.', 'caf-redux-framework'),
						'default'   => true,
						'on'        => 'Enable FontAwesome',
						'off'       => 'Disable',
					),
					array(
						'id'     => 'section-general-settings-end',
						'type'   => 'section',
						'indent' => false,
					),
				),
			);

			$this->sections[] = array(
				'type' => 'divide',
			);

			// Set up image sizes
			$sizes = array();
			foreach( get_intermediate_image_sizes() as $size) $sizes[$size] = $size;

			$this->sections[] = array(
				'icon'      => 'el-icon-th-list',
				'title'     => __('<i class="el-icon-th-list"></i> Vehicle Lists', 'caf-redux-framework'),
				'fields'    => array(
					array(
					   'id' => 'section-vehicle-lists-widget',
					   'type' => 'section',
					   'indent' => false
					),
					array(
						'id'        => 'opt-caf-vehicle-list-page-title',
						'type'      => 'text',
						'title'     => __('Inventory Page Title', 'caf-redux-framework'),
						'desc'  	=> __('Page title for Vehicle Inventory List template', 'caf-redux-framework'),
						'default'   => 'Inventory Search',
					),
					array(
						'id'        => 'opt-caf-list-image-size',
						'type'      => 'select',
						'title'     => __('List Image Size', 'caf-redux-framework'),
						'required' 	=> array('opt-caf-list-image-size', '!=', ''),
						//'subtitle'  => __('', 'caf-redux-framework'),
						'desc'      => __('Sets the image size to display on list views.', 'caf-redux-framework'),
						'default'   => 'caf_vehicle_list',
						'options'	=> 	$sizes,
					),
					array(
						'id'          => 'opt-caf-list-details',
						'type'        => 'textarea',
						'title'       => __('Details', 'caf-redux-frameork'),
						'subtitle'    => __('Format: <pre>fieldname:label</pre> or <pre>parent,fieldname:label</pre>', 'caf-redux-frameork'),
						'desc'        => __('Select details to display on the list view.', 'caf-redux-frameork'),
						//'validate'	  => 'html_custom',
						'default'	  => "caf_model:Model\ncaf_year:Year\ncaf_trim:Trim\n"
							."caf_summary_features,odometer:Odometer\n"
							."caf_summary_features,transmission:Trans\n"
							."caf_summary_features,exterior_color:Exterior Color\n"
							."caf_summary_features,interior_color:Interior Color\n"
							."caf_summary_features,doors:Doors",
					),
					array(
						'id'       => 'opt-caf-list-default-image',
						'type'     => 'media',
						'url'      => true,
						'title'    => __('Default Vehicle Image', 'redux-framework-demo'),
						'desc'     => __('Displays when no other image is available. ', 'redux-framework-demo'),
						//'subtitle' => __('Upload any media using the WordPress native uploader', 'redux-framework-demo'),
						'default'  => array(
							'url'=>plugins_url( 'img/vehicle_blank.png', __FILE__ )
						),
					),
					array(
						'id'        => 'opt-caf-vehicle-list-unknown-price',
						'type'      => 'text',
						'title'     => __('Unknown Price Text', 'caf-redux-framework'),
						'desc'  	=> __('Displays text for zero-value prices', 'caf-redux-framework'),
						'default'   => 'Please Call',
					),
					array(
						'id'     => 'section-vehicle-lists-end',
						'type'   => 'section',
						'indent' => false,
					),
				),
			);

			$this->sections[] = array(
				'icon'      => 'el-icon-website',
				'title'     => __('<i class="el-icon-website"></i> Vehicle Detail Pages', 'caf-redux-framework'),
				'desc'      => __('<p>These settings are primary switches for detail pages and can be overridden on a per-vehicle basis. Secondary settings are displayed in the right sidebar beneath the <strong>Publish</strong> button.</p>', 'caf-redux-framework'),
				'fields'    => array(
					array(
					   'id' => 'section-vehicle-detail-pages-widget',
					   'type' => 'section',
					   //'title' => __('Indented Options', 'redux-framework-demo'),
					   //'subtitle' => __('With the "section" field you can create indent option sections.', 'redux-framework-demo'),
					   'indent' => false
					),
					array(
						'id'       => 'opt-caf-detail-back-buttons',
						'type'     => 'button_set',
						'title'    => __('Back to List Buttons', 'redux-framework-demo'),
						//'subtitle' => __('No validation can be done on this field type', 'redux-framework-demo'),
						'desc'     => __('Displays "back" buttons on the listing page.', 'redux-framework-demo'),
						'multi'    => true,
						'options' => array(
							'top' => 'Top of Listing',
							'bottom' => 'Bottom of Listing',
						 ),
						'default' => array('top', 'bottom'),
					),
					array(
						'id'        => 'opt-caf-detail-back-button-text',
						'type'      => 'text',
						'title'     => __('Back Button Text', 'caf-redux-framework'),
						//'subtitle' 	=> __('navigation'),
						'desc'      => __('HTML area for back buttons.', 'caf-redux-framework'),
						'default'   => '<span class="back-icon avia-font-entypo-fontello"></span> Go Back',
					),
					array(
						'id'        => 'opt-caf-detail-show-page-content',
						'type'      => 'switch',
						'title'     => __('Show Page Content', 'caf-redux-framework'),
						//'subtitle' 	=> __('navigation'),
						'desc'      => __('Display post content.', 'caf-redux-framework'),
						'default'   => false,
						'on'        => 'On',
						'off'       => 'Off',
					),
					array(
						'id'        => 'opt-caf-detail-show-large-gallery',
						'type'      => 'switch',
						'title'     => __('Large Slider', 'caf-redux-framework'),
						//'subtitle' 	=> __('navigation'),
						'desc'      => __('Display large image gallery.', 'caf-redux-framework'),
						'default'   => true,
						'on'        => 'On',
						'off'       => 'Off',
					),
					array(
						'id'        => 'opt-caf-detail-show-small-gallery',
						'type'      => 'switch',
						'title'     => __('Small Slider', 'caf-redux-framework'),
						//'subtitle' 	=> __('navigation'),
						'desc'      => __('Display small image gallery.', 'caf-redux-framework'),
						'default'   => true,
						'on'        => 'On',
						'off'       => 'Off',
					),
					array(
						'id'        => 'opt-caf-detail-show-description',
						'type'      => 'switch',
						'title'     => __('Vehicle Description', 'caf-redux-framework'),
						//'subtitle' 	=> __('navigation'),
						'desc'      => __('Display vehicle description.', 'caf-redux-framework'),
						'default'   => true,
						'on'        => 'On',
						'off'       => 'Off',
					),
					array(
						'id'        => 'opt-caf-detail-show-summary',
						'type'      => 'switch',
						'title'     => __('Summary', 'caf-redux-framework'),
						//'subtitle' 	=> __('navigation'),
						'desc'      => __('Display vehicle summary stats.', 'caf-redux-framework'),
						'default'   => true,
						'on'        => 'On',
						'off'       => 'Off',
					),
					array(
						'id'        => 'opt-caf-detail-show-cta',
						'type'      => 'switch',
						'title'     => __('Call To Action', 'caf-redux-framework'),
						//'subtitle' 	=> __('navigation'),
						'desc'      => __('Display call-to-action box.', 'caf-redux-framework'),
						'default'   => false,
						'on'        => 'On',
						'off'       => 'Off',
					),

					// Hidden cta contents

					array(
						'id'        => 'section-ctacontents-start',
						'type'      => 'section',
						'title'     => __('Call-To-Action Settings', 'caf-redux-framework'),
						//'subtitle'  => __('With the "section" field you can create indent option sections.', 'caf-redux-framework'),
						'indent'    => true,
						'required'  => array('opt-caf-detail-show-cta', "=", 1),
					),
					array(
						'id'               => 'opt-caf-detail-cta-contents',
						'type'             => 'editor',
						'title'            => __('CTA Text', 'caf-redux-frameork'),
						//'subtitle'         => __('Subtitle text would go here.', 'caf-redux-frameork'),
						'default'          => 'Do you want to know more?',
						'editor_options'   => array(
							'teeny'            => true,
							'textarea_rows'    => 10
						)
					),
					array(
						'id'        => 'opt-caf-detail-cta-link',
						'type'      => 'text',
						'title'     => __('CTA Link', 'caf-redux-framework'),
						//'subtitle' 	=> __('navigation'),
						'desc'      => __('Links CTA to specified page.', 'caf-redux-framework'),
						'default'   => '/contact/',
					),
					array(
						'id'        => 'opt-caf-detail-cta-icon',
						'type'      => 'text',
						'title'     => __('CTA Icon', 'caf-redux-framework'),
						//'subtitle' 	=> __('navigation'),
						'desc'      => __('HTML area for simple icon insert.', 'caf-redux-framework'),
						'default'   => '<span class="icon-demo"></span>',
					),
					array(
						'id'       => 'opt-caf-detail-cta-background',
						'type'     => 'background',
						'title'    => __('CTA Background', 'redux-framework-demo'),
						//'subtitle' => __('Body background with image, color, etc.', 'redux-framework-demo'),
						'desc'     => __('This is the description field, again good for additional info.', 'redux-framework-demo'),
						'default'  => array(
							'background-color' => '#1e73be',
						)
					),
					array(
						'id'        => 'section-ctacontents-end',
						'type'      => 'section',
						'indent'    => false, // Indent all options below until the next 'section' option is set.
						'required'  => array('opt-caf-detail-cta-contents', "=", 1),
					),

					// / Hidden cta contents

					array(
						'id'        => 'opt-caf-detail-show-additional',
						'type'      => 'switch',
						'title'     => __('Additional Features', 'caf-redux-framework'),
						//'subtitle' 	=> __('navigation'),
						'desc'      => __('Display additional features.', 'caf-redux-framework'),
						'default'   => true,
						'on'        => 'On',
						'off'       => 'Off',
					),

					// Hidden Additional Features contents

					array(
						'id'        => 'section-additionalfeatures-start',
						'type'      => 'section',
						'title'     => __('Call-To-Action Settings', 'caf-redux-framework'),
						//'subtitle'  => __('With the "section" field you can create indent option sections.', 'caf-redux-framework'),
						'indent'    => true,
						'required'  => array('opt-caf-detail-show-additional', "=", 1),
					),
					array(
						'id'        => 'opt-caf-detail-additional-list-icon',
						'type'      => 'text',
						'title'     => __('List Icon', 'caf-redux-framework'),
						//'subtitle' 	=> __('navigation'),
						'desc'      => __('HTML area for simple icon insert.', 'caf-redux-framework'),
						'default'   => '<span class="list-icon avia-font-entypo-fontello"></span>',
					),
					array(
						'id'        => 'section-additionalfeatures-end',
						'type'      => 'section',
						'indent'    => false, // Indent all options below until the next 'section' option is set.
						'required'  => array('opt-caf-detail-show-additional', "=", 1),
					),
					array(
						'id'     => 'section-vehicle-detail-pages-end',
						'type'   => 'section',
						'indent' => false,
					),

					// /Hidden Additional Features contents

				),
			);
			$this->sections[] = array(
				'icon'      => 'el-icon-globe',
				'title'     => __('<i class="el-icon-globe"></i> CarProof Settings', 'caf-redux-framework'),
				'fields'    => array(
					array(
					   'id' => 'section-carproof-settings-widget',
					   'type' => 'section',
					   //'title' => __('Indented Options', 'redux-framework-demo'),
					   //'subtitle' => __('With the "section" field you can create indent option sections.', 'redux-framework-demo'),
					   'indent' => false
					),
					array(
					   'id' => 'section-carproof-settings-api-start',
					   'type' => 'section',
					   'title' => __('API Settings', 'redux-framework-demo'),
					   //'subtitle' => __('With the "section" field you can create indent option sections.', 'redux-framework-demo'),
					   'indent' => true
					),
					array(
						'id'        => 'opt-caf-carproof-api-dealer-key',
						'type'      => 'multi_text',
						'title'     => __('Dealer Key', 'caf-redux-framework'),
						//'subtitle' 	=> __('navigation'),
						'desc'      => __('Third-party access key for link data. Add keys as needed corresponding with each dealership.', 'caf-redux-framework'),
						'default'   => array('#'),
					),
					array(
						'id'     => 'section-carproof-settings-api-end',
						'type'   => 'section',
						'indent' => false,
					),
					array(
					   'id' => 'section-carproof-settings-button-start',
					   'type' => 'section',
					   'title' => __('Button Settings', 'redux-framework-demo'),
					   //'subtitle' => __('With the "section" field you can create indent option sections.', 'redux-framework-demo'),
					   'indent' => true
					),
					array(
						'id'        => 'opt-caf-enable-carproof',
						'type'      => 'button_set',
						'title'     => __('Enable CarProof Links', 'caf-redux-framework'),
						//'subtitle' 	=> __('navigation'),
						'desc'      => __('Displays CarProof links if available for vehicles.', 'caf-redux-framework'),
						'default'   => 3,
						'options' 	=> array(
							'enable'		=> 'Enable',
							'webform'		=> 'Webform',
							'0'		=> 'Disable',
						),
					),
					array(
						'id'        => 'opt-caf-carproof-iframe',
						'type'      => 'text',
						'title'     => __('CarProof Webform iFrame Link', 'caf-redux-framework'),
						//'subtitle' 	=> __('navigation'),
						'desc'      => __('Links CarProof button to specific URL.', 'caf-redux-framework'),
						'default'   => '#',
					),
					array(
						'id'       => 'opt-caf-carproof-fancybox-size',
						'type'     => 'dimensions',
						'units'    => false,
						'title'    => __('Fancybox Size', 'redux-framework-demo'),
						'desc'     => __('Specify fancybox size in pixels', 'redux-framework-demo'),
						'default'  => array(
							'Width'   => '400',
							'Height'  => '900'
						),
					),
					array(
						'id'        => 'opt-caf-list-carproof-logo',
						'type'      => 'media',
						'title'     => __('List View CarProof Logo', 'caf-redux-framework'),
						'url'		=> true,
						//'subtitle' 	=> __('navigation'),
						'desc'      => __('Add a custom logo for CarProof links.', 'caf-redux-framework'),
						'default'   => array(
							'url' => plugins_url('img/carproof_logo.png', __FILE__)
						),
					),
					array(
						'id'        => 'opt-caf-list-carproof-text',
						'type'      => 'text',
						'title'     => __('List View CarProof Button Text', 'caf-redux-framework'),
						//'subtitle' 	=> __('navigation'),
						'desc'      => __('Displays text next to the CarProof logo.', 'caf-redux-framework'),
						'default'   => 'Report Available',
					),
					array(
						'id'        => 'opt-caf-list-carproof-link-action',
						'type'      => 'button_set',
						'title'     => __('List View Link Action', 'caf-redux-framework'),
						//'subtitle' 	=> __('navigation'),
						'desc'      => __('Displays CarProof links if available for vehicles.', 'caf-redux-framework'),
						'default'   => 'report',
						'options' 	=> array(
							'report'		=> 'Report Link',
							'vehicle'		=> 'Vehicle Link',
							'0'		=> 'Disable',
						),
					),
					array(
						'id'        => 'opt-caf-detail-carproof-logo',
						'type'      => 'media',
						'title'     => __('Detail View CarProof Logo', 'caf-redux-framework'),
						'url'		=> true,
						//'subtitle' 	=> __('navigation'),
						'desc'      => __('Add a custom logo for CarProof links.', 'caf-redux-framework'),
						'default'   => array(
							'url' => plugins_url('img/carproof_detail_logo.png', __FILE__)
						),
					),
					array(
						'id'        => 'opt-caf-detail-carproof-text',
						'type'      => 'text',
						'title'     => __('Detail View CarProof Button Text', 'caf-redux-framework'),
						//'subtitle' 	=> __('navigation'),
						'desc'      => __('Displays text next to the CarProof logo.', 'caf-redux-framework'),
						'default'   => '',
					),
					array(
						'id'     => 'section-carproof-settings-button-end',
						'type'   => 'section',
						'indent' => false,
					),
					array(
						'id'     => 'section-carproof-settings-end',
						'type'   => 'section',
						'indent' => false,
					),
				),
			);

			$this->sections[] = array(
				'type' => 'divide',
			);

			$this->sections[] = array(
				'title'     => __('<i class="el-icon-puzzle"></i> Dealer Links Widget', 'caf-redux-framework'),
				'desc'      => __('', 'caf-redux-framework'),
				'icon'      => 'el-icon-puzzle',
				// 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
				'fields'    => array(
					array(
					   'id' => 'section-dealer-links-widget',
					   'type' => 'section',
					   //'title' => __('Indented Options', 'redux-framework-demo'),
					   //'subtitle' => __('With the "section" field you can create indent option sections.', 'redux-framework-demo'),
					   'indent' => false
					),
					array(
						'id'       => 'opt-caf-dealer-widget-icon-align',
						'type'     => 'button_set',
						'title'    => __('Icon Placement', 'redux-framework-demo'),
						'subtitle' => __('Align icons in buttons', 'redux-framework-demo'),
						//'desc'     => __('This is the description field, again good for additional info.', 'redux-framework-demo'),
						'options' => array(
							'left' => 'Left',
							'right' => 'Right',
						 ),
						'default' => 'left'
					),
					array(
						'id'       => 'opt-caf-dealer-widget-icon-display',
						'type'     => 'button_set',
						'title'    => __('Icon Type', 'redux-framework-demo'),
						'subtitle' => __('Select icon render method', 'redux-framework-demo'),
						//'desc'     => __('This is the description field, again good for additional info.', 'redux-framework-demo'),
						'options' => array(
							'image' => 'Image Tag',
							'background' => 'CSS Background-Image',
						 ),
						'default' => 'image'
					),
					array(
					   'id' => 'section-dealer-phone-start',
					   'type' => 'section',
					   'title' => __('Dealer Phone', 'redux-framework-demo'),
					   'subtitle' => __('Options for the Dealer Phone button.', 'redux-framework-demo'),
					   'indent' => true
					),
					array(
						'id'        => 'opt-caf-dealer-widget-phone-text',
						'type'      => 'text',
						'title'     => __('Dealer Phone Text', 'caf-redux-framework'),
						'desc'  	=> __('Default phone text for widget display.', 'caf-redux-framework'),
						'default'   => 'Call',
					),
					array(
						'id'       => 'opt-caf-dealer-widget-phone-button-type',
						'type'     => 'button_set',
						'title'    => __('Button Type', 'redux-framework-demo'),
						'desc'     => __('Determines button action.', 'redux-framework-demo'),
						//Must provide key => value pairs for options
						'options' => array(
							'form' => 'iFrame Form',
							'link' => 'Link',
						 ),
						'default' => 'link'
					),
					array(
						'id'       => 'opt-caf-dealer-widget-phone-fancybox-size',
						'type'     => 'dimensions',
						'units'    => false,
						'title'    => __('Fancybox Size', 'redux-framework-demo'),
						'desc'     => __('Specify fancybox size in pixels', 'redux-framework-demo'),
						'default'  => array(
							'Width'   => '400',
							'Height'  => '900'
						),
						'required' => array('opt-caf-dealer-widget-phone-button-type', '=', 'form'),
					),
					array(
						'id'        => 'opt-caf-dealer-widget-phone-link',
						'type'      => 'text',
						'title'     => __('Dealer Phone Link', 'caf-redux-framework'),
						'desc'  	=> __('Link phone number to this page.', 'caf-redux-framework'),
						'default'   => '/contact/',
					),
					array(
						'id'       => 'opt-caf-dealer-widget-phone-icon',
						'type'     => 'media',
						'url'      => true,
						'title'    => __('Dealer Phone Icon', 'redux-framework-demo'),
						//'desc'     => __('Basic media uploader with disabled URL input field.', 'redux-framework-demo'),
						//'subtitle' => __('Upload any media using the WordPress native uploader', 'redux-framework-demo'),
						'default'  => array(
							'url'=>''
						),
					),
					array(
						'id'     => 'section-dealer-phone-end',
						'type'   => 'section',
						'indent' => false,
					),
					array(
					   'id' => 'section-dealer-email-start',
					   'type' => 'section',
					   'title' => __('Dealer Email', 'redux-framework-demo'),
					   'subtitle' => __('Options for the Dealer Email button.', 'redux-framework-demo'),
					   'indent' => true 
					),
					array(
						'id'        => 'opt-caf-dealer-widget-email-text',
						'type'      => 'text',
						'title'     => __('Dealer Email Text', 'caf-redux-framework'),
						'desc'  	=> __('Default email text for widget display.', 'caf-redux-framework'),
						'default'   => 'Email for details',
					),
					array(
						'id'       => 'opt-caf-dealer-widget-email-button-type',
						'type'     => 'button_set',
						'title'    => __('Button Type', 'redux-framework-demo'),
						'desc'     => __('Determines button action.', 'redux-framework-demo'),
						//Must provide key => value pairs for options
						'options' => array(
							'form' => 'iFrame Form',
							'link' => 'Link',
						 ),
						'default' => 'link'
					),
					array(
						'id'       => 'opt-caf-dealer-widget-email-fancybox-size',
						'type'     => 'dimensions',
						'units'    => false,
						'title'    => __('Fancybox Size', 'redux-framework-demo'),
						'desc'     => __('Specify fancybox size in pixels', 'redux-framework-demo'),
						'default'  => array(
							'Width'   => '400',
							'Height'  => '900'
						),
						'required' => array('opt-caf-dealer-widget-email-button-type', '=', 'form'),
					),
					array(
						'id'        => 'opt-caf-dealer-widget-email-link',
						'type'      => 'text',
						'title'     => __('Dealer Email Link', 'caf-redux-framework'),
						'desc'  	=> __('Link email to this page.', 'caf-redux-framework'),
						'default'   => '/forms/get-vehicle-details/',
					),
					array(
						'id'       => 'opt-caf-dealer-widget-email-icon',
						'type'     => 'media',
						'url'      => true,
						'title'    => __('Dealer Email Icon', 'redux-framework-demo'),
						//'desc'     => __('Basic media uploader with disabled URL input field.', 'redux-framework-demo'),
						//'subtitle' => __('Upload any media using the WordPress native uploader', 'redux-framework-demo'),
						'default'  => array(
							'url'=>''
						),
					),
					array(
						'id'     => 'section-dealer-email-end',
						'type'   => 'section',
						'indent' => false,
					),

					array(
					   'id' => 'section-dealer-appraisal-start',
					   'type' => 'section',
					   'title' => __('Dealer Appraisal', 'redux-framework-demo'),
					   'subtitle' => __('Options for the Dealer Appraisal button.', 'redux-framework-demo'),
					   'indent' => true
					),
					array(
						'id'        => 'opt-caf-dealer-widget-appraisal-text',
						'type'      => 'text',
						'title'     => __('Dealer Appraisal Text', 'caf-redux-framework'),
						'desc'  	=> __('Default appraisal text for widget display.', 'caf-redux-framework'),
						'default'   => 'Trade-in Appraisal',
					),
					array(
						'id'       => 'opt-caf-dealer-widget-appraisal-button-type',
						'type'     => 'button_set',
						'title'    => __('Button Type', 'redux-framework-demo'),
						'desc'     => __('Determines button action.', 'redux-framework-demo'),
						//Must provide key => value pairs for options
						'options' => array(
							'form' => 'iFrame Form',
							'link' => 'Link',
						 ),
						'default' => 'link'
					),
					array(
						'id'       => 'opt-caf-dealer-widget-appraisal-fancybox-size',
						'type'     => 'dimensions',
						'units'    => false,
						'title'    => __('Fancybox Size', 'redux-framework-demo'),
						'desc'     => __('Specify fancybox size in pixels', 'redux-framework-demo'),
						'default'  => array(
							'Width'   => '400',
							'Height'  => '900'
						),
						'required' => array('opt-caf-dealer-widget-appraisal-button-type', '=', 'form'),
					),
					array(
						'id'        => 'opt-caf-dealer-widget-appraisal-link',
						'type'      => 'text',
						'title'     => __('Dealer Appraisal Link', 'caf-redux-framework'),
						'desc'  	=> __('Link appraisal to this page.', 'caf-redux-framework'),
						'default'   => '/forms/trade-appraisal',
					),
					array(
						'id'       => 'opt-caf-dealer-widget-appraisal-icon',
						'type'     => 'media',
						'url'      => true,
						'title'    => __('Dealer Appraisal Icon', 'redux-framework-demo'),
						//'desc'     => __('Basic media uploader with disabled URL input field.', 'redux-framework-demo'),
						//'subtitle' => __('Upload any media using the WordPress native uploader', 'redux-framework-demo'),
						'default'  => array(
							'url'=>''
						),
					),
					array(
						'id'     => 'section-dealer-appraisal-end',
						'type'   => 'section',
						'indent' => false,
					),
					array(
					   'id' => 'section-dealer-finance-start',
					   'type' => 'section',
					   'title' => __('Dealer Finance', 'redux-framework-demo'),
					   'subtitle' => __('Options for the Dealer Finance button.', 'redux-framework-demo'),
					   'indent' => true 
					),
					array(
						'id'        => 'opt-caf-dealer-widget-finance-text',
						'type'      => 'text',
						'title'     => __('Dealer Finance Inquiry Text', 'caf-redux-framework'),
						'desc'  	=> __('Default finance text for widget display.', 'caf-redux-framework'),
						'default'   => 'Finance Inquiry',
					),
					array(
						'id'       => 'opt-caf-dealer-widget-finance-button-type',
						'type'     => 'button_set',
						'title'    => __('Button Type', 'redux-framework-demo'),
						'desc'     => __('Determines button action.', 'redux-framework-demo'),
						//Must provide key => value pairs for options
						'options' => array(
							'form' => 'iFrame Form',
							'link' => 'Link',
						 ),
						'default' => 'link'
					),
					array(
						'id'       => 'opt-caf-dealer-widget-finance-fancybox-size',
						'type'     => 'dimensions',
						'units'    => false,
						'title'    => __('Fancybox Size', 'redux-framework-demo'),
						'desc'     => __('Specify fancybox size in pixels', 'redux-framework-demo'),
						'default'  => array(
							'Width'   => '400',
							'Height'  => '900'
						),
						'required' => array('opt-caf-dealer-widget-finance-button-type', '=', 'form'),
					),
					array(
						'id'        => 'opt-caf-dealer-widget-finance-link',
						'type'      => 'text',
						'title'     => __('Dealer Finance Inquiry Link', 'caf-redux-framework'),
						'desc'  	=> __('Link finance to this page.', 'caf-redux-framework'),
						'default'   => '/forms/finance-inquiry',
					),
					array(
						'id'       => 'opt-caf-dealer-widget-finance-icon',
						'type'     => 'media',
						'url'      => true,
						'title'    => __('Dealer Finance Inquiry Icon', 'redux-framework-demo'),
						//'desc'     => __('Basic media uploader with disabled URL input field.', 'redux-framework-demo'),
						//'subtitle' => __('Upload any media using the WordPress native uploader', 'redux-framework-demo'),
						'default'  => array(
							'url'=>''
						),
					),
					array(
						'id'     => 'section-dealer-finance-end',
						'type'   => 'section',
						'indent' => false,
					),
					array(
						'id'     => 'section-dealer-links-widget-end',
						'type'   => 'section',
						'indent' => false,
					),
				),
			);

			$this->sections[] = array(
				'title'     => __('<i class="el-icon-puzzle"></i> Sharing Links Widget', 'caf-redux-framework'),
				'desc'      => __('', 'caf-redux-framework'),
				'icon'      => 'el-icon-puzzle',
				// 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
				'fields'    => array(
					array(
					   'id' => 'section-sharing-links-widget',
					   'type' => 'section',
					   //'title' => __('Indented Options', 'redux-framework-demo'),
					   //'subtitle' => __('With the "section" field you can create indent option sections.', 'redux-framework-demo'),
					   'indent' => false
					),
					array(
						'id'       => 'opt-caf-sharing-widget-icon-align',
						'type'     => 'button_set',
						'title'    => __('Icon Placement', 'redux-framework-demo'),
						'subtitle' => __('Align icons in buttons', 'redux-framework-demo'),
						//'desc'     => __('This is the description field, again good for additional info.', 'redux-framework-demo'),
						'options' => array(
							'left' => 'Left',
							'right' => 'Right',
						 ),
						'default' => 'left'
					),
					array(
					   'id' => 'section-sharing-links-widget-dealersend-start',
					   'type' => 'section',
					   'title' => __('Dealer Send Link', 'redux-framework-demo'),
					   //'subtitle' => __('With the "section" field you can create indent option sections.', 'redux-framework-demo'),
					   'indent' => true
					),
					array(
						'id'        => 'opt-caf-sharing-widget-sendtoafriend-text',
						'type'      => 'text',
						'title'     => __('Dealer Send Text', 'caf-redux-framework'),
						'desc'  	=> __('Default Send to a Friend text for widget display.', 'caf-redux-framework'),
						'default'   => 'Send to a friend',
					),
					array(
						'id'       => 'opt-caf-sharing-widget-sendtoafriend-button-type',
						'type'     => 'button_set',
						'title'    => __('Link Type', 'redux-framework-demo'),
						'desc'     => __('Determines link action.', 'redux-framework-demo'),
						//Must provide key => value pairs for options
						'options' => array(
							'form' => 'iFrame Form',
							'link' => 'Link',
						 ),
						'default' => 'link'
					),
					array(
						'id'       => 'opt-caf-sharing-widget-sendtoafriend-fancybox-size',
						'type'     => 'dimensions',
						'units'    => false,
						'title'    => __('Fancybox Size', 'redux-framework-demo'),
						'desc'     => __('Specify fancybox size in pixels', 'redux-framework-demo'),
						'default'  => array(
							'Width'   => '400',
							'Height'  => '900'
						),
					),
					array(
						'id'        => 'opt-caf-sharing-widget-sendtoafriend-link',
						'type'      => 'text',
						'title'     => __('Dealer Send Link', 'caf-redux-framework'),
						'desc'  	=> __('Link phone number to this page.', 'caf-redux-framework'),
						'default'   => '/forms/send-friend/',
					),
					array(
						'id'       => 'opt-caf-sharing-widget-sendtoafriend-icon',
						'type'     => 'text',
						'title'    => __('Dealer Send Icon', 'redux-framework-demo'),
						'default'  => '',
					),
					array(
						'id'     => 'section-sharing-links-widget-dealersend-end',
						'type'   => 'section',
						'indent' => false,
					),
					array(
					   'id' => 'section-sharing-links-widget-dealersms-start',
					   'type' => 'section',
					   'title' => __('Dealer SMS Link', 'redux-framework-demo'),
					   //'subtitle' => __('With the "section" field you can create indent option sections.', 'redux-framework-demo'),
					   'indent' => true
					),
					array(
						'id'        => 'opt-caf-sharing-widget-sms-text',
						'type'      => 'text',
						'title'     => __('Dealer SMS Text', 'caf-redux-framework'),
						'desc'  	=> __('Default SMS text for widget display.', 'caf-redux-framework'),
						'default'   => 'Send this link via SMS',
					),
					array(
						'id'        => 'opt-caf-sharing-widget-sms-link',
						'type'      => 'text',
						'title'     => __('Dealer SMS Link', 'caf-redux-framework'),
						'desc'  	=> __('Link sms to this page.', 'caf-redux-framework'),
						'default'   => 'sms://?body=Pacific%20Mazda%3A%20[caf-url]',
					),
					array(
						'id'       => 'opt-caf-sharing-widget-sms-icon',
						'type'     => 'text',
						'title'    => __('Dealer SMS Icon', 'redux-framework-demo'),
						//'desc'     => __('Basic media uploader with disabled URL input field.', 'redux-framework-demo'),
						//'subtitle' => __('Upload any media using the WordPress native uploader', 'redux-framework-demo'),
						'default'  => ''
					),
					array(
						'id'     => 'section-sharing-links-widget-dealersms-end',
						'type'   => 'section',
						'indent' => false,
					),
					array(
					   'id' => 'section-sharing-links-widget-facebook-start',
					   'type' => 'section',
					   'title' => __('Share to Facebook', 'redux-framework-demo'),
					   //'subtitle' => __('With the "section" field you can create indent option sections.', 'redux-framework-demo'),
					   'indent' => true
					),
					array(
						'id'        => 'opt-caf-sharing-widget-facebook-text',
						'type'      => 'text',
						'title'     => __('Widget Facebook Text', 'caf-redux-framework'),
						'desc'  	=> __('Default appraisal text for widget display.', 'caf-redux-framework'),
						'default'   => 'Share on Facebook',
					),
					array(
						'id'        => 'opt-caf-sharing-widget-facebook-link',
						'type'      => 'text',
						'title'     => __('Dealer Facebook Link', 'caf-redux-framework'),
						'desc'  	=> __('Link facebook to this page.', 'caf-redux-framework'),
						'default'   => 'https://www.facebook.com/sharer/sharer.php?u=[caf-url]',
					),
					array(
						'id'       => 'opt-caf-sharing-widget-facebook-icon',
						'type'     => 'text',
						'title'    => __('Dealer Facebook Icon', 'redux-framework-demo'),
						//'desc'     => __('Basic media uploader with disabled URL input field.', 'redux-framework-demo'),
						//'subtitle' => __('Upload any media using the WordPress native uploader', 'redux-framework-demo'),
						'default'  => '',
					),
					array(
						'id'     => 'section-sharing-links-widget-facebook-end',
						'type'   => 'section',
						'indent' => false,
					),
					array(
					   'id' => 'section-sharing-links-widget-print-start',
					   'type' => 'section',
					   'title' => __('Print Page', 'redux-framework-demo'),
					   //'subtitle' => __('With the "section" field you can create indent option sections.', 'redux-framework-demo'),
					   'indent' => true
					),
					array(
						'id'        => 'opt-caf-sharing-widget-print-text',
						'type'      => 'text',
						'title'     => __('Dealer Print Page Text', 'caf-redux-framework'),
						'desc'  	=> __('Default print page for widget display.', 'caf-redux-framework'),
						'default'   => 'Print this page',
					),
					array(
						'id'        => 'opt-caf-sharing-widget-print-link',
						'type'      => 'text',
						'title'     => __('Dealer Finance Inquiry Link', 'caf-redux-framework'),
						'desc'  	=> __('Link print action to this page.', 'caf-redux-framework'),
						'default'   => 'javascript:window.print();',
					),
					array(
						'id'       => 'opt-caf-sharing-widget-print-icon',
						'type'     => 'text',
						'title'    => __('Dealer Print Icon', 'redux-framework-demo'),
						//'desc'     => __('Basic media uploader with disabled URL input field.', 'redux-framework-demo'),
						//'subtitle' => __('Upload any media using the WordPress native uploader', 'redux-framework-demo'),
						'default'  => '',
					),
					array(
						'id'     => 'section-sharing-links-widget-print-end',
						'type'   => 'section',
						'indent' => false,
					),
					array(
						'id'     => 'section-sharing-links-widget-end',
						'type'   => 'section',
						'indent' => false,
					),
				),
			);

			$this->sections[] = array(
				'type' => 'divide',
			);

			$data_file = $CAF_Settings['opt-caf-api-inventory-data-location'];
			$this->sections[] = array(
				'title'     => __('<i class="el-icon-cloud-alt"></i> API Details', 'caf-redux-framework'),
				'desc'      => __('', 'caf-redux-framework'),
				'icon'      => 'el-icon-cloud-alt',
				// 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
				'fields'    => array(
					array(
					   'id' => 'section-api-details',
					   'type' => 'section',
					   //'title' => __('Indented Options', 'redux-framework-demo'),
					   //'subtitle' => __('With the "section" field you can create indent option sections.', 'redux-framework-demo'),
					   'indent' => false
					),
					array(
						'id'       => 'opt-caf-api-base-url',
						'type'     => 'text',
						'title'    => __('API Base URL', 'redux-framework-demo'),
						//'desc'     => __('Basic media uploader with disabled URL input field.', 'redux-framework-demo'),
						'subtitle' => __('Default: http://autofeeds.ca/bst', 'redux-framework-demo'),
						'default'  => CAF_API_URL,
					),
					array(
						'id'       => 'opt-caf-api-dealer-lookup',
						'type'     => 'select',
						'title'    => __('Dealer Lookup', 'redux-framework-demo'),
						'subtitle' => __('<a href="#" class="ajax button button-primary" id="getDealers">Refresh from API</a>', 'redux-framework-demo'),
						'desc'     => __('This setting does not affect the API configuration - it only provides an ID lookup to enter in the text fields above.', 'redux-framework-demo'),
						// Must provide key => value pairs for select options
						'options'  => caf_get_dealers(),
						'default'  => null,
					),
					array(
						'id'=>'opt-caf-api-dealer-ids',
						'type' => 'multi_text',
						'title' => __('Dealership IDs', 'redux-framework-demo'),
						'validate' => 'numeric',
						'subtitle' => __('Allows multiple dealerships', 'redux-framework-demo'),
						'desc' => __('Enter each numeric dealer ID in its own field. IDs can be found in the lookup box 
							in the next field. A taxonomy is created for each dealer entered here.', 'redux-framework-demo')
					),
					array(
						'id'       => 'opt-caf-api-inventory-data-location',
						'type'     => 'text',
						'title'    => __('API Inventory Data Storage Location', 'redux-framework-demo'),
						//'desc'     => __('Basic media uploader with disabled URL input field.', 'redux-framework-demo'),
						'desc' => __('Stores most recent API vehicle list call, <strong>
							relative to site root</strong>. <a target="_blank" href="'.$data_file.'">View file</a>
							<a href="'.plugins_url('CaordaAutofeeds_DataDump.php', __FILE__).'?file='.urlencode($data_file).'" target="_blank">Pretty Formatting</a>', 'redux-framework-demo'),
						'default'  => '/wp-content/uploads/caf/inventory-data.txt',
						//'validate' => 'caf_startslash_validate_callback_function',
					),
					array(
						'id'       => 'opt-caf-api-inventory-content',
						'type'     => 'raw',
						'title'    => __('API Vehicles Test', 'redux-framework-demo'),
						'subtitle' => __('<a href="#" class="ajax button button-primary" data-target="#api-vehicles-test-display" id="getVehicles">Refresh from API</a>', 'redux-framework-demo'),
						'mode'     => 'json',
						'theme'    => 'monokai',
						'desc'     => 'Content from the API Vehicle connection is displayed here for preview. Actual call
							data is stored in a text file at the location listed above.',
						//'default'  => caf_get_vehicles_dump(),
						'content' => '<p>This field is limited to a max display of 1000 characters - see the link for more detail.</p><div id="api-vehicles-test-display"><textarea>'.caf_get_vehicles_dump( $charlimit = 1000).'</textarea></div>',
					),
					array(
						'id'     => 'section-api-details-end',
						'type'   => 'section',
						'indent' => false,
					),
				),
			);

			$this->sections[] = array(
				'title'     => __('<i class="el-icon-podcast"></i> API Actions', 'caf-redux-framework'),
				'desc'      => __('', 'caf-redux-framework'),
				'icon'      => 'el-icon-podcast',
				// 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
				'fields'    => array(
					array(
					   'id' => 'section-api-actions',
					   'type' => 'section',
					   //'title' => __('Indented Options', 'redux-framework-demo'),
					   //'subtitle' => __('With the "section" field you can create indent option sections.', 'redux-framework-demo'),
					   'indent' => false
					),
					array(
						'id'       => 'opt-caf-api-enable-test-mode',
						'type'     => 'switch',
						'title'    => __('Enable Test Import Restrictions', 'redux-framework-demo'),
						'subtitle' => __('Restrict import by ID', 'redux-framework-demo'),
						'default'  => false,
						'on' 	   => 'Enable',
						'off'	   => 'Disable',
					),
					array(
					   'id' => 'section-caf-test-mode-start',
					   'type' => 'section',
					   'title' => __('Indented Options', 'redux-framework-demo'),
					   'subtitle' => __('With the "section" field you can create indent option sections.', 'redux-framework-demo'),
					   'indent' => true,
					   'required' => array('opt-caf-api-enable-test-mode', 'equals', '1'),
					),
					array(
						'id'=>'opt-caf-api-test-restrict-ids',
						'type' => 'multi_text',
						'title' => __('Restrict IDs', 'redux-framework-demo'),
						'desc' => __('Add vehicle IDs to restrict import to.', 'redux-framework-demo'),
					),
					array(
						'id'     => 'section-caf-test-mode-end',
						'type'   => 'section',
						'indent' => false,
					),
					array(
						'id'       => 'opt-caf-api-import-vehicles',
						'type'     => 'raw',
						'title'    => __('Perform Mass Import', 'redux-framework-demo'),
						'subtitle' => __('Pulls in all available vehicles.', 'redux-framework-demo'),
						//'desc'     => __('This is the description field for additional info.', 'redux-framework-demo'),
						'content'  => '
							<div class="caf-progress-bar default wrapper">
								<div class="caf-progress-bar progress info"></div>
							</div>
							<a id="cafBeginImport" href="#" class="button button-primary">Begin Import</a>
							<a id="cafResetImport" href="#" class="button button-secondary">Reset Import</a>
							<div class="import-status">Stopped</div>
							<br />
							<div class="import-messages"></div>
							<hr />
						'
					),
					array(
						'id'     => 'section-api-actions-end',
						'type'   => 'section',
						'indent' => false,
					),
					array(
						'id'       => 'opt-caf-cleanse-system',
						'type'     => 'raw',
						'title'    => __('System Cleansing', 'redux-framework-demo'),
						'subtitle' => __('Manages all imported content in the system.', 'redux-framework-demo'),
						//'desc'     => __('This is the description field for additional info.', 'redux-framework-demo'),
						'content'  => '
							<a id="cafRepopulateData" href="#" class="caf-ajax button button-primary" data-ajax-action="caf_repopulate_inventory">Prep for Data Re-Import</a>
							<a id="cafDeleteImages" href="#" class="caf-ajax button button-danger caf-delete" data-ajax-action="caf_delete_images" data-confirm="true">Delete Images</a>
							<a id="cafPurgeImages" href="#" class="caf-ajax button button-danger caf-delete" data-ajax-action="caf_purge_images" data-confirm="true">Purge Images</a>
							<a id="cafDeleteInventory" href="#" class="caf-ajax button button-danger caf-delete" data-ajax-action="caf_delete_inventory" data-confirm="true">Delete Inventory</a>
							<div class="delete-messages"></div>
							<hr />
						'
					),

				)
			);

			$this->sections[] = array(
				'icon'      => 'el-icon-graph',
				'title'     => __('<i class="el-icon-graph"></i> Status Logs', 'caf-redux-framework'),
				'fields'    => array(
					array(
					   'id' => 'section-status-logs',
					   'type' => 'section',
					   //'title' => __('Indented Options', 'redux-framework-demo'),
					   //'subtitle' => __('With the "section" field you can create indent option sections.', 'redux-framework-demo'),
					   'indent' => false
					),
					array(
						'id'        => 'opt-caf-num-logs',
						'type'      => 'slider',
						'title'     => __('Error Log Count', 'redux-framework-demo'),
						//'subtitle'  => __('This slider displays the value as a label.', 'redux-framework-demo'),
						'desc'      => __('Retains this many error logs in the database', 'redux-framework-demo'),
						"default"   => 30,
						"min"       => 1,
						"step"      => 1,
						"max"       => 1000,
						'display_value' => 'text'
					),
					array(
						'id'       => 'opt-caf-log-status',
						'type'     => 'raw',
						'title'    => __('Event Log: '.caf_count_logs().' entries', 'redux-framework-demo'),
						//'subtitle' => __('Subtitle text goes here.', 'redux-framework-demo'),
						//'desc'     => __('This is the description field for additional info.', 'redux-framework-demo'),
						'content'  => caf_get_logs(),
					),
					array(
						'id'     => 'section-status-logs-end',
						'type'   => 'section',
						'indent' => false,
					),
					array(
						'id'       => 'opt-caf-api-img-import',
						'type'     => 'textarea',
						'title'    => __('Images to Import', 'redux-framework-demo'),
						//'desc'     => __('Basic media uploader with disabled URL input field.', 'redux-framework-demo'),
						//'subtitle' => __('Upload any media using the WordPress native uploader', 'redux-framework-demo'),
						'default'  => '',
					),
					array(
						'id'       => 'opt-caf-api-import-time',
						'type'     => 'text',
						'title'    => __('Average Image Import Time', 'redux-framework-demo'),
						'desc'     => __('Avertage time per image to import. PHP max run time is '.ini_get('max_execution_time').' seconds.', 'redux-framework-demo'),
						//'subtitle' => __('Upload any media using the WordPress native uploader', 'redux-framework-demo'),
						'default'  => '',
					),
					array(
						'id'       => 'opt-caf-api-import-progress',
						'type'     => 'textarea',
						'validate' => 'html',
						'title'    => __('Import Progress', 'redux-framework-demo'),
						'desc'     => __('Last executed step in import process.', 'redux-framework-demo'),
						//'subtitle' => __('Upload any media using the WordPress native uploader', 'redux-framework-demo'),
						'default'  => '',
					),


				),
			);


			$schedules = array();
			foreach( (array) wp_get_schedules() as $k=>$sch) $schedules[$k] = $sch['display']." (".$k.")";

			$this->sections[] = array(
				'icon'      => 'el-icon-wrench',
				'title'     => __('<i class="el-icon-wrench"></i> Cron Tasks', 'caf-redux-framework'),
				'fields'    => array(
					array(
						'id'       => 'opt-caf-wp-cron-disable',
						'type'     => 'switch',
						'title'    => __('Disable WP Cron', 'redux-framework-demo'),
						'subtitle' => __('Toggle between WP cron and a manual cron job', 'redux-framework-demo'),
						'default'  => false,
						'on' 	   => 'Manual Cron',
						'off'	   => 'WP Cron',
					),
					array(
					    'id'       => 'opt-caf-cron-clean-logs',
					    'type'     => 'select',
					    'title'    => __('Clean Logs', 'redux-framework-demo'),
					    'options'  => $schedules,
					    'default'  => 'hourly',
					),
					array(
					    'id'       => 'opt-caf-cron-import-vehicles',
					    'type'     => 'select',
					    'title'    => __('Import Vehicles', 'redux-framework-demo'),
					    'options'  => $schedules,
					    'default'  => '4x_daily',
					),
					array(
					    'id'       => 'opt-caf-cron-import-images',
					    'type'     => 'select',
					    'title'    => __('Import Images', 'redux-framework-demo'),
					    'options'  => $schedules,
					    'default'  => 'two_minutes',
					),
					array(
					    'id'       => 'opt-caf-cron-check-rmv-vehicles',
					    'type'     => 'select',
					    'title'    => __('Check for Removed Vehicles', 'redux-framework-demo'),
					    'options'  => $schedules,
					    'default'  => '2x_daily',
					),
					array(
					    'id'       => 'opt-caf-cron-delete-term-vehicles',
					    'type'     => 'select',
					    'title'    => __('Delete Old Vehicles', 'redux-framework-demo'),
					    'options'  => $schedules,
					    'default'  => 'daily',
					),
					array(
						'id'       => 'opt-caf-import-image-batch',
						'type'     => 'text',
						'title'    => __('Image Batch Import Iterations', 'redux-framework-demo'),
						'desc'     => __('Number of vehicle image sets to process on each cron run. Integer value.', 'redux-framework-demo'),
						//'subtitle' => __('Upload any media using the WordPress native uploader', 'redux-framework-demo'),
						'default'  => '1',
						'validate' => 'numeric'
					),


				),
			);

			$this->sections[] = array(
				'type' => 'divide',
			);

			/*============================================
			=            Plugin Customization            =
			============================================*/

			$this->sections[] = array(
				'title'     => __('<i class="el-icon-puzzle"></i> FacetWP Customization', 'caf-redux-framework'),
				'desc'      => __('', 'caf-redux-framework'),
				'icon'      => 'el-icon-puzzle',
				// 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
				'fields'    => array(
					array(
						'id' => 'opt-caf-fwp-intro',
						'type' =>'raw',
						'content'  => "<p>Sample FacetWP configuration can be found at <a href='".plugins_url('caorda-autofeeds/sampledata/facetwp_example.json')."''>facetwp_example.json</a></p>"
					),
					array(
					   'id' => 'section-fwp-widget-start',
					   'type' => 'section',
					   'title' => __('FWP Widget Options', 'redux-framework-demo'),
					   //'subtitle' => __('With the "section" field you can create indent option sections.', 'redux-framework-demo'),
					   'indent' => true 
					),
					array(
						'id'=>'opt-caf-fwp-filter-widget-filter-shortcodes',
						'type' => 'multi_text',
						'title' => __('FWP Filter Shortcodes', 'redux-framework-demo'),
						//'subtitle' => __('If you enter an invalid color it will be removed. Try using the text "blue" as a color.  ;)', 'redux-framework-demo'),
						'desc' => __('Enter a FWP filter shortcode per box in the format <em>title:shortcode</em>. <a href="https://facetwp.com/documentation/" target="_blank">FWP Documentation</a>', 'redux-framework-demo'),
						'default' => array(
							'Price:price_range',
							'Year:year_range',
							'Make:make',
							'Model:model',
							'Color:color'
						),
					),
					array(
						'id'       => 'opt-caf-fwp-enable-dropfilters',
						'type'     => 'switch',
						'title'    => __('Enable Dropdown Filters', 'redux-framework-demo'),
						//'subtitle' => __('Look, it\'s on!', 'redux-framework-demo'),
						'desc' => __('Adds drop expansion to the filters list', 'redux-framework-demo'),
						'default'  => true,
						'on' => 'Enabled',
						'off' => 'Disabled'
					),
					array(
						'id'       => 'opt-caf-fwp-title-display',
						'type'     => 'button_set',
						'title'    => __('Filter Title Display', 'redux-framework-demo'),
						//'subtitle' => __('No validation can be done on this field type', 'redux-framework-demo'),
						'desc'     => __('Determine how and where filter titles appear. For adding to select option, visit the <a href="/wp-admin/options-general.php?page=facetwp">FacetWP settings page</a>.', 'redux-framework-demo'),
						'multi'    => true,
						'options' => array(
							'external' => 'External Label',
							'internal' => 'First Select Option',
						 ),
						'default' => array('external'),
					),
					array(
						'id'       => 'opt-caf-fwp-filter-title-icon',
						'type'     => 'text',
						'title'    => __('Filter Title Icon', 'redux-framework-demo'),
						'desc'     => __('Icon displayed in the title box', 'redux-framework-demo'),
						//'subtitle' => __('Upload any media using the WordPress native uploader', 'redux-framework-demo'),
						'default'  => '<span>&raquo;</span>',
					),
					array(
						'id'       => 'opt-caf-fwp-filter-widget-filter-reseturl',
						'type'     => 'text',
						'title'    => __('Reset Filters URL', 'redux-framework-demo'),
						//'desc'     => __('Basic media uploader with disabled URL input field.', 'redux-framework-demo'),
						//'subtitle' => __('Upload any media using the WordPress native uploader', 'redux-framework-demo'),
						'default'  => '/',
					),
					array(
						'id'     => 'section-fwp-widget-end',
						'type'   => 'section',
						'indent' => false,
					),
					array(
					   'id' => 'section-fwp-search-start',
					   'type' => 'section',
					   'title' => __('FWP Search Options', 'redux-framework-demo'),
					   //'subtitle' => __('With the "section" field you can create indent option sections.', 'redux-framework-demo'),
					   'indent' => false
					),
					array(
						'id'       => 'opt-caf-fwp-scrolltop-refresh',
						'type'     => 'switch',
						'title'    => __('Scroll To Top', 'redux-framework-demo'),
						//'subtitle' => __('Look, it\'s on!', 'redux-framework-demo'),
						'desc' => __('Scroll back to top of page when search query is changed', 'redux-framework-demo'),
						'default'  => true,
						'on' => 'Enabled',
						'off' => 'Disabled'
					),
					array(
						'id'=>'opt-caf-fwp-items-per-page',
						'type' => 'multi_text',
						'title' => __('Items Per Page Dropdown', 'redux-framework-demo'),
						//'subtitle' => __('If you enter an invalid color it will be removed. Try using the text "blue" as a color.  ;)', 'redux-framework-demo'),
						'desc' => __('Adds a dropdown to the <em>facetwp_sort_html()</em> hook using format <em>value:label</em>', 'redux-framework-demo'),
						'default' => array(
							'10:10 Results',
							'25:25 Results',
							'50:50 Results',
							'-1:View All',
						),
					),
					array(
						'id'       => 'opt-caf-fwp-pager-first-text',
						'type'     => 'text',
						'title'    => __('Pager First Text', 'redux-framework-demo'),
						//'desc'     => __('Basic media uploader with disabled URL input field.', 'redux-framework-demo'),
						//'subtitle' => __('Upload any media using the WordPress native uploader', 'redux-framework-demo'),
						'default'  => '&laquo; First',
					),
					array(
						'id'       => 'opt-caf-fwp-pager-last-text',
						'type'     => 'text',
						'title'    => __('Pager Last Text', 'redux-framework-demo'),
						//'desc'     => __('Basic media uploader with disabled URL input field.', 'redux-framework-demo'),
						//'subtitle' => __('Upload any media using the WordPress native uploader', 'redux-framework-demo'),
						'default'  => 'Last &raquo;',
					),
					array(
						'id'=>'opt-caf-fwp-sort-options',
						'type' => 'multi_text',
						'title' => __('Sort Options', 'redux-framework-demo'),
						//'subtitle' => __('If you enter an invalid color it will be removed. Try using the text "blue" as a color.  ;)', 'redux-framework-demo'),
						'desc' => __('Alters sort options using the <em>facetwp_sort_options()</em> hook. Fields here are JSON-encoded.
							<a href="https://facetwp.com/documentation/facetwp_sort_options/" target="_blank">Documentation</a><br />Example: <pre>{"title_asc": { "label": "Title (A-Z)", "query_args": { "orderby": "title", "order": "ASC" } } }</pre>', 'redux-framework-demo'),
						'default' => array(
							'{"default": { "label": "Sort by", "query_args": [] } }',
							'{"title_asc": { "label": "Title (A-Z)", "query_args": { "orderby": "title", "order": "ASC" } } }',
							'{"title_desc": { "label": "Title (Z-A)", "query_args": { "orderby": "title", "order": "DESC" } } }',
							'{"date_desc": { "label": "Date (Newest)", "query_args": { "orderby": "date", "order": "DESC" } } }',
							'{"date_asc": { "label": "Date (Oldest)", "query_args": { "orderby": "date",  "order": "ASC" } } }',
							'{"price_asc": { "label": "Price (Lowest)", "query_args": { "orderby": "meta_value_num", "meta_key":"caf_regular_price", "order": "ASC" } } }',
							'{"price_desc": { "label": "Price (Highest)", "query_args": { "orderby": "meta_value_num", "meta_key":"caf_regular_price", "order": "DESC" } } }',
							'{"year_asc": { "label": "Year (Oldest)", "query_args": { "orderby": "meta_value_num", "meta_key":"caf_year", "order": "ASC" } } }',
							'{"year_desc": { "label": "Year (Newest)", "query_args": { "orderby": "meta_value_num", "meta_key":"caf_year", "order": "DESC" } } }',
							'{"make_asc": { "label": "Make (Desc)", "query_args": { "orderby": "meta_value_num", "meta_key":"caf_make", "order": "ASC" } } }',
							'{"make_desc": { "label": "Make (Asc)", "query_args": { "orderby": "meta_value_num", "meta_key":"caf_make", "order": "DESC" } } }',
							'{"model_asc": { "label": "Model (Desc)", "query_args": { "orderby": "meta_value_num", "meta_key":"caf_model", "order": "ASC" } } }',
							'{"model_desc": { "label": "Model (Asc)", "query_args": { "orderby": "meta_value_num", "meta_key":"caf_model", "order": "DESC" } } }',
						),
					),
					array(
						'id'     => 'section-fwp-search-end',
						'type'   => 'section',
						'indent' => false,
					),

				),
			);
			$this->sections[] = array(
				'title'     => __('<i class="el-icon-warning-sign"></i> ACF Fields', 'caf-redux-framework'),
				'desc'      => __('', 'caf-redux-framework'),
				'icon'      => 'el-icon-warning-sign',
				// 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
				'fields'    => array(
					array(
					   'id' => 'section-acf-fields',
					   'type' => 'section',
					   //'title' => __('Indented Options', 'redux-framework-demo'),
					   //'subtitle' => __('With the "section" field you can create indent option sections.', 'redux-framework-demo'),
					   'indent' => false
					),
					array(
						'id'=>'opt-caf-acf-field-pairs',
						'type' => 'multi_text',
						'title' => __('Advanced Custom Field pairs', 'redux-framework-demo'),
						//'subtitle' => __('If you enter an invalid color it will be removed. Try using the text "blue" as a color.  ;)', 'redux-framework-demo'),
						'desc' => __('Enter <strong>ACF_GUID:fieldname</strong> pairs here. ACF fields struggle with lookup during
							init routines so we need to manually bind the pairs here.', 'redux-framework-demo'),
						'default' => caf_acf_get_field_pairs(),
					),
					array(
						'id'     => 'section-acf-fields-end',
						'type'   => 'section',
						'indent' => false,
					),
				)
			);

			$this->sections[] = array(
				'type' => 'divide',
			);

			$this->sections[] = array(
				'title'     => __('<i class="el-icon-refresh"></i> Import / Export', 'caf-redux-framework'),
				'desc'      => __('Import and Export your Redux Framework settings from file, text or URL.', 'caf-redux-framework'),
				'icon'      => 'el-icon-refresh',
				'fields'    => array(
					array(
						'id'            => 'opt-import-export',
						'type'          => 'import_export',
						'title'         => 'Import Export',
						'subtitle'      => 'Save and restore your Redux options',
						'full_width'    => false,
					),
				),
			);

			$this->sections[] = array(
				'type' => 'divide',
			);

			$this->sections[] = array(
				'title'     => __('<i class="el-icon-refresh"></i> About CAF', 'caf-redux-framework'),
				'desc'      => __('General plugin information.', 'caf-redux-framework'),
				'icon'      => 'el-icon-list-alt',
				'fields'    => array(
					array(
						'id'       => 'opt-caf-about-caf',
						'type'     => 'raw',
						'title'    => __('About CAF', 'redux-framework-demo'),
						//'subtitle' => __('Subtitle text goes here.', 'redux-framework-demo'),
						//'desc'     => __('This is the description field for additional info.', 'redux-framework-demo'),
						'content'  => '<div><p>Caorda AutoFeeds is created under the GPLv3 License and managed with love
						by <a href="http://caorda.com?source=plugin&name=CaordaAutoFeeds" target="_blank">Caorda Web Solutions</a>.</p>

						</div>',
					),
					array(
						'id'       => 'opt-caf-todo-caf',
						'type'     => 'raw',
						'title'    => __('Todo List', 'redux-framework-demo'),
						//'subtitle' => __('Subtitle text goes here.', 'redux-framework-demo'),
						//'desc'     => __('This is the description field for additional info.', 'redux-framework-demo'),
						'content'  => '<div>
							<ol>
								<li>Add default featured image to options panel and imported vehicles</li>
								<li>Add option for testing small-batch import</li>
								<li>Add debugging information for future developers</li>
								<li>Add # column control for Summary Features field</li>
								<li>Add a dashboard widget for stats and recent activity</li>
								<li>Integrate analytics and view tracking</li>
							</ol>
						</div>',
					),
					array(
						'id'       => 'opt-caf-help-caf',
						'type'     => 'raw',
						'title'    => __('Quick Tips', 'redux-framework-demo'),
						//'subtitle' => __('Subtitle text goes here.', 'redux-framework-demo'),
						//'desc'     => __('This is the description field for additional info.', 'redux-framework-demo'),
						'content'  => '<div>
							<ul>
								<li>Manually step through the import process at <a href="/wp-admin/admin-ajax.php?action=caf_api_import_controller&debug" target="_blank">/wp-admin/admin-ajax.php?action=caf_api_import_controller&debug</a></li>
								<li>Add these to wp-config.php:
									<pre>define("DISABLE_WP_CRON", "true"); define("WP_CRON_LOCK_TIMEOUT", 60*3);</pre>
								</li>
							</ul>
						</div>',
					),
				),
			);



			if (file_exists(trailingslashit(dirname(__FILE__)) . 'README.html')) {
				$tabs['docs'] = array(
					'icon'      => 'el-icon-book',
					'title'     => __('Documentation', 'caf-redux-framework'),
					'content'   => nl2br(file_get_contents(trailingslashit(dirname(__FILE__)) . 'README.html'))
				);
			}
		}

		public function setHelpTabs() {

			// Custom page help tabs, displayed using the help API. Tabs are shown in order of definition.
			$this->args['help_tabs'][] = array(
				'id'        => 'redux-help-tab-1',
				'title'     => __('Theme Information 1', 'caf-redux-framework'),
				'content'   => __('<p>This is the tab content, HTML is allowed.</p>', 'caf-redux-framework')
			);

			$this->args['help_tabs'][] = array(
				'id'        => 'redux-help-tab-2',
				'title'     => __('Theme Information 2', 'caf-redux-framework'),
				'content'   => __('<p>This is the tab content, HTML is allowed.</p>', 'caf-redux-framework')
			);

			// Set the help sidebar
			$this->args['help_sidebar'] = __('<p>This is the sidebar content, HTML is allowed.</p>', 'caf-redux-framework');
		}

		/**
		*
		*  All the possible arguments for Redux.
		*  For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
		*
		 * */
		public function setArguments() {

			$theme = wp_get_theme(); // For use with some settings. Not necessary.

			$this->args = array(
				// TYPICAL -> Change these values as you need/desire
				'opt_name'          => 'CAF_Settings',            // This is where your data is stored in the database and also becomes your global variable name.
				'display_name'      => 'Caorda AutoFeeds',     // Name that appears at the top of your panel
				'display_version'   => '1.2.0',  // Version that appears at the top of your panel
				'menu_type'         => 'menu',                  //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
				'allow_sub_menu'    => true,                    // Show the sections below the admin menu item or not
				'menu_title'        => __('Caorda Autofeeds', 'caf-redux-framework'),
				'page_title'        => __('Caorda Autofeeds', 'caf-redux-framework'),

				// You will need to generate a Google API key to use this feature.
				// Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
				'google_api_key' => '', // Must be defined to add google fonts to the typography module
				'async_typography'  => false,                    // Use a asynchronous font on the front end or font string
				'admin_bar'         => false,                    // Show the panel pages on the admin bar
				'global_variable'   => 'CAF_Settings',                      // Set a different name for your global variable other than the opt_name
				'dev_mode'          => false,                    // Show the time the page took to load, etc
				'customizer'        => false,                    // Enable basic customizer support

				// OPTIONAL -> Give you extra features
				'page_priority'     => null,                    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
				'page_parent'       => '',            // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
				'page_permissions'  => 'manage_caf_options',        // Permissions needed to access the options panel.
				'menu_icon'         => plugins_url('img/autofeeds-logo_16px.png', __FILE__),                      // Specify a custom URL to an icon
				'last_tab'          => '',                      // Force your panel to always open to a specific tab (by id)
				'page_icon'         => 'icon-themes',           // Icon displayed in the admin panel next to your menu_title
				'page_slug'         => 'caf_options',              // Page slug used to denote the panel
				'save_defaults'     => true,                    // On load save the defaults to DB before user clicks save or not
				'default_show'      => false,                   // If true, shows the default value next to each field that is not the default value.
				'default_mark'      => '',                      // What to print by the field's title if the value shown is default. Suggested: *
				'show_import_export' => true,                   // Shows the Import/Export panel when not used as a field.

				// CAREFUL -> These options are for advanced use only
				'transient_time'    => 60 * MINUTE_IN_SECONDS,
				'output'            => true,                    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
				'output_tag'        => true,                    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
				'footer_credit'     => '<strong>Caorda Autofeeds</strong> created by <a href="http://caorda.com?source=plugin&name=CaordaAutoFeeds" target="_blank">Caorda Web Solutions</a>',                   // Disable the footer credit of Redux. Please leave if you can help it.

				// FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
				'database'              => '', // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
				'system_info'           => false, // REMOVE

				// HINTS
				'hints' => array(
					'icon'          => 'icon-question-sign',
					'icon_position' => 'right',
					'icon_color'    => 'lightgray',
					'icon_size'     => 'normal',
					'tip_style'     => array(
						'color'         => 'light',
						'shadow'        => true,
						'rounded'       => false,
						'style'         => '',
					),
					'tip_position'  => array(
						'my' => 'top left',
						'at' => 'bottom right',
					),
					'tip_effect'    => array(
						'show'          => array(
							'effect'        => 'slide',
							'duration'      => '500',
							'event'         => 'mouseover',
						),
						'hide'      => array(
							'effect'    => 'slide',
							'duration'  => '500',
							'event'     => 'click mouseleave',
						),
					),
				)
			);


			// SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
			/*
			$this->args['share_icons'][] = array(
				'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
				'title' => 'Visit us on GitHub',
				'icon'  => 'el-icon-github'
				//'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
			);
			*/
			$this->args['share_icons'][] = array(
				'url'   => 'https://www.facebook.com/caorda',
				'title' => 'Like us on Facebook',
				'icon'  => 'el-icon-facebook'
			);
			$this->args['share_icons'][] = array(
				'url'   => 'http://twitter.com/reduxframework',
				'title' => 'Follow us on Twitter',
				'icon'  => 'el-icon-twitter'
			);
			$this->args['share_icons'][] = array(
				'url'   => 'https://twitter.com/caorda',
				'title' => 'Find us on LinkedIn',
				'icon'  => 'el-icon-linkedin'
			);

			// Panel Intro text -> before the form
			if (!isset($this->args['global_variable']) || $this->args['global_variable'] !== false) {
				if (!empty($this->args['global_variable'])) {
					$v = $this->args['global_variable'];
				} else {
					$v = str_replace('-', '_', $this->args['opt_name']);
				}
				//$this->args['intro_text'] = sprintf(__('<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'caf-redux-framework'), $v);
			} else {
				//$this->args['intro_text'] = __('<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'caf-redux-framework');
			}

			// Add content after the form.
			//$this->args['footer_text'] = __('<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'caf-redux-framework');
		}

	}

	global $CAFreduxConfig;
	$CAFreduxConfig = new Redux_Framework_CAF_config();
}

/**
  *Custom function for the callback referenced above
 */
if (!function_exists('redux_my_custom_field')):
	function redux_my_custom_field($field, $value) {
		print_r($field);
		echo '<br/>';
		print_r($value);
	}
endif;

/**
  *Custom function for the callback validation referenced above
 * */
if (!function_exists('redux_validate_callback_function')):
	function redux_validate_callback_function($field, $value, $existing_value) {
		$error = false;
		$value = 'just testing';

		/*
		  do your validation

		  if(something) {
			$value = $value;
		  } elseif(something else) {
			$error = true;
			$value = $existing_value;
			$field['msg'] = 'your custom error message';
		  }
		 */

		$return['value'] = $value;
		if ($error == true) {
			$return['error'] = $field;
		}
		return $return;
	}
endif;


function CAFaddPanelCSS() {

	wp_enqueue_script('thickbox',null,array('jquery'));
	wp_enqueue_style('thickbox.css', '/'.WPINC.'/js/thickbox/thickbox.css', null, '1.0');

	wp_enqueue_style('caf-redux-custom-css', plugins_url('css/redux-custom.css', __FILE__), array( 'redux-css' ), time(), 'all' );

	wp_enqueue_script( 'caf-redux-custom-js', plugins_url('js/redux-custom.js', __FILE__), array('jquery'), '1.0', true );
}
// This example assumes your opt_name is set to redux_demo, replace with your opt_name value
add_action( 'redux/page/CAF_Settings/enqueue', 'CAFaddPanelCSS' );

// Returns a table format of template files
function get_template_file_list_html(){

	$output = '<table><thead><tr><th>File Name</th><th>Size</th><th>Location</th><th>Active</th></tr></thead><tbody>';

	// Get initial template files
	$template_dir = dirname(__FILE__).'/caf-templates';
	$templates = glob( $template_dir.'/*.php');

	// Check for existing files
	$target_dir = get_stylesheet_directory().'/caf-templates';
	$existing_files = glob($target_dir.'/*.php');

	$total_files = array_filter(array_merge($templates, (array) $existing_files) );

	// Preprocess files
	$final_files = array();
	foreach($total_files as $k=>$file){
		$name = basename($file);

		// Check if we're overriding plugin files
		foreach($final_files as &$f){
			if( ($name == $f['name']) && ($f['location'] == 'Plugin') ){
				$f['active'] = 'Inactive';
			}
		}

		$final_files[] = array(
			'full' => $file,
			'name' => $name,
			'location' => strpos($file, 'caorda-autofeeds') ? 'Plugin' : 'Theme',
			'size' => human_filesize( filesize($file)),
			'active' => 'Active'
		);
	}

	foreach($final_files as $file){
		$active = strtolower($file['active']);

		$output .= '<tr class="'.$active.'"><td>'.$file['name'].'</td><td>'.$file['size'].'</td><td>'.$file['location'].'</td><td>'.$file['active'].'</td></tr>';
	}

	$output .= '</tbody></table>';

	return $output;
} // get_template_file_list_html

function human_filesize($bytes, $decimals = 0) {
  $sz = 'BKMGTP';
  $factor = floor((strlen($bytes) - 1) / 3);
  return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
}

/* Returns WP_Logging entries */
/*

	TODO: Convert this to an Ajax action
	We have to early-init $wp_query here to make this work, but the taxonomy
	relations fail for $type in the WP_Logging:: class. Consider dumping these
	error logs out to a text file, or init the content at a stage later than
	the Redux options are loaded.

*/
function caf_get_logs($type=NULL){

	// Make sure that we've init a query already
	global $wp_query;
	if(!$wp_query) $wp_query = new WP_Query();

	$logs = WP_Logging::get_connected_logs( array('posts_per_page'=> -1, 'log_type' => $type) );

	$output = '<div class="caf-log-container"><table><thead><tr>
		<th class="date">Date</th><th>Description</th><th>ID</th></tr></thead><tbody>';

	if($logs){
		foreach($logs as $log){

			// UNAVAILABLE!
			//$terms = wp_get_post_terms( $log->ID, 'wp_log_type');
			//echo '<pre>'.print_r($terms, true).'</pre>';

			$output .= '<tr>';
			$output .= '<td><span class="caf-log-date">'.date('Y-m-d', strtotime($log->post_date)).'</span>
				<span class="caf-log-time">'.date('g:i:s a', strtotime($log->post_date)).'</span></td>
				<td><h4 class="caf-log-title">'.$log->post_title.'</h4><div class="caf-log-content">'
					.str_replace('&#8221;', '"', apply_filters('the_content', $log->post_content) ).'</div></td>
				<td>'.$log->ID.'</td>';

			$output .= '</tr>';
		}
	}


	$output .= '</tbody></table></div>';

	return $output;
}

function caf_count_logs(){
	global $wpdb;
	$count = $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->posts WHERE post_type= 'wp_log'" );
	return $count;
}
