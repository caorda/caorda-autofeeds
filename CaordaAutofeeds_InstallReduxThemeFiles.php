<?php

	$path = explode('wp-content', dirname(__FILE__));
	require_once( $path[0].'wp-admin/admin.php');

	// Auth check
	if ( !is_user_logged_in() || !current_user_can('manage_caf_options') ) die();

	if( $_REQUEST['state'] == 'interface'):
?>

<div class="caf-installer-dialog" style="background:#f1f1f1;padding:30px;">
	<h2>Install Caorda Autofeed Files</h2>
	<p>Clicking the following button will install CAF template files into your current
		active theme. It will <strong>NOT</strong> overwrite existing files - do to that,
		click the reset button instead.</p>

	<p>
		<a id="caf-template-reset" class="buttons button-secondary">Reset Template Files</a>
		<a id="caf-template-install" class="buttons button-primary">Install Template Files</a>
		<span class="loader" style="background:url(/wp-admin/images/spinner.gif);display:none;width:20px;height:20px;margin:0 20px;1"></span>
	</p>

</div>

<script type="text/javascript">
jQuery(function($) {

	$('.buttons').click(function() {
		var actionState = 'install';	// default action
		$btn = $(this);

		if( $btn.attr('id') == 'caf-template-reset'){
			if( confirm('This action will remove any existing CAF templates in your theme folder. Are you'
				+' sure you want to continue?')){
				actionState = 'reset';
			} else{
				return false;
			}

		}

		$.ajax({
			type: "POST",
			url: "<?php echo $_SERVER['PHP_SELF']; ?>",
			data: { state: actionState},
			beforeSend: function(){
				$btn.parent().find('.loader').css('display', 'inline-block');
			}
		}).done(function( msg ) {

			var data = $.parseJSON(msg);
			$btn.parent().find('.loader').css('display', 'none');

			console.log(data);

			if( typeof( data) != 'object'){
				alert('Error - installation failed. Please try copying files via FTP. Message: '+msg);
			} else{
				// Successful ajax

				if( data.status == 'success'){
					alert('Success - '+data.message);
					tb_remove();
					location.reload(false);
				}
			}

		}); // ajax done


	});

});
</script>

<?php elseif($_REQUEST['state'] == 'install' || $_REQUEST['state'] == 'reset'):

	// Init messages
	$output = array( 'status' => 'init', 'message' => 'Starting installation...' );

	// Get initial template files
	$template_dir = dirname(__FILE__).'/caf-templates';
	$templates = glob( $template_dir.'/*.php');

	// Check for existing files
	$target_dir = get_stylesheet_directory().'/caf-templates';
	$existing_files = glob($target_dir.'/*.php');

	// Take care of the reset action
	if( ($_REQUEST['state'] == 'reset') && is_dir($target_dir) ){
		$output = array( 'status' => 'cleaning', 'message' => 'Cleaning existing files...' );
		clean_directory($target_dir);

		// Re-init existing files
		$existing_files = glob($target_dir.'/*.php');
	}

	$output = array( 'status' => 'filtering', 'message' => 'Filtering existing files...' );

	// Remove duplicate files from copy list
	if( !empty($templates) && !empty($existing_files)){
		foreach($templates as &$tpl){
			foreach($existing_files as $ext){
				if( basename($tpl) == basename($ext)){
					$tpl = null;
					continue;
				}
			} // foreach $existing files
		} // foreach $templates
	} // if plugin templates exist

	$templates = array_filter($templates);

	// Ensure directory exists
	if( !is_dir($target_dir)) mkdir($target_dir);

	$output = array( 'status' => 'copying', 'message' => 'Moving template files to destination...' );

	// Copy files to target directory
	$files_copied = array();
	foreach($templates as $tpl){
		if(copy($tpl, $target_dir.'/'.basename($tpl) ) ){
			$files_copied[] = basename($tpl);
		}
	}


	// Output status info / message
	if( count($files_copied)){
		$message = 'CAF Templates Installed! '.count($files_copied).' files copied to destination.';
		$output = array( 'status' => 'success', 'message' => $message, 'data'=>$files_copied);
	} else{
		$output = array( 'status' => 'success', 'message' => 'Process complete - zero files moved.' );
	}

	if( $_REQUEST['state'] == 'reset') $output['message'] .= ' Template reset success.';

	echo json_encode($output);

endif;


// http://stackoverflow.com/questions/3349753/delete-directory-with-files-in-it
function clean_directory($dir){
	//$dir = 'samples' . DIRECTORY_SEPARATOR . 'sampledirtree';
	$it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
	$files = new RecursiveIteratorIterator($it,
				 RecursiveIteratorIterator::CHILD_FIRST);

	foreach($files as $file) {
		if ($file->getFilename() === '.' || $file->getFilename() === '..') {
			continue;
		}
		if ($file->isDir()){
			rmdir($file->getRealPath());
		} else {
			unlink($file->getRealPath());
		}
	}
	rmdir($dir);
}