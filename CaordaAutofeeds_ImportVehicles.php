<?php

//include_once('CaordaAutofeeds_TaxMetaClass.php');

global $count;

$count = array( 'parsed' => 0, 'ignored' => 0, 'updated' => 0, 'added'=>0, 'deleted'=>0);

/*===========================================

=            CAF Import Vehicles            =

===========================================*/



function caf_import_vehicles_action(){



	global $acf_fields, $count, $CAF_Settings, $status;		// used later in field updates

	$CAF_Settings = get_option('CAF_Settings', array());

	$vehicle_file = untrailingslashit(ABSPATH).$CAF_Settings['opt-caf-api-inventory-data-location'];



	$time_total = 0;

	$images_to_import = array();





	// Make sure file exists

	if( !file_exists($vehicle_file)){

		WP_Logging::add( $title = 'CAF Vehicle Import', 'Vehicle Import Failed: no dump file exists. '.$vehicle_file, $parent = 0, $type = 'error' );

		return json_encode( array('status' => 'error', 'message' => 'Vehicle Import Failed: no dump file exists') );

	}





	// Get contents, decode

	$dump = json_decode(file_get_contents($vehicle_file), true);

	//echo '<pre>Dump: '.print_r($dump[0], true).'</pre>';



	// Set up ACF field associations

	if(empty($acf_fields)) $acf_fields = load_acf_fields();



	/*()

	foreach($CAF_Settings['opt-caf-acf-field-pairs'] as $pair){

		$pair = explode(':', $pair);

		$acf_fields[$pair[1]] = $pair[0];

		//$acf_fields[$pair[1]] = $pair[1];

		$acf_fields[$pair[0]] = $pair[1];

	}

	*/

	//echo '<pre>'.print_r( $acf_fields, true).'</pre>';





	// TODO: Nest another foreach $dump outside of this to iterate each dealership
	// Consider making a manual SQL query instead of the multiple joins that WP performs?
	$count_page = 10;
	$index = 0;

	$posts = array();
	do {
		$find_ids = get_posts( array( 'post_type' => 'caf_inventory', 'posts_per_page' => $count_page, 'offset' => ($count_page * $index++)));
		foreach ($find_ids as $find_id) {
			$posts[] = get_post_meta($find_id->ID, 'caf_autofeeds_id', true);
		}
	} while (count($find_ids) == $count_page);

	//echo '<pre>'.print_r($posts, true).'</pre>';

	// Loop through each vehicle!

	foreach($dump as $dealership){

		foreach($dealership as $afid=>$vehicle){

			$time_start = microtime(true);



			// Debug restrict by given IDs

			if( $CAF_Settings['opt-caf-api-enable-test-mode'] && !in_array($afid, $CAF_Settings['opt-caf-api-test-restrict-ids'] ) ){ // TEMP FAKE RESTRICT

				continue;

			}

			//$find_ids = get_posts( array( 'post_type'=>'caf_inventory', 'meta_key'=>'caf_autofeeds_id', 'meta_value' => $afid));



			//echo '<pre>FOUND POSTS: '.print_r($find_ids, true).'</pre>';

			wp_reset_postdata();



			// If we have a duplicate, compare posts

			if( in_array($afid, $posts) ){

				// Existing vehicle? Make updates.
				$find_ids = get_posts( array( 'post_type'=>'caf_inventory', 'meta_key'=>'caf_autofeeds_id', 'meta_value' => $afid));
				$id = 0;
				if (count($find_ids)) {
					$id = $find_ids[0]->ID;
				}


				// Compare checksums
				if ($id == 0) {
					$old_checksum = 0;
				} else {
					$old_checksum = get_field( $acf_fields['caf_checksum'], $id);
				}

				$new_checksum = md5(json_encode( $vehicle));



				//echo '<pre>Old: '.print_r($old_checksum, true).'</pre>';

				//echo '<pre>New: '.print_r($new_checksum, true).'</pre>';



				// TODO: Change back to != when ready for production

				if( $old_checksum != $new_checksum){

					//echo '<pre>'.print_r( array('mismatch', $id, $old_checksum, $new_checksum), true).'</pre>';



					// Update existing vehicle fields

					caf_vehicle_update_fields($id, $vehicle);



					// Track images to import

					if( !empty($vehicle['Photos'])){



						// Load post ID into array, not photo text

						$images_to_import[] = $id;



					}



					$count['updated']++;



				} else {



					// Checksums matched, ignore vehicle

					$count['ignored']++;

				}





			} else{

				// New vehicle? make insertion!

				$data = array(

					'post_type' => 'caf_inventory',

					'post_title' => implode(' ', array($vehicle['Year'], $vehicle['Make'], $vehicle['Model'])),

					'post_status' => 'publish',

					'post_author' => 0,

					'ping_status' => 'closed',

					'comment_status' => 'closed'

				);

				$id = wp_insert_post($data);



				// Set default ACF field values

				$update_fields = array(

					'caf_display_summary' 	=> 'opt-caf-detail-show-summary',

					'caf_display_features'	=> 'opt-caf-detail-show-summary',

					'caf_display_description'=> 'opt-caf-detail-show-description',

					'caf_display_content'	=> 'opt-caf-detail-show-page-content',

					'caf_display_galleries'	=> array('opt-caf-detail-show-small-gallery', 'opt-caf-detail-show-large-gallery'),

					'caf_display_cta'		=> 'opt-caf-detail-show-cta',

				);



				foreach($update_fields as $acf_field=>$redux_field){

					if( is_array($redux_field)) $data = $CAF_Settings[$redux_field[0]] || $CAF_Settings[$redux_field[1]];

					else $data = $CAF_Settings[$redux_field];



					update_field( $acf_fields[$acf_field], $data, $id );

				}





				// Track images to import

				if( !empty($vehicle['Photos'])){

					//$images_to_import[$id] = $id;

					$images_to_import[] = $id;

					/*$images_to_import[$id] = array(

						'post_id' => $id,

						'vehicle_id' => $vehicle['VehicleID'],

						'photos' => $vehicle['Photos'],

					);*/

				}



				// Then update custom fields

				caf_vehicle_update_fields($id, $vehicle);

				$count['added']++;

			}





			$count['parsed']++;

			$time_end = microtime(true);

			$time_total += $time_end - $time_start;

			$count['time_elapsed'] = $time_total;

			$count['time_start'] = $time_start;

			$count['time_end'] = $time_end;



		} // foreach vehicles

	} // foreach dealership



	if(!empty($images_to_import)) arsort($images_to_import);



	//echo '<pre>'.print_r($count, true).'</pre>';

	add_action('admin_notices', 'caf_import_count_msg');

	// TODO: Check for deleted vehicles



	$api_img_import = (array) json_decode($CAF_Settings['opt-caf-api-img-import'], true);

	$api_img_import = array_merge($images_to_import, $api_img_import);

	$api_img_import = array_values(array_unique($api_img_import));



	$CAF_Settings['opt-caf-api-img-import'] = json_encode($api_img_import);

	update_option( 'CAF_Settings', $CAF_Settings );



	$output = array('status' => 'success', 'counts'=>$count);

	WP_Logging::add( $title = 'CAF Vehicle Import', 'Vehicle Import: '.json_encode($output), $parent = 0, $type = 'status' );



	return $output;



}



/*=====================================================

=            CAF Update Vehicle ACF Fields            =

=====================================================*/



function caf_vehicle_update_fields($id, $vehicle, $update_title=false){

	global $acf_fields;



	// Set up ACF field associations

	if(empty($acf_fields)) $acf_fields = load_acf_fields();





	//file_put_contents('testcrondump.txt', 'test: '.print_r($vehicle, true), FILE_APPEND);



	// Init field/meta mapping

	$mapping = array(

		// API Field' => meta_key1, meta_key2			// singular value

		// API Field => array( meta key => title)		// for repeaters

		// API Field => array( field1, field2)			// Multiple fields



		'VehicleID' 		=> 'caf_autofeeds_id',

		//'DealerID' 			=> '',

		'Sold' 				=> 'caf_sold_status',

		//'VIN' 				=> '',

		'StockNumber' 		=> 'caf_stocknum',

		//'DateAdded' 		=> '',

		//'DateUpdated' 		=> '',

		'DateSold' 			=> 'caf_sold_date',

		'TrimLevel' 		=> 'caf_trim',

		'Year' 				=> 'caf_year',

		//'Odometer' 			=> array('caf_summary_features' => array('feature' => 'Odometer', 'target'=>'text') ),

		'Price' 			=> 'caf_regular_price',

		//'EngineTypeName' 	=> array(),

		//'EngineCylinders' 	=> array('caf_summary_features' => array('feature' => 'Cylinders', 'target'=>'text') ),

		//'EngineCapacity' 	=> array('caf_summary_features' => array('feature' => 'Engine Capacity', 'target'=>'text') ),

		//'Doors' 			=> array('caf_summary_features' => array('feature' => 'Doors', 'target'=>'text') ),

		//'Seats' 			=> array('caf_summary_features' => array('feature' => 'Seats', 'target'=>'text') ),

		'Description' 		=> 'caf_description',

		//'Comment' 			=> '',

		//'Group' 			=> '',

		//'Type' 				=> '',

		'Make' 				=> 'caf_make',

		'Model' 			=> 'caf_model',

		'Group'				=> 'caf_group',

		'Type'				=> 'caf_type',

		//'InteriorColour' 	=> array('caf_summary_features' => 'Interior Color'),

		'ExteriorColour' 	=> 'caf_exterior_color',

		'VIN'				=> 'caf_vin',

		'DateAdded'			=> 'caf_date_added',

		'Transmission_Description' 	=> 'caf_transmission',

		'Drive_Train_Description' 	=> 'caf_drive',



	);



	// Update title if needed, usually coming from AJAX "import data" request

	if( $update_title){

		$post_title = implode(' ', array($vehicle['Year'], $vehicle['Make'], $vehicle['Model']));

		wp_update_post(array(

			'ID'=>$id,

			'post_title'=>$post_title,

			'post_name'=>str_replace(' ', '-', strtolower($post_title)),	// permalink

		));

	}



	$aux_fields = array(

		'caf_checksum' 			=> md5(json_encode($vehicle)),

		'caf_autofeeds_object' 	=> json_encode($vehicle),

	);



	// Pre-map Additional Features field

	$feature_fieldnames = array(

		'Features',

		'Options'

	);

	$add_features = array();



	// Process each feature designator

	foreach($feature_fieldnames as $fname){

		if( isset($vehicle[$fname]) && count($vehicle[$fname])){

			foreach($vehicle[$fname] as $f) $add_features[] = array('feature' => sani($f) );

		}

	}



	// Repeater fields, reverse mapping

	$rept_fields = array(

		'caf_summary_features' => array(

			array('feature' => 'Year', 'text' 				=> sani($vehicle['Year'] ) ),

			array('feature' => 'Engine', 'text' 			=> sani($vehicle['Engine_Description'] ) ),

			array('feature' => 'Make', 'text' 				=> sani($vehicle['Make'] ) ),

			array('feature' => 'Cylinders', 'text' 			=> sani($vehicle['EngineCylinders'] ) ),

			array('feature' => 'Model', 'text' 				=> sani($vehicle['Model'] ) ),

			array('feature' => 'Transmission', 'text' 		=> sani($vehicle['Transmission_Description'] ) ),

			array('feature' => 'Trim', 'text' 				=> sani($vehicle['TrimLevel'] ) ),

			array('feature' => 'Drive', 'text' 				=> sani($vehicle['Drive_Train_Description'] ) ),

			array('feature' => 'Exterior Color', 'text' 	=> sani($vehicle['ExteriorColour'] ) ),

			array('feature' => 'Seating Capacity', 'text' 	=> sani($vehicle['Seats'] ) ),

			array('feature' => 'Interior Color', 'text' 	=> sani($vehicle['InteriorColour'] ) ),

			array('feature' => 'Vehicle Type', 'text' 		=> sani($vehicle['Type'] ) ),

			array('feature' => 'Odometer', 'text' 			=> sani($vehicle['Odometer'] ) ),

			array('feature' => 'Doors', 'text' 				=> sani($vehicle['Doors'] ) ),

			array('feature' => 'Transmission', 'text' 		=> sani($vehicle['Transmission_Type'] ) ),

			array('feature' => 'Stock ID', 'text' 			=> sani($vehicle['StockNumber'] ) ),

		),

		'caf_additional_features' => $add_features,

	);





	// Process vehicle feeds

	foreach($mapping as $vec_field=>$acf_field){



		// Multiple fields?

		if( !is_array($acf_field) ){

			$data = $vehicle[$vec_field];



			// Sanitize fields

			switch($acf_field){
				case 'caf_regular_price':
					$data = (int) $data;
					break;
				case 'caf_sold_status':
					if( $data == 'false') $data = false;
					else $data = true;
					break;
				case 'caf_description': 
					$data = str_replace('\\"', '&quot;', $data);
					break;
			}

			// Single field

			update_field( $acf_fields[$acf_field], $data, $id);

			//echo '<pre>Single field: '.print_r( array($acf_fields[$acf_field], $acf_field, $vehicle[$vec_field], $id), true).'</pre>';

		}

	}



	// Process aux fields

	foreach($aux_fields as $field=>$data){

		switch ($field) {
			case 'caf_autofeeds_object':
				$data = str_replace('\\"', '&quot;', $data);
				break;
		}

		update_field( $acf_fields[$field], $data, $id);

		//echo '<pre>Aux field: '.print_r( array($acf_fields[$field], $field, $data, $id), true).'</pre>';

	}



	// Process repeater fields

	foreach($rept_fields as $field=>$data){



		// Strip empty values

		if( is_array($data)){

			foreach($data as $k=>$d){

				if( !empty($d['text']) && !$d['text']) unset($data[$k]);

				if( substr( trim($d['feature']), -1 ) == ':') unset($data[$k]);



				continue;

			}

		}



		update_field( $acf_fields[$field], $data, $id);

		//echo '<pre>REPEATER: '.$field.' '.print_r($data, true).'</pre>';

	}





	// Set vehicle dealer taxonomy

	$dealer_terms = get_terms( 'caf_dealerships', array( 'hide_empty' => false ) );

	foreach($dealer_terms as $k=>$term){



		$tax_dealerID = get_tax_meta($term,'caf_DealerID');

		if( $tax_dealerID == $vehicle['DealerID']){

			$terms = wp_set_object_terms( $id, $term->slug, 'caf_dealerships', true );

			break;

		}

	}



	return true;



} // caf_vehicle_update_fields





// Test debug trigger

// add_action('admin_head', 'caf_import_vehicles_action');





/*======================================

=            Misc Functions            =

======================================*/



function caf_import_images($post_id, $is_cron=FALSE){

	global $CAF_Settings, $count;



	require_once(ABSPATH . "wp-admin/includes/image.php");

    require_once(ABSPATH . "wp-admin/includes/file.php");

    require_once(ABSPATH . "wp-admin/includes/media.php");



	unset($time_start, $time_end, $time_elapsed);

	$time_start = microtime(true);



	$output = array(

		'images' => array(),

		'images_parsed' => 0,

		'images_ignored' => 0,

		'images_added' => 0,

		'images_deleted' => 0,

		'time_elapsed' => 0,

		'time_start' => 0,

		'time_end' => 0,

		'memory_use' => 0,

	);



	// Load images from vehicle post

	$auto_object = get_field('caf_autofeeds_object', $post_id);

	if( ! $json = json_decode($auto_object, true)){

		return false;

	}



	$images = $json['Photos'];

	$vehicle_id = $json['VehicleID'];



	$existing_imgs = get_children( array('post_parent'=>$post_id, 'post_type'=>'attachment', 'post_mime_type'=>'image') );



	foreach($images as $image){



		// Search existing images for matching id

		$image_exists = $is_newer = $ext_img_id = false;



		if( !empty($existing_imgs)){

			foreach($existing_imgs as $ext_img){

				if( $ext_img->post_content == $image['ID']){

					$image_exists = true;

					$ext_img_id = $ext_img->ID;



					if( $image['Date'] != $ext_img->post_date){

						$is_newer = true;

						wp_delete_attachment($ext_img->ID, true);

						$output['images_deleted']++;

					}



				}

			} // foreach

		}



		// Add image if non-existant or newer

		if( !$image_exists || ($image_exists && $is_newer) ){



			// Check if image url is absolute, if not add source

			if( strpos($image['Url'], 'http') !== false ){

				$url = $image['Url'];

			} else{

				$caf_url = parse_url($CAF_Settings['opt-caf-api-base-url']);

				$url = $caf_url['scheme'].'://'.$caf_url['host'].$image['Url'];

			}



			$tmp = download_url( $url );

			$file_array = array(

				'name' => basename( $url ),

				'tmp_name' => $tmp

			);



			// Check for download errors

			if ( is_wp_error( $tmp ) ) {

				@unlink( $file_array[ 'tmp_name' ] );

				$count['images_parsed']++;

				return $tmp;

			}



			$image_title = implode(' ', array($vehicle_id, $image['Caption']) );

			$image_data = array(

				'post_date' => $image['Date'],

				'post_excerpt'=>$image['Caption'],

				'post_content'=>$image['ID'],

				'post_modified' => $image['Date'],

			);



			$id = media_handle_sideload( $file_array, $post_id, $image_title, $image_data );



			// Check for handle sideload errors.

			if ( is_wp_error( $id ) ) {

				@unlink( $file_array['tmp_name'] );

				$count['images_parsed']++;

				return $id;

			}



			//echo '<pre>Test ID: '.print_r($id, true).'</pre>';

			$test = wp_set_object_terms($id, 'vehicle', 'caf_media_cat');



			$attachment = wp_get_attachment_url( $id );

			$output['images'][] = $id;

			$output['images_added']++;



		} else{



			// Image existed and was older, send back existing id

			$output['images'][] = $ext_img_id;

			$output['images_ignored']++;



		} // if image !exists and is newer



		$output['images_parsed']++;



	} // foreach images





	if( $is_cron){



		// Associate images with gallery field for vehicle

		$acf_fields = array();

		foreach($CAF_Settings['opt-caf-acf-field-pairs'] as $pair){

			$pair = explode(':', $pair);

			$acf_fields[$pair[1]] = $pair[0];

			$acf_fields[$pair[0]] = $pair[1];

		}



		update_field( $acf_fields['caf_gallery'], $output['images'], $post_id);

		update_post_meta($post_id, '_thumbnail_id', $output['images'][0]);

	}





	// Report and return.

	$time_end = microtime(true);

	$output['time_elapsed'] = round( ($time_end-$time_start), 2);

	$output['time_start'] = $time_start;

	$output['time_end'] = $time_end;

	$output['memory_use'] = (memory_get_peak_usage(true)/1024/1024);



	return $output;

} // caf_import_images







function caf_import_count_msg(){

	global $count;

	echo '<div class="updated">

			<h4>CAF Vehicle Import</h4>

			<ul><li>Parsed: '.$count['parsed'].'</li>

				<li>Ignored: '.$count['ignored'].'</li>

				<li>Updated: '.$count['updated'].'</li>

				<li>Added: '.$count['added'].'</li>

				<li>Deleted: '.$count['deleted'].'</li>

			</ul>

		</div>';



}



function recursive_array_search($needle,$haystack) {

    foreach($haystack as $key=>$value) {

        $current_key=$key;

        if($needle===$value OR (is_array($value) && recursive_array_search($needle,$value) !== false)) {

            return $current_key;

        }

    }

    return false;

}



// Checks val before using

function sani(&$val){

	if(isset($val) && !empty($val) ) return $val;

	else return null;

}