<?php
include_once('CaordaAutofeeds_ShortCodeScriptLoader.php');
 
class CaordaAutofeeds_ShortCode_featured_vehicles extends CaordaAutofeeds_ShortCodeScriptLoader {
 
	static $addedAlready = false;
	public function addScript() {
		if (!self::$addedAlready) {
			self::$addedAlready = true;
			//wp_register_script('my-script', plugins_url('js/my-script.js', __FILE__), array('jquery'), '1.0', true);
			//wp_print_scripts('my-script');
		}
	}

	public function handleShortcode($atts, $content) {
		$atts = extract( shortcode_atts( array( 
			'blink' => "",
			'posts_per_page' => -1,
			'offset' => 0,
			'post_type' => 'caf_featured_vehicle',
			'category' => '',
			'orderby' => 'menu_order',
			'order' => 'DESC',
			'include' => '',
			'exclude' => '',
			'meta_key' => '',
			'meta_value' => '',
			'post_mime_type' => '',
			'post_parent' => '',
			'post_status' => 'publish',
			'suppress_filters' => true
		),$atts ) );

		$posts = get_posts(array(
			'posts_per_page'   => $posts_per_page,
			'offset'           => $offset,
			'category'         => $category,
			'orderby'          => $orderby,
			'order'            => $order,
			'include'          => $include,
			'exclude'          => $exclude,
			'meta_key'         => $meta_key,
			'meta_value'       => $meta_value,
			'post_type'        => $post_type,
			'post_mime_type'   => $post_mime_type,
			'post_parent'      => $post_parent,
			'post_status'      => $post_status,
			'suppress_filters' => $suppress_filters,
		));

		$output = '';

		foreach($posts as $post){
			//$img = get_the_post_thumbnail( $post->ID, 'caf_vehicle_bar');
			$img_src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'caf_vehicle_bar');
			$output .= '<div class="vehicle">'
				.'<a href="'.get_permalink($post->ID).'">'
					.'<span class="vehicle-img" style="background-image:url('.$img_src[0].');"></span>'
					.'<span class="vehicle-text">'
						.'<span class="vehicle-title">'.$post->post_title.'</span>'
						.'<span class="vehicle-subtitle">'.get_field('subtitle', $post->ID).'</span>'
					.'</span>'
				.'</a>'
			.'</div>';
		}


		$output = '<div class="caf-featured"><div class="caf-featured-vehicles">'.$output .'</div></div>';
		$output .= '<div class="caf-featured-banner">';

		if( $blink){
			$output .= '<a href="'.$blink.'">'.apply_filters('the_content', $content).'</a>';
		} else{
			$output .= apply_filters('the_content', $content);	
		}
		

		$output .= '</div>';
		return $output;
	}

} // class