<?php

// Define events
global $caf_cron_events, $CAF_Settings;
$CAF_Settings = get_option('CAF_Settings', array());

$caf_cron_events = array(
	'caf_clean_logs' 				=> isset($CAF_Settings['opt-caf-cron-clean-logs']) ? $CAF_Settings['opt-caf-cron-clean-logs'] : 'hourly',
	'caf_import_vehicles' 			=> isset($CAF_Settings['opt-caf-cron-import-vehicles']) ? $CAF_Settings['opt-caf-cron-import-vehicles'] : '4x_daily',
	'caf_import_images' 			=> isset($CAF_Settings['opt-caf-cron-import-images']) ? $CAF_Settings['opt-caf-cron-import-images'] : 'two_minutes',
	'caf_check_removed_vehicles' 	=> isset($CAF_Settings['opt-caf-cron-check-rmv-vehicles']) ? $CAF_Settings['opt-caf-cron-check-rmv-vehicles'] : '2x_daily',
	'caf_delete_terminal_vehicles' 	=> isset($CAF_Settings['opt-caf-cron-delete-term-vehicles']) ? $CAF_Settings['opt-caf-cron-delete-term-vehicles'] : 'daily',
);

// add custom time to cron
add_filter('cron_schedules', 'caf_filter_cron_schedules');
function caf_filter_cron_schedules($param) {
	return array(
		'three_minutes' => array(
			'interval' => 60 * 3, // seconds
			'display'  => __('Every 3 minutes')
		),
		'two_minutes' => array(
			'interval' => 60 * 2, // seconds
			'display'  => __('Every 2 minutes')
		),
		'five_minutes' => array(
			'interval' => 60 * 5, // seconds
			'display'  => __('Every 5 minutes')
		),
		'2x_daily' => array(
			'interval' => 3600 * 12, // 12 hours
			'display' => __('Two times daily (CAF)')
		),
		'4x_daily' => array(
			'interval' => 3600 * 6, // 6 hours
			'display' => __('Four times daily (CAF)')
		)
	);
}


// On an early action hook, check if the hook is scheduled - if not, schedule it.
add_action( 'init', 'caf_setup_schedule' );
function caf_setup_schedule() {
	global $caf_cron_events;

	// Check and schedule each event
	foreach($caf_cron_events as $event=>$schedule){
		if ( ! wp_next_scheduled( $event ) ) {
			wp_schedule_event( time(), $schedule, $event);
		}
	} // foreach events

} // caf_setup_schedule



/*==========================================
=            Clean wp_log posts            =
==========================================*/

// On the scheduled action hook, run a function.
add_action( 'caf_clean_logs', 'caf_clean_debug_logs' );
function caf_clean_debug_logs() {
	global $wpdb;
	$CAF_Settings = get_option('CAF_Settings', array());
	$num_logs = $CAF_Settings['opt-caf-num-logs'];

	WP_Logging::add( $title = 'CAF Cron Job', 'CAF Cron: Cleaning logs', $parent = 0, $type = 'event' );

	$wpdb->query("CREATE TEMPORARY TABLE temp (ID int);");
	$wpdb->query("INSERT INTO temp SELECT ID FROM $wpdb->posts WHERE post_type='wp_log' ORDER BY ID DESC LIMIT 0, ".$num_logs);
	$wpdb->query("DELETE FROM wp_posts WHERE post_type = 'wp_log' AND ID NOT IN (SELECT ID FROM temp)");
	$wpdb->query("DROP TABLE temp;");

}

/*==============================================
=            Import vehicles action            =
==============================================*/

// in CaordaAutofeeds_ImportVehicles.php
add_action( 'caf_import_vehicles', 'caf_cron_import_vehicles' );
function caf_cron_import_vehicles(){

	// Init status var
	$status = array('current'=>'init');

	while($status['current'] !== 'complete'){
		$status = caf_ajax_import_controller($cron=true);
		WP_Logging::add( $title = 'CAF Cron Vehicle Import - ['.$status['current']
			.' <a href="/wp-admin/post.php?post='.$status['output']['start_key'].'&action=edit">'
			.$status['output']['start_key'].'</a>]', 'CAF Cron: Importing Vehicles. '.print_r($status, true),
			$parent = 0, $type = 'event'
		);
	}



}

/*============================================
=            Import Images Action            =
============================================*/
add_action( 'caf_import_images', 'caf_cron_import_images' );
function caf_cron_import_images(){

	$result = array('images_parsed'=>0, 'images_ignored'=>0);
	$CAF_Settings = get_option('CAF_Settings', array());
	$loops = intval($CAF_Settings['opt-caf-import-image-batch']) ? intval($CAF_Settings['opt-caf-import-image-batch']) : 1;
	$vehicles = (array) json_decode( $CAF_Settings['opt-caf-api-img-import'], true);

	// Perform multiple loops if needed
	for($i=0;$i<$loops;$i++){

		// Grab first vehicle in vehicles in queue
		$vehicle = reset($vehicles);
		$vehicle = isset($vehicle) ? $vehicle : NULL;
		$current_key = key($vehicles);

		// Process
		if( $vehicle ){

			if( $result = caf_import_images($vehicle, $is_cron=TRUE )){

				// Remove processed vehicle from list
				unset($vehicles[ $current_key ]);

				$log_title = 'CAF Cron Image Import - <a href="/wp-admin/post.php?post='.$vehicle.'&action=edit">['.$vehicle.']</a>';
				$log_content = $result['images_added'].' added, '.$result['images_ignored'].' ignored, '.$result['images_parsed'].' parsed. '.round($result['time_elapsed'], 2).' sec, '.$result['memory_use'].' MB<br />'
				.print_r($result, true);
			} else{

				// Move ID to end of list
				unset($vehicles[ $current_key ]);
				$vehicles[] = $vehicle;

				$log_title = 'CAF Cron Image Import ERROR - <a href="/wp-admin/post.php?post='.$vehicle.'&action=edit">['.$vehicle.']</a>';
				$log_content = 'There was a problem importing images.<br />'
				.print_r($result, true);
			}


			WP_Logging::add( $log_title, $log_content, $parent = 0, $type = 'event' );


		} // if images available for processing


	} // for i

	// Update WP option
	$CAF_Settings['opt-caf-api-img-import'] = json_encode($vehicles);
	update_option( 'CAF_Settings', $CAF_Settings );

} // caf_cron_import_images




/*=========================================================
=            Check for deleted / sold vehicles            =
==========================================================*/

add_action( 'caf_check_removed_vehicles', 'caf_cron_check_removed_vehicles' );
function caf_cron_check_removed_vehicles(){
	$CAF_API = new CAF_API();
	$dealers = $CAF_API::get_vehicles();
	$api_vehicles = array();
	$caf_vehicles = get_posts(array(
		'numberposts' => -1,
		'post_type' => 'caf_inventory',
		'post_status' => 'publish',
	));

	// Load source vehicle IDs into mass array for quick parsing
	foreach($dealers as $dealer){
		$api_vehicles = array_merge($api_vehicles , array_keys($dealer) );
	}

	$scheduled = array(); // tracking altered vehicles

	foreach($caf_vehicles as $caf_vehicle){

		// Find our current vehicle id
		$vehicle_id = get_field('caf_autofeeds_id', $caf_vehicle->ID);
		//$ids[] = $vehicle_id;

		$is_sold = get_field('caf_sold_status', $caf_vehicle->ID);

		// If that vehicle is no longer listed...
		if( (false === array_search($vehicle_id, $api_vehicles)) || ($is_sold) ){

			// Mark for deletion!
			$caf_vehicle->post_status = 'terminal';
			wp_update_post($caf_vehicle);
			$scheduled[] = $vehicle_id;
		}
	}


	if(count($scheduled)){
		WP_Logging::add( $title = 'CAF Inventory Check', 'Scheduled '.count($scheduled).' vehicles for termination. <br />'.json_encode($scheduled), $parent = 0, $type = 'event' );
	}

}
/*=======================================================
=            Delete terminal listed vehicles            =
=======================================================*/
add_action( 'caf_delete_terminal_vehicles', 'caf_cron_delete_terminal_vehicles' );
function caf_cron_delete_terminal_vehicles(){
	$output = array('deleted_inventory'=>array(), 'error'=>array() );
	$del_childs = array();
	$childs = 0;

	$args = array(
		'date_query' => array(
			array(
				'column' => 'post_modified',
				'before'  => '1 month ago',
			),
		),
		'posts_per_page' => -1,
		'post_type' => 'caf_inventory',
		'post_status' => 'terminal',
	);

	$the_query = new WP_Query( $args );

	// The Loop
	if ( $the_query->have_posts() ) {
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			$id = get_the_ID();

			// Collect attachments
			$children = get_children(array('post_parent'=>$id));


			// Remove attachments
			foreach($children as $child){
				$cid = $child->ID;
				if( false === wp_delete_attachment($cid, true) ) $output['error'][] = $cid;
				else $del_childs[] = $cid;
				$childs++;
			}

			if( false === wp_delete_post($id, true) ) $output['error'][] = $id;
			else $output['deleted_inventory'][$id] = $del_childs;
		}

		// Done, add a log event
		WP_Logging::add( $title = 'CAF Cron Scheduled Deletion', 'CAF Cron: Deleted '
			.count($output['deleted_inventory']).' terminal vehicles, '
			.$childs.' images with '.count($output['error']).' errors.<br />'.json_encode($output, true), $parent = 0, $type = 'event' );

	} else {
		// no posts found
	}


	/* Restore original Post Data */
	wp_reset_postdata();

}





/*============================================================
=            Clear schedule hooks on deactivation            =
============================================================*/

// On deactivation, remove all functions from the scheduled action hook.
$path = plugin_dir_path( __FILE__);
$path_exp = explode('/', $path );
$path_exp_clean = array_filter($path_exp);
$file = end( $path_exp_clean).'.php';

register_deactivation_hook( plugin_dir_path(__FILE__).$file, 'caf_cron_deactivation' );
function caf_cron_deactivation() {
	global $caf_cron_events;

	foreach($caf_cron_events as $event=>$schedule){
		wp_clear_scheduled_hook( $event );
	}
}


