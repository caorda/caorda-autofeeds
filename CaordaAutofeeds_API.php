<?php

/**
 * Class for interacting with the Caorda Autofeeds API
 *
 * @package     CAF API Class
 * @copyright   Copyright (c) 2014, Eric McNiece
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
*/

class CAF_API {


	/**
	 * Class constructor.
	 *
	 * @since 1.0
	 *
	 * @access public
	 * @return void
	 */
	function __construct() {

		// create the log post type
		//add_action( 'init', array( $this, 'register_post_type' ) );

	}

	/**
	 * Gets dealers from API if forced, otherwise gets from WP options
	 *
	 * @since 1.1
	 * @access public
	 *
	 * @uses $this->get_dealers()     Returns array of dealers
	 */
	public static function get_dealers($force_update=false, $output_type = 'array'){
		global $caf_curl_errors;

		$dealers = get_option('caf_api_dealers');

		if(!$dealers || $force_update){

			$dealers = CAF_API::curl_api_call('dealer', 'caf_api_dealers', $output_type);
		} else{
			// Pulling from option, which is json encoded
			$dealers = json_decode($dealers, true);
		}



		return $dealers;

	} // get_dealers



	/**
	 * Gets vehicles from API if forced, otherwise gets from WP options
	 *
	 * @since 1.1
	 * @access public
	 *
	 * @uses $this->get_vehicles()     Returns array of vehicles
	 */
	public static function get_vehicles($force_update=false, $dealer_id=NULL){
		$CAF_Settings = get_option('CAF_Settings', array());

		// Prep dealer id array
		if(!$dealer_id) $dealer_id = $CAF_Settings['opt-caf-api-dealer-ids'];
		if(!is_array($dealer_id)) $dealer_id = array($dealer_id);

		foreach($dealer_id as $dealer){
			$method = 'dealer/'.$dealer.'/vehicles';

			$vehicles[] = CAF_API::curl_api_call($method); // might want to split this into dealer IDs sometime

		} // foreach dealers

		$json = json_encode($vehicles);

		if( $json){

			$file = untrailingslashit(ABSPATH).$CAF_Settings['opt-caf-api-inventory-data-location'];

			if( file_exists($file)){
				$dump = file_put_contents($file, $json);
				WP_Logging::add( $title = 'CAF API Event', 'Event: Storing vehicles in output file', $parent = 0, $type = 'status' );
			}
		}


		return $vehicles;

	} // get_vehicles


	/**
	 * Base cURL call made from other methods
	 *
	 * @since 1.1
	 * @access public
	 *
	 * @uses $this->curl_api_call()     Returns array of data
	 */
	public static function curl_api_call($method=null, $update_option=null, $output_type='array'){
		global $caf_curl_errors;
		if(!$method) return false;

		// Get cURL resource
		$curl = curl_init();

		// Set some options - we are passing in a useragent too here
		$url = trailingslashit(CAF_API_URL).$method;

		// TEMPORARY: GET RID OF THIS WHEN VEHICLE FEED WORKS!!!
		if( strpos($method, 'vehicle') ){
			//$url = 'http://mazdavictoria.develapache/api_debug_vehicles.txt';
		}

		// END TEMPORARY HACK


		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $url,
			CURLOPT_USERAGENT => 'Caorda Autofeeds WordPress Plugin'
		));

		$resp = curl_exec($curl);

		if(!$resp){
			$caf_curl_errors = array(curl_error($curl), curl_errno($curl));
			add_action( 'admin_notices', 'caf_curl_error_admin_notice' );

			$ermsg = 'Error cURL: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl).'. Requested URL: '.trailingslashit($api_url).$method;
			WP_Logging::add( $title = 'CAF cURL Error', $ermsg, $parent = 0, $type = 'error' );

			return false;
		}

		curl_close($curl);

		$logmsg = 'Event: API call: '.$method;
		if( $update_option) $logmsg .= ' - Update option:'.$update_option;

		WP_Logging::add( $title = 'CAF API Event', $message = $logmsg, $parent = 0, $type = 'event' );
		$data = json_decode($resp, true); // Force associative array!

		// Successful decode?
		if($data){
			if( !empty($data['content']['data']) ){
				// Dealer call - data element is populated

				// Serialize here to match get_option parameter, just in case
				$output = $data['content']['data'];

			} else if( count($data['content'])){
				// Vehicle call - content contains vehicle id arrays

				$output = $data['content'];
			}

			if($update_option){
				update_option( $update_option, json_encode($output ) );
			}

		}
		else{

			// JSON decode failed, display error
			WP_Logging::add( $title = 'CAF API Event', $message = 'Event: API call: '.$method.' - JSON Decode failed', $parent = 0, $type = 'error' );
			$output = array('status'=>'error', 'message'=>'JSON Decode failed.');

		}// if successful decode

		// Handle output format
		if( $output_type == 'array'){ /* do nothing, already decoded */}
		else if( $output_type == 'json'){
			$output = json_encode($output);
		}

		return $output;

	} // curl_api_call



} // CAF_API

$GLOBALS['caf_api'] = new CAF_API();
