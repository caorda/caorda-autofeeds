<?php

function caf_excerpt($limit) {

	$excerpt = explode(' ', get_the_excerpt(), $limit);

	if (count($excerpt)>=$limit) {
		array_pop($excerpt);
		$excerpt = implode(" ",$excerpt).'...';
	} else {
		$excerpt = implode(" ",$excerpt);
	}

	$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	return $excerpt;
}

// Trims content to length
function caf_content($text=null, $limit=140, $readmore=NULL, $id=NULL) {

	if( $text) $content = explode(' ', $text, $limit);
	else $content = explode(' ', get_the_content(), $limit);

	if (count($content)>=$limit) {
		array_pop($content);
		$content = implode(" ",$content).'...';
	} else {
		$content = implode(" ",$content);
	}

	$content = preg_replace('/\[.+\]/','', $content);
	$content = apply_filters('the_content', $content);
	$content = str_replace(']]>', ']]&gt;', $content);

	if( isset($readmore) && isset($id) && $text ){
		$link = get_permalink($id);
		$content = str_lreplace('</p>', ' <a href="'.$link.'">'.$readmore.'</a></p>', $content);
	}
	return $content;
}

/* Formats DL for details display
	Expecting a format like this:

	parent,fieldname:label

	caf_model:Model
	caf_year:Year
	caf_trim:Trim
	caf_summary_features,odometer:Odometer
	caf_summary_features,transmission:Trans
	caf_summary_features,exterior_color:Exterior Color
	caf_summary_features,interior_color:Interior Color
	caf_summary_features,doors:Doors

*/
function caf_compile_details_list($details=NULL, $post){

	if( !isset($details)) $details = $CAF_Settings['opt-caf-list-details'];

	$detail_compile = array();
	foreach( preg_split('/\n/', $details) as $detail){
		if( strpos($detail, ',')){
			$d = explode(',', $detail);
			$dc = explode(':', $d[1]);
			foreach($d as &$v) $v = trim($v);
			foreach($dc as &$v) $v = trim($v);

			$detail_compile[$d[0]][$dc[0]] = $dc[1];
		} else{
			$d = explode(':', $detail);
			foreach($d as &$v) $v = trim($v);
			$detail_compile[$d[0]] = $d[1];
		}
	}

	//echo '<pre>CPL: '.print_r($detail_compile, true).'</pre>';

	$output = '<dl class="caf-details-list">';

	foreach($detail_compile as $k=>$detail){
		if( !is_array($detail) ){

			if( $val = get_field($k, $post->ID) ){

				$output .= '<dt>'.trim($detail).':</dt><dd>'.$val.'</dd>';
			} else if ($val = get_field($detail, $post->ID)){

				$output .= '<dt>'.trim($detail).':</dt><dd>'.$val.'</dd>';
			}
		} else{


			// is array, go deeper
			if( $parent = get_field($k, $post->ID)){

				foreach($detail as $j=>$det){

					$search_normal = array_searchRecursive($det, $parent);
					$search_lower = array_searchRecursive(strtolower($det), $parent);

					$search_result = empty($search_lower) ? $search_normal : $search_lower;

					// Only populate if we have values
					if( !empty($search_result) && $parent[$search_result[0]]['text'] )
						$output .= '<dt>'.$det.':</dt><dd>'.$parent[$search_result[0]]['text'].'</dd>';

				} // foreach $det

			} // if $parent exists

		} // if is/is not array

	} // foreach $details

	$output .= '</dl>';
	return $output;

} // caf_compile_details_list

// Quick currency formatting
function caf_cur_format($string=NULL, $prefix=''){
	$output = '';
	global $CAF_Settings;
	if( $string){
		$output = number_format($string, 0);

		if( $CAF_Settings['opt-caf-cursymbol-position']) $output = $CAF_Settings['opt-caf-currency-symbol'].$output;
		else $output = $output.$CAF_Settings['opt-caf-currency-symbol'];

		$output = $prefix.' '.$output;

	}

	return $output;
}

function str_lreplace($search, $replace, $subject){
    $pos = strrpos($subject, $search);

    if($pos !== false)
    {
        $subject = substr_replace($subject, $replace, $pos, strlen($search));
    }

    return $subject;
}

function array_searchRecursive( $needle, $haystack, $strict=false, $path=array() ){
    if( !is_array($haystack) ) {
        return false;
    }

    foreach( $haystack as $key => $val ) {
        if( is_array($val) && $subPath = array_searchRecursive($needle, $val, $strict, $path) ) {
            $path = array_merge($path, array($key), $subPath);
            return $path;
        } elseif( (!$strict && $val == $needle) || ($strict && $val === $needle) ) {
            $path[] = $key;
            return $path;
        }
    }
    return false;
}

// Check if path is in plugin or theme
function caf_get_tpl_location($filename=__FILE__){

	if( strpos($filename, 'caorda-autofeeds')){
		$location = 'plugin';
	} else{
		$location = 'theme';
	}

	return $location;
}

/* Quickie url insert */
add_shortcode('caf-url', 'caf_url_insert' );
function caf_url_insert($atts, $content){

	$url = urlencode('http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']);
	return $url;
}

/* Generate back buttons */
function caf_back_button(){
	global $CAF_Settings;

	$page = $_COOKIE['caf_pages'];
	$url = isset($page) ? $page : $CAF_Settings['opt-caf-fwp-filter-widget-filter-reseturl'];

	$link = '<a href="'.$url.'">'.$CAF_Settings['opt-caf-detail-back-button-text'].'</a>';
	return $link;
}


function caf_curl_error_admin_notice(){
	global $caf_curl_errors;

	echo '<div class="error">
		<h3>CAF API Error</h3>
		<p>Caorda Autofeeds cURL error id #'.$caf_curl_errors[1].': <strong>'.$caf_curl_errors[0].'</strong></p>
	</div>';
}

/*==========================================================
=            ACF Field Association Field Helper            =
==========================================================*/
// Helps ACF make the correct field associations for un-initd post type fields
function caf_get_field($fieldname, $post_id=null){
	global $CAF_Settings, $post;
	if(!$CAF_Settings) $CAF_Settings = get_option('CAF_Settings');

	$acf_fields = array();
	foreach($CAF_Settings['opt-caf-acf-field-pairs'] as $pair){
		$pair = explode(':', $pair);
		$acf_fields[$pair[1]] = $pair[0];
		$acf_fields[$pair[0]] = $pair[1];
	}

	$output = get_field( $acf_fields[$fieldname] , $post_id);
	return $output;

}
function caf_update_field($fieldname, $value, $post_id=null){
	global $CAF_Settings, $post;
	if(!$CAF_Settings) $CAF_Settings = get_option('CAF_Settings');

	$acf_fields = array();
	foreach($CAF_Settings['opt-caf-acf-field-pairs'] as $pair){
		$pair = explode(':', $pair);
		$acf_fields[$pair[1]] = $pair[0];
		$acf_fields[$pair[0]] = $pair[1];
	}

	$output = update_field( $acf_fields[$fieldname], $value, $post_id);
	return $output;

}

// Sets up ACF field array for use around the place
function load_acf_fields(){

	global $acf_fields, $CAF_Settings;
	$CAF_Settings = get_option('CAF_Settings', array());

	$acf_fields = array();
	foreach($CAF_Settings['opt-caf-acf-field-pairs'] as $pair){
		$pair = explode(':', $pair);
		$acf_fields[$pair[1]] = $pair[0];
		//$acf_fields[$pair[1]] = $pair[1];
		$acf_fields[$pair[0]] = $pair[1];
	}

	return $acf_fields;
}

/*============================================
=            Compile CarProof URL            =
============================================*/
function caf_get_carproof_url($post_id, $link_action="report"){
	global $CAF_Settings;

	$output = array(
		'url' => '',
		'class' => '',
		'data' => ''
	);

	$cp_enable = $CAF_Settings['opt-caf-enable-carproof'];
	$cp_key = NULL;
	$cp_keys = $CAF_Settings['opt-caf-carproof-api-dealer-key'];
	$cp_dealers = $CAF_Settings['opt-caf-api-dealer-ids'];
	$vin = get_field('caf_vin', $post_id);
	$cp_dealer_keys = array();

	// Perform CarProof/DealerID merge and lookup
	if( count($cp_keys) == count($cp_dealers) ){
		foreach($cp_keys as $k=>$key)
			$cp_dealer_keys[$cp_dealers[$k]] = $key;
	}

	// Dealer taxonomy vars
	//$current_dealer = array_values(get_terms( 'caf_dealerships', array('number'=>1)) ); // OLD! Doesn't use actual post
	$tax = get_the_terms($post_id, 'caf_dealerships');
	if( $tax) $current_dealer = array_values($tax); // array_values to reset key index
	else $current_dealer = array();


	if( count($current_dealer)){
		$dealer_id = get_tax_meta($current_dealer[0]->term_id,'caf_DealerID');
		$cp_key = $cp_dealer_keys[$dealer_id];

		// Add to output for use in theme
		$output['dealer'] = $current_dealer;
		$output['dealer_id'] = $dealer_id;
	}

	// Generate direct link or iframe webform (by request) content
	if( $CAF_Settings['opt-caf-enable-carproof'] == 'webform'){

		// iFrame version
		$output['url'] = $CAF_Settings['opt-caf-carproof-iframe'].'?'
		.'vehicle_url='.urlencode(get_permalink($post_id ))
		.'&vehicle_vin='.urlencode(get_field('caf_vin'))
		.'&vehicle_desc='.urlencode( get_the_title($post_id ) );

		$output['class'] = 'caf-fancybox iframe';

	} else if( ($cp_enable == 'enable') && $cp_key && $vin ){

		// Direct link version
		if( $link_action == 'report') $output['url'] = 'https://webservice.carproof.com/memberreports/lookup.aspx?id='.$cp_key.'&VIN='.$vin;
		else if($link_action == 'vehicle') $output['url'] = get_permalink( $post_id );
		$output['class'] = 'carproof-link';

	}

	$output['data'] = implode(' ', array(
		'data-fancybox-type="iframe"',
		'data-fancybox-width="'.$CAF_Settings['opt-caf-carproof-fancybox-size']['width'].'"',
		'data-fancybox-height="'.$CAF_Settings['opt-caf-carproof-fancybox-size']['height'].'"',
	));

	return $output;
}


/*==============================================
=            WP-Style Get Functions            =
==============================================*/
function get_caf_search_page(){
	global $CAF_Settings;
	return get_permalink($CAF_Settings['opt-caf-search-page']);
}
function the_caf_search_page(){
	global $CAF_Settings;
	echo get_permalink($CAF_Settings['opt-caf-search-page']);
}



/*
 * Replacement for get_adjacent_post()
 *
 * This supports only the custom post types you identify and does not
 * look at categories anymore. This allows you to go from one custom post type
 * to another which was not possible with the default get_adjacent_post().
 * Orig: wp-includes/link-template.php
 *
 * @param string $direction: Can be either 'prev' or 'next'
 * @param multi $post_types: Can be a string or an array of strings
 */
function caf_get_adjacent_post($direction = 'prev', $post_types = 'post') {
    global $post, $wpdb;

    if(empty($post)) return NULL;
    if(!$post_types) return NULL;

    if(is_array($post_types)){
        $txt = '';
        for($i = 0; $i <= count($post_types) - 1; $i++){
            $txt .= "'".$post_types[$i]."'";
            if($i != count($post_types) - 1) $txt .= ', ';
        }
        $post_types = $txt;
    } else {
    	$post_types = "'".trim($post_types, "'")."'";
    }


    $current_post_date = $post->post_date;

    $join = '';
    $in_same_cat = FALSE;
    $excluded_categories = '';
    $adjacent = $direction == 'prev' ? 'previous' : 'next';
    $op = $direction == 'prev' ? '<' : '>';
    $order = $direction == 'prev' ? 'DESC' : 'ASC';

    $join  = apply_filters( "get_{$adjacent}_post_join", $join, $in_same_cat, $excluded_categories );
    $where = apply_filters(
    	"get_{$adjacent}_post_where",
    	$wpdb->prepare(
    		"WHERE p.post_date $op %s AND p.post_type IN({$post_types}) AND p.post_status = 'publish'",
    		$current_post_date
    	),
    	$in_same_cat,
    	$excluded_categories
    );
    $sort  = apply_filters( "get_{$adjacent}_post_sort", "ORDER BY p.post_date $order LIMIT 1" );

    $query = "SELECT p.* FROM $wpdb->posts AS p $join $where $sort";

    $query_key = 'adjacent_post_' . md5($query);
    $result = wp_cache_get($query_key, 'counts');
    if ( false !== $result )
        return $result;

    $result = $wpdb->get_row("SELECT p.* FROM $wpdb->posts AS p $join $where $sort");
    if ( null === $result )
        $result = '';

    wp_cache_set($query_key, $result, 'counts');
    return $result;
}

function caf_acf_get_field_pairs(){
	// TODO: make this dynamically populate from ACF definitions
	// Will probably need to make db calls

	//$fields = get_field_objects();
	$fields = array(
		'field_534c22a61c2ca:caf_gallery',
		'field_534c209137334:caf_carproof_link',
		'field_534c20e537336:caf_summary_features',
		'field_534c21bd37339:caf_additional_features',
		'field_534c431f9d29f:caf_description',
		'field_534d6eda9dbc3:caf_sale_price',
		'field_534d7903eba1c:caf_checksum',
		'field_534d7b782e8b2:caf_autofeeds_id',
		'field_53582caf96c0f:caf_autofeeds_object',
		'field_533c93de63485:caf_year',
		'field_533c93e763486:caf_make',
		'field_533c93ea63487:caf_model',
		'field_533c93ee63488:caf_trim',
		'field_533c93f263489:caf_regular_price',
		'field_533c9c4becc62:caf_display_content',
		'field_533c9d0c7fbf6:caf_display_galleries',
		'field_534317352c782:caf_display_cta',
		'field_534c1faccc11f:caf_display_summary',
		'field_534c1fe3cc120:caf_display_features',
		'field_534d7a339d3ef:caf_display_description',
		'field_535ad04da23ff:caf_exterior_color',
		'field_5360200e87f03:caf_date_added',
		'field_5362c06479671:caf_group',
		'field_5362d10ac05ed:caf_type',
		'field_5367c5904a049:caf_vin',
		'field_5367c5e443b32:caf_sold_status',
		'field_53e2cad556819:caf_transmission',
		'field_53e2cadf5681a:caf_drive',
		'field_53ebad04e1194:caf_stocknum',
	);
	return $fields;
}
