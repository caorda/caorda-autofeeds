jQuery(document).ready(function($){

	/*================================================
	=            Get Dealers Button Click            =
	================================================*/

	$('#getDealers').on('click', function(e){
		e.preventDefault();
		$btn = $(this);
		var cafAjaxUrl = ajaxurl + '?action=caf_api_get_dealers';
		console.log( 'CAF Ajax: '+cafAjaxUrl);

		$.ajax({
			type:"POST",
			url: cafAjaxUrl,
			dataType: 'json',
			//data: newCustomerFor,
			beforeSend: function(){
				$btn.after('<span class="ajax-loader"></span>');
			},
			success:function(data){

				console.log( data);
				location.reload(false);
			},
			complete: function(){
				$btn.next('.ajax-loader').remove();
			}
		});

		return false;
	}); // getDealers .click


	/*=============================================
	=            Get vehicles from API            =
	=============================================*/

	$('#getVehicles').on('click', function(e){
		e.preventDefault();
		$btn = $(this);
		var cafAjaxUrl = ajaxurl + '?action=caf_api_get_vehicles';
		console.log( 'CAF Ajax: '+cafAjaxUrl);

		$.ajax({
			type:"POST",
			url: cafAjaxUrl,
			dataType: 'json',
			//data: newCustomerFor,
			beforeSend: function(){
				$btn.after('<span class="ajax-loader"></span>');
			},
			success:function(data){
				var jsonData = JSON.stringify(data);
				$( $btn.data('target') ).children('textarea').val(jsonData);

				//location.reload(false)
			},
			complete: function(){
				$btn.next('.ajax-loader').remove();
			}
		});

		return false;
	}); // getDealers .click

	/*=================================================
	=            Delete All CAF Inventory            =
	=================================================*/

	$('.caf-ajax').on('click', function(e){
		e.preventDefault();
		var $btn = $(this),
			action = $btn.data('ajax-action');
			cafAjaxUrl = ajaxurl + '?action='+action;

		if( $btn.data('confirm') == 'true'){
			if( !confirm("ARE YOU SURE you want to do this? This action cannot be undone.") ){
				return false;
			}
		}

		console.log( 'CAF Ajax: '+cafAjaxUrl);


		$.ajax({
			type:"POST",
			url: cafAjaxUrl,
			dataType: 'json',
			//data: newCustomerFor,
			beforeSend: function(){
				$btn.after('<span class="ajax-loader"></span>');
			},
			success:function(data){

				console.log( data);
				$btn.parent().append('<div class="updated">'+data.message+'</div>');

			},
			complete: function(){
				$btn.next('.ajax-loader').remove();
			}
		});

		return false;
	}); // getDealers .click

	/*================================================
	=            Import vehicles to posts            =
	================================================*/
	var importStatus = 'ready',
		cafRefreshTimer = null,
		$importStartBtn = $('#cafBeginImport'),
		$importResetBtn = $('#cafResetImport'),
		$statBar = $importStartBtn.parent().find('.caf-progress-bar'),
		$status = $('.import-status'),
		cafAjaxUrl = ajaxurl + '?action=caf_api_import_controller',
		timerInterval = 5000;

	var importAjax = {
		type:"POST",
		url: cafAjaxUrl,
		//async: true,
		dataType: 'json',
		timeout: 120000,	// matches php time limit
		beforeSend: function(){
			$statBar.addClass('animate-bg active');

		},
		success:function(data){
			//console.log( typeof(data), typeof(data.output), typeof(data.output.images) );
			var statusText = data.current,
				updateMsg = '',
				date = new Date(),
				timeStamp = date.toISOString();

			// Append image progress if present
			if( data.output && data.output.images ){
				statusText = statusText + ' ('+data.output.images.parsed+'/'+data.output.images.count+')';
			}

			$status.text( 'Status: '+statusText);
			importStatus = data.current;

			var barProg = updateStatBar( importStatus, data);
			$statBar.children('.progress').width( (barProg*100)+'%' );

			//console.log( data);

			// Add info messages if needed
			switch( data.current){
				case 'stopped':
					updateMsg = 'Halted - initializing import routines...';
					break;
				case 'running':
					updateMsg = 'Import started';
					break;
				case 'process vehicles':
					updateMsg = 'Importing vehicles...';
					break;
				case 'process images':
					console.log( data);
					if( data.output.images){
						updateMsg = 'Images processed: '+
							data.output.images.added+' added, '+
							data.output.images.ignored+' ignored, '+
							data.output.images.parsed+' parsed, '+
							data.output.images.deleted+' deleted, '+
							data.output.images.count+' total. Time elapsed: '+
							data.output.images.time_elapsed; //+' Start: '+
							//data.output.images.time_start+' End: '+
							//data.output.images.time_end;
						console.log( updateMsg);
					} else {
						updateMsg = 'Vehicles imported: '+
							data.output.counts.added+' added, '+
							data.output.counts.ignored+' ignored, '+
							data.output.counts.updated+' updated, '+
							data.output.counts.parsed+' parsed.'; // Time elapsed: '+
							//data.output.counts.time_elapsed+'. Start: '+
							//data.output.counts.time_start+'. End: '+
							//data.output.counts.time_end;
						console.log( updateMsg);
					}
					break;
				case 'complete':
					updateMsg = 'Import complete!';
					break;
				case 'resetting import state':
					updateMsg = 'Resetting import state...';

				default:
					updateMsg = 'Undefined status: '+data.current;
			}

			updateMsg = timeStamp + ' :: '+updateMsg;
			$('.import-messages').prepend('<div class="updated">'+updateMsg+'</div>');


		},
		complete: function(){

			// Proceed to next loop
			if( importStatus != 'complete') $.ajax( importAjax );
			if( importStatus == 'complete') completeImportForm();
		}

	} // importAjax

	$importStartBtn.on('click', function(e){
		e.preventDefault();

		if( importStatus == 'ready'){
			importStatus = 'running';
			console.log( 'CAF Ajax: '+cafAjaxUrl);
			$importStartBtn.addClass('stop-import button-danger').removeClass('button-primary').text('Stop Import');

			// Start the import process!
			$.ajax( importAjax );

		} else if( importStatus != 'complete'){

			completeImportForm();
		}

		return false;
	}); // getDealers .click

	// Quick reset of import action
	$importResetBtn.on('click', function(e){

		var resetAjax = importAjax;
		resetAjax.url = ajaxurl + '?action=caf_api_reset_import&reset';
		$.ajax( resetAjax);
		completeImportForm();
		location.reload(false);
	});


	// Moves status bar along
	function updateStatBar(status, data){
		// 10% for importing posts, 90% for images
		var progress = null;

		switch(status){
			case 'stopped':
				progress = 0;
				break;
			case 'running':
				progress = 0.02;
				break;
			case 'process vehicles':
				progress = 0.1;
				break;
			case 'process images':
				if( typeof(data.output.images) !== 'undefined'){
					progress = (( data.output.images.parsed / data.output.images.count ) * 0.9) + 0.1;
				}else{
					progress = 0.11;
				}
				break;
			case 'complete':
				progress = 1;
				break;
		} // switch

		return progress;
	}

	// Resets import form
	function completeImportForm(){

		console.log('stopping import...');
		$importStartBtn.addClass('button-primary').removeClass('stop-import button-danger').text('Begin Import');
		$status.text('complete');
		clearInterval(cafRefreshTimer);
		importStatus = 'ready';

		setTimeout(function(){
			$statBar.removeClass('animate-bg active');
			$statBar.children('.progress').width( 0 );
		}, 3000);

	}



}); // document.ready