/*==============================================
=            Custom FWP Filter Hook            =
==============================================*/
function number_format(e,t,n,r){e=(e+"").replace(/[^0-9+\-Ee.]/g,"");var i=!isFinite(+e)?0:+e,s=!isFinite(+t)?0:Math.abs(t),o=typeof r==="undefined"?",":r,u=typeof n==="undefined"?".":n,a="",f=function(e,t){var n=Math.pow(10,t);return""+(Math.round(e*n)/n).toFixed(t)};a=(s?f(i,s):""+Math.round(i)).split(".");if(a[0].length>3){a[0]=a[0].replace(/\B(?=(?:\d{3})+(?!\d))/g,o)}if((a[1]||"").length<s){a[1]=a[1]||"";a[1]+=(new Array(s-a[1].length+1)).join("0")}return a.join(u)}


(function($) {
	$(function() {

		if( typeof(wp) !== 'undefined' ){
			wp.hooks.addAction('facetwp/set_label/slider', function($this) {
	            var facet_name = $this.attr('data-name');

	            if( 'price_range' == facet_name){
	            	var label = '$' + number_format(FWP.settings[facet_name]['lower']);
	            	label += ' — ';
	            	label += '$' + number_format(FWP.settings[facet_name]['upper']);
	            }
	            if( 'year_range' == facet_name){
	            	var label = parseInt(FWP.settings[facet_name]['lower'])
	            		+ ' — '
	            		+ parseInt(FWP.settings[facet_name]['upper']);
	            }

	            $this.find('.facetwp-slider-label').html(label);

	        }, 200);
		}

		$(document).on('change', '.caf-fwp-items-per-page', function() {
			FWP_HTTP.per_page = $(this).val();
			FWP.refresh();
		});

		// Reset form button
		$(document).on('click', '.caf-fwp-reset', function(e) {
			e.preventDefault();
			FWP.reset();
			return false;
		});

		// Controlling widget filter visibility
		$(document).on('click', '.caf-fwp-filter-title.expand-enabled', function(e) {
			$(this).next('.caf-fwp-filter').slideToggle();
		});

		$(document).on('facetwp-refresh', function() {

			// Loading gif
	    	$('.facetwp-template').prepend('<div class="caf-loading"><span class="caf-loading-text">Loading</span> <span class="caf-loading-gif"></span></div>');
	    });

		$(document).on('facetwp-loaded', function() {

			if( window.location.hash !== ""){
				setCookie('caf_pages', window.location.href, 100);
			}

			if( FWP_HTTP.per_page){
				$('.caf-fwp-items-per-page').val( FWP_HTTP.per_page);
			}

			// Scroll back to top
			if( CAF_FWP_scrolltop){
				$('html, body').animate({ scrollTop: $('.caf-inventory-navigation').offset().top }, 500)
;			}

			if( $('.caf-fancybox.iframe').length){
				$('.caf-fancybox.iframe').each(function(){
					var fbHeight = $(this).data('fancybox-height') || 600,
						fbWidth =  $(this).data('fancybox-width') || 400;

					$(this).fancybox({
						autoSize: false,
						autoHeight: false,
						width:fbWidth,
						height:fbHeight,
						scrolling:'yes'
					});

				}); // fancybox each
			} // if iframes

		});


	});
})(jQuery);


function setCookie(c_name,value,exdays){
	var exdate=new Date();
	exdate.setDate(exdate.getDate() + exdays);

	var c_value=escape(value) +	((exdays==null) ? "" : ("; expires="+exdate.toUTCString()));
	document.cookie=c_name + "=" + c_value + '; path=/';
}

function getCookie(c_name){
	var i,x,y,ARRcookies=document.cookie.split(";");

	for (i=0;i<ARRcookies.length;i++){
		x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
		y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
		x=x.replace(/^\s+|\s+$/g,"");

		if (x==c_name){
			return unescape(y);
		}
	}
}