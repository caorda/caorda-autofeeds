(function($) {
	$(function() {


	if( $('.caf-fancybox.iframe').length ){

		$('.caf-fancybox.iframe').each(function(){
			var fbHeight = $(this).data('fancybox-height') || 600,
				fbWidth =  $(this).data('fancybox-width') || 400;

			$(this).fancybox({
				autoSize: false,
				autoHeight: false,
				width:fbWidth,
				height:fbHeight,
				scrolling:'yes'
			});

		});

	}

	});
})(jQuery);

jQuery(document).ready(function($){

	$('.caf-fancybox.iframe').on('click', function(e){

		e.preventDefault();
		return false;
	})

});