/*==============================================
=            ACF Button AJAX Action            =
==============================================*/
function populateVehicleFields(){

	(function($) {

		var afText = $('[data-name="caf_autofeeds_object"]').find('textarea').text(),
			post_id = $('input#post_ID').val(),
			$btn = $('[data-ajax="populateVehicleFields"]'),
			afObject = null;


		try{ afObject = JSON.parse(afText); }
		catch(e){
			alert('Error: could not decode CAF Autofeeds object properly. Please ensure valid JSON.');
			return false;
		}

		console.log( post_id, afObject );

		var cafAjaxUrl = ajaxurl + '?action=caf_reload_data&post_id='+post_id;
		console.log( 'CAF Ajax: '+cafAjaxUrl);

		$.ajax({
			type:"POST",
			url: cafAjaxUrl,
			dataType: 'json',
			data: afObject,
			beforeSend: function(){
				$btn.after('<span class="ajax-loader"></span>');
			},
			success:function(data){

				console.log( data);
				if( typeof(data.status) !== 'undefined' ){
					$btn.parent().append('<div class="updated">'+data.status+': '+data.message+'</div>');
				}

				location.reload(false);
			},
			complete: function(){
				$btn.next('.ajax-loader').remove();
			}
		});

	})(jQuery);


}