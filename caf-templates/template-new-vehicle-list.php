<?php

	global $CAF_Settings;
	get_header();

	// Used for div class and determining sidebar (bottom)
	$template_location = caf_get_tpl_location(__FILE__);

	// Set up title
	if( get_field('caf_year') || get_field('caf_make') || get_field('caf_model')){
		$title = '<span class="caf-year">'.get_field('caf_year').'</span>
        			<span class="caf-make">'.get_field('caf_make').'</span>
        			<span class="caf-model">'.get_field('caf_model').'</span>
        			<span class="caf-trim">'.get_field('caf_trim').'</span>';
	} else{
		$title = '<span class="caf-generic-title">'.get_the_title().'</span>';
	}
?>
	<!-- CAF template-new-vehicle-list.php -->
	<div class="stretch_full container_wrap alternate_color light_bg_color title_container">
		<?php 
			$locations = get_nav_menu_locations(); 
			$items = wp_get_nav_menu_items($locations['avia']);
			_wp_menu_item_classes_by_context($items);
			$breadcrumb = array();

			foreach($items as $k=>$item) {
				if ($item->current_item_ancestor || $item->current) {
					$crumbs[] = '<span typeof="v:Breadcrumb"><a href="'.$item->url.'" title="'.$item->title.'" class="'.($k == 0 ? 'trail-begin ' : '').($k == count($items) ? 'trail-end ' : '').'">'.$item->title.'</a></span>';
				}
			}

		// check if we got posts to display:
		if (have_posts()) :	while (have_posts()) : the_post(); ?>
		<div class="container">
			<h1 class="main-title entry-title">
				<a itemprop="headline" title="Permanent Link: <?php the_title(); ?>" rel="bookmark" href="<?php echo get_permalink(); ?>"><?php echo $title; ?></a>
			</h1>
			<div class="breadcrumb breadcrumbs avia-breadcrumbs">
				<div xmlns:v="http://rdf.data-vocabulary.org/#" class="breadcrumb-trail">
					<span class="trail-before">
						<span class="breadcrumb-title">You are here:</span>
					</span>
					<?php echo implode('<span class="sep">/</span>', $crumbs); ?>
				</div>
			</div>
		</div>
	</div>
	<div class='container_wrap container_wrap_first main_color sidebar_right caf-template-single-inventory-php  caf-location-<?php echo $template_location; ?>'>

		<div class='container template-single-caf single-caf single-vehicle sidebar_left'>
			
			<main class='content units nine alpha'>


					<article class="<?php echo implode(" ", get_post_class('post-entry post-entry-type-'.$post_format . " " . $post_class . " ".$with_slider)); ?>">

						<div class="entry-content">
							<div class="caf-featured-vehicles">
							<?php $vehicles = get_posts(array('posts_per_page' => -1, 'post_type' => 'caf_featured_vehicle', 'orderby' => 'date', 'order' => 'ASC'));
								foreach ($vehicles as $vehicle) : ?>
								<div class="caf-featured-vehicle">
									<div class="caf-featured-vehicle-image-container">
										<a href="<?php echo get_permalink($vehicle->ID); ?>" title="<?php echo $vehicle->post_title; ?>"><?php echo get_the_post_thumbnail($vehicle->ID, 'medium'); ?></a>
									</div>
									<div class="caf-featured-vehicle-title-container">
										<a href="<?php echo get_permalink($vehicle->ID); ?>" class="caf-featured-vehicle-title-link" title="<?php echo $vehicle->post_title; ?>"><?php echo $vehicle->post_title; ?></a>
									</div>
									<div class="caf-featured-vehicle-price-container">
										<?php echo caf_cur_format(get_field('caf_regular_price', $vehicle->ID)); ?>
									</div>
								</div>		
								<?php endforeach; wp_reset_query(); ?>
							</div>
						</div>


					</article>

				<?php endwhile; else:?>

					<article class="entry">
						<header class="entry-content-header">
							<h1 class='post-title entry-title'><?php _e('Nothing Found', 'caorda_autofeeds'); ?></h1>
						</header>

						<p class="entry-content" ><?php _e('Sorry, no posts matched your criteria', 'caorda_autofeeds'); ?></p>

						<footer class="entry-footer"></footer>
					</article>

				<?php endif; // end loop! ?>





			<!--end content-->
			</main>

			<?php
				// get sidebar
				if( $template_location == 'plugin'){
					include('sidebar-caf-featured-listpage.php');
				} else{
					get_template_part('caf-templates/sidebar-caf-featured-listpage');
				}
			?>


		</div><!--end container-->

	</div><!-- close default .container_wrap element -->


<?php get_footer(); ?>