<?php

	global $CAF_Settings;
	get_header();

	// Used for div class and determining sidebar (bottom)
	$template_location = caf_get_tpl_location(__FILE__);

	// Prearrange galleries
	$ids = array();
	if( $imgs = get_field('caf_gallery')){
		foreach($imgs as $img ){
			$ids[] = $img['id'];
		}

		$imgs_list = implode(',', $ids);
	}


	$colorIds = array();
	$colorsGalleries = array();
	$colorImages = array();
	if (have_rows('caf_color_picker')) {
		while (have_rows('caf_color_picker')) {
			the_row();

			$colorIds[] = get_sub_field('color');
			$colorsGalleries[] = get_sub_field('image');
		}
	}

	
	// Set up title
	$title = '<span class="caf-generic-title">'.get_the_title().'</span>';
?>
	<!-- CAF single.php -->
	<div class="stretch_full container_wrap alternate_color light_bg_color title_container">
		<?php 
			$locations = get_nav_menu_locations(); 
			$items = wp_get_nav_menu_items($locations['avia']);
			_wp_menu_item_classes_by_context($items);
			$breadcrumb = array();

			foreach($items as $k=>$item) {
				if ($item->current_item_ancestor || $item->current) {
					$crumbs[] = '<span typeof="v:Breadcrumb"><a href="'.$item->url.'" title="'.$item->title.'" class="'.($k == 0 ? 'trail-begin ' : '').($k == count($items) ? 'trail-end ' : '').'">'.$item->title.'</a></span>';
				}
			}

		// check if we got posts to display:
		if (have_posts()) :	while (have_posts()) : the_post(); ?>
		<div class="container">
			<h1 class="main-title entry-title">
				<a itemprop="headline" title="Permanent Link: <?php the_title(); ?>" rel="bookmark" href="<?php echo get_permalink(); ?>"><?php echo $title; ?></a>
			</h1>
			<div class="breadcrumb breadcrumbs avia-breadcrumbs">
				<div xmlns:v="http://rdf.data-vocabulary.org/#" class="breadcrumb-trail">
					<span class="trail-before">
						<span class="breadcrumb-title">You are here:</span>
					</span>
					<?php echo implode('<span class="sep">/</span>', $crumbs); ?>
				</div>
			</div>
		</div>
	</div>
	<div class='container_wrap container_wrap_first main_color sidebar_right caf-template-single-inventory-php  caf-location-<?php echo $template_location; ?>'>

		<div class='container template-single-caf single-caf single-vehicle sidebar_left'>

			<?php if($CAF_Settings['opt-caf-detail-back-buttons']['top']) : if(get_option('caf_back_button') == true) : ?>
				<div class="caf-back caf-back-top">
					<a href="#"><?php echo $CAF_Settings['opt-caf-detail-back-button-text']; ?></a>
				</div>
			<?php endif; endif; ?>
			
			<main class='content units nine alpha'>




					<article class="<?php echo implode(" ", get_post_class('post-entry post-entry-type-'.$post_format . " " . $post_class . " ".$with_slider)); ?>">

						<header>
							<div class="caf-vehicle-intro clearfix">
								<div class="caf-float-left">
									<h1><?php echo $title; ?></h1>
								</div>
								<div class="caf-price caf-float-right">
									<h2>
										<span class="caf-from">
											From:
										</span>
										<span class="caf-regular-price">
											<?php echo caf_cur_format(get_field('caf_regular_price', $post->ID) ); ?>
										</span>
									</h2>
								</div>
							</div>
						</header>

						<div class="entry-content">

							<?php /* *******************************************

								Section: Large Gallery
								rendermode="background" renderheight="460px"
							********************************************** */ ?>

							<?php if($CAF_Settings['opt-caf-detail-show-large-gallery']) : if( get_field('caf_display_galleries')): ?>
								<div class="caf-gallery-large caf-galleries clearfix">
									<?php echo do_shortcode('[gallery wpowlcarousel_render_mode="wp_owl_carousel"
									fb_theme="light" image_size="owl_slider_large" carouselname="caf_vehicle_main"
									items="1" singleitem="true" fb_caption_pos="outside" openeffect="drop"
									closeeffect="drop" fb_link_class="noLightbox" enablefancybox="true"
									autoheight="true" autoplay="true" lazyload="false" navigation="true"
									pagination="true" afterinit="caf_parent_afterinit" ids="'.$imgs_list.'"]'); ?>
								</div>
							<?php endif; endif; ?>

							<?php /* *******************************************

								Section: Color Picker Galleries
								rendermode="background" renderheight="460px"
							********************************************** */ ?>
							<?php if(get_field('caf_display_color_picker')) : ?>
								<?php if (count($colorIds) > 0) {
									foreach ($colorIds as $k=>$colorId) { ?>
										<div class="caf-gallery-large caf-color-picker-large caf-color-picker-<?php echo $k; ?> clearfix">
											<?php echo '<img src="'.$colorsGalleries[$k].'" alt="" />'; ?>
										</div>
								<?php }
								} ?>
							<?php endif; ?>

							<?php /* *******************************************

								Section: Small Gallery

							********************************************** */ ?>
							<?php if($CAF_Settings['opt-caf-detail-show-small-gallery']) : if( get_field('caf_display_galleries') && count($imgs_list > 0)): ?>

								<div class="caf-gallery-small caf-galleries caf-section clearfix">
									<?php echo do_shortcode('[gallery wpowlcarousel_render_mode="wp_owl_carousel"
									image_size="medium" items="6" fb_theme="light" fb_link_class="noLightbox"
									openeffect="drop" closeeffect="drop" lazyload="true" itemsdesktop="{800,4}"
									itemsdesktopsmall="{600,6}" itemstablet="{400,6}" itemstabletsmall="false"
									itemsmobile="{320,6}" autoplay="false" fb_caption_pos="outside" itemsScaleUp="true"
									parentcarousel="caf_vehicle_main" carouselname="caf_vehicle_thumbs"
									navigation="true" pagination="false" disableclicks="true" afterinit="caf_child_afterinit"
									navigationtext="{<span class=\'avia-font-entypo-fontello\'></span>,<span class=\'avia-font-entypo-fontello\'></span>}" 
									rendermode="background" renderheight="80px" ids="'.$imgs_list.'"]'); ?>
								</div>

							<?php endif; endif; ?>

							<?php /* *******************************************

								Section: Vehicle Metaboxes

							********************************************** */ ?>
							<div class="caf-section caf-metaboxes caf-clearfix">
								<?php if (get_field('caf_display_color_picker') && count($colorIds) > 0) : ?>
									<div class="caf-color-picker-cta caf-metabox">
										<h3>Choose a color</h3>
										<ul class="caf-color-picker-list">
											<?php
												foreach ($colorIds as $k=>$colorId) { ?>
													<li class="caf-color-picker-item <?php if ($k == 0) echo 'first'; ?> <?php if ($k == (count($colorIds) - 1)) echo 'last'; ?>">
														<a class="caf-color-picker-color" style="background-color: <?php echo $colorId; ?>" data-id="<?php echo $k; ?>"></a>
													</li> 
												<?php } ?>
										</ul>
									</div>
								<?php endif; ?>

								<?php if($CAF_Settings['opt-caf-detail-show-small-gallery']) : if (get_field('caf_display_galleries') && count($imgs_list) > 0) : ?>
									
										
											<a class="caf-gallery-button caf-gallery-cta caf-metabox selected" href="#main">
												View Gallery
											</a>
										
									
								<?php endif; endif; ?>

								<?php if (get_field('caf_display_build_cta')) : ?>
									
										
											<a href="<?php echo get_field('caf_build_cta_link'); ?>" class="<?php echo get_field('caf_build_link_class'); ?> caf-build-cta caf-metabox" target="_blank">
												Build
											</a>
										
									
								<?php endif; ?>

								<?php if (get_field('caf_display_compare_cta')) : ?>
									
										
											<a href="<?php echo get_field('caf_compare_cta_link'); ?>" class="<?php echo get_field('caf_compare_link_class'); ?> caf-compare-cta caf-metabox" target="_blank">
												Compare
											</a>
										
									
								<?php endif; ?>
							</div>

							<?php /* *******************************************

								Section: Main Content

							********************************************** */ ?>

							<?php if($CAF_Settings['opt-caf-detail-show-page-content']) : if( get_field('caf_display_content')): ?>
								<div class="caf-content clearfix">
									<?php the_content(); ?>
								</div>
							<?php endif; endif; ?>

							<?php /* *******************************************

								Section: Vehicle Specifications Multiselect

							********************************************** */ ?>

							<?php if($CAF_Settings['opt-caf-detail-show-summary']) : if(get_field('caf_show_specifications') && ($summary = get_field('caf_specifications') ) ): ?>
								<div class="caf-features-list caf-section">
									<h3>Specs</h3>
									<div class="caf-summary-box">
										<div class="caf-summary-labels">
										<?php foreach ($summary as $k=>$summary_item) { ?>
											<div class="caf-summary-label">
												<a class="caf-summary-link caf-summary-link-<?php echo $k; ?> <?php if ($k == 0) echo 'current'; ?>" data-id="<?php echo $k; ?>"><?php echo $summary_item['label']; ?></a>
											</div>
										<?php } ?>
										</div>
										<div class="caf-summary-content-area">
											<?php foreach ($summary as $k=>$summary_item) { ?>
												<div class="caf-summary-content caf-summary-content-<?php echo $k; ?> <?php if ($k == 0) echo 'current'; ?>" data-id="<?php echo $k ?>">
													<?php echo $summary_item['content']; ?>
												</div>
											<?php } ?>
										</div>
									</div>
								</div>
							<?php endif; endif; ?>

							<?php /* *******************************************

								Section: Vehicle Specifications Multiselect

							********************************************** */ ?>

							<?php if($CAF_Settings['opt-caf-detail-show-summary']) : if(get_field('caf_show_specifications') && ($summary = get_field('caf_specifications_pdf'))): ?>
								<div class="caf-features-pdf caf-section">
									<a href="<?php echo $summary; ?>" class="caf-features-pdf-text"><span>Download as a PDF</span></a>&nbsp;<a href="<?php echo $summary; ?>" class="caf-features-pdf-image"><img src="<?php echo plugins_url('caorda-autofeeds/img/icon-pdf.png'); ?>" alt="Download as a PDF" /></a>
								</div>
							<?php endif; endif; ?>

							<?php /* *******************************************

								Section: Vehicle Call To Action

							********************************************** */ ?>

							<?php if($CAF_Settings['opt-caf-detail-show-cta']) : if( get_field('caf_display_cta') ): ?>
								<div class="caf-cta  caf-section clearfix">

									<div class="cta-wrap">
										<div class="cta-content">
											<a href="<?php echo $CAF_Settings['opt-caf-detail-cta-link']; ?>" class="caf-cta-icon">
												<?php echo $CAF_Settings['opt-caf-detail-cta-icon']; ?>
											</a>
											<?php echo apply_filters('the_content', $CAF_Settings['opt-caf-detail-cta-contents']); ?>
										</div>
									</div>

								</div>
							<?php endif; endif; ?>


							<?php /* *******************************************

								Section: Additional Features

							********************************************** */ ?>

							<?php if($CAF_Settings['opt-caf-detail-show-additional']) : if( get_field('caf_display_features') && ($features = get_field('caf_additional_features'))): ?>
								<div class="caf-features caf-section clearfix">
									<h3>Additional Features</h3>
									<ul>
										<?php foreach($features as $k=>$feat): $parity = ($k%2) ? 'even' : 'odd';?>
											<li class="item-<?php echo $k; ?> p-<?php echo $parity; ?>">
												<?php echo $CAF_Settings['opt-caf-detail-additional-list-icon']; ?>
												<span class="feature"><?php echo $feat['feature']; ?></span>
											</li>
										<?php endforeach; ?>
									</ul>
								</div>
							<?php endif; endif; ?>

						</div>

						<footer>
							<?php if($CAF_Settings['opt-caf-detail-back-buttons']['bottom']) : if(get_option('caf_back_button') == true) : ?>
								<div class="caf-back caf-back-bottom">
									<a href="#"><?php echo $CAF_Settings['opt-caf-detail-back-button-text']; ?></a>
								</div>
							<?php endif; endif; ?>
						</footer>



					</article>

				<?php endwhile; else:?>

					<article class="entry">
						<header class="entry-content-header">
							<h1 class='post-title entry-title'><?php _e('Nothing Found', 'caorda_autofeeds'); ?></h1>
						</header>

						<p class="entry-content" ><?php _e('Sorry, no posts matched your criteria', 'caorda_autofeeds'); ?></p>

						<footer class="entry-footer"></footer>
					</article>

				<?php endif; // end loop! ?>





			<!--end content-->
			</main>

			<?php
				// get sidebar
				if( $template_location == 'plugin'){
					include('sidebar-caf-featured-detailpage.php');
				} else{
					get_template_part('caf-templates/sidebar-caf-featured-detailpage');
				}
			?>


		</div><!--end container-->

	</div><!-- close default .container_wrap element -->


<?php get_footer(); ?>