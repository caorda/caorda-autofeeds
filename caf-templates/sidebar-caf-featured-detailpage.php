<aside class="sidebar caf-sidebar-featured-detailpage sidebar_left three units">
    <div class='inner_sidebar extralight-border'>

        <?php
            if (get_field('caf_show_featured_vehicles_as_navigation')) {
                $pages = get_posts(array('posts_per_page' => -1, 'post_type' => 'caf_featured_vehicle', 'orderby' => 'date', 'order' => 'ASC'));
                echo '<nav class="widget widget_nav_menu widget_nav_hide_child">';
                echo '<ul class="nested-nav">';
                foreach ($pages as $page) {
                    echo '<li class="page_item page-item-'.$page->ID.' '.(get_permalink($page->ID) == get_permalink() ? 'current_page_item' : '').'"><a href="'.get_permalink($page->ID).'">'.$page->post_title.'</a></li>';
                }
                echo '</ul>';
                echo '</nav>';
            }
            if( is_active_sidebar( 'caf-featured-detail-page-sidebar' ) ){
                dynamic_sidebar('caf-featured-detail-page-sidebar');
            } else{
                echo '<!-- empty sidebar -->';
            }

        ?>

    </div>
</aside>