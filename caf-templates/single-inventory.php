<?php

	global $CAF_Settings;
	get_header();

	// Used for div class and determining sidebar (bottom)
	$template_location = caf_get_tpl_location(__FILE__);

	// Prearrange galleries
	$ids = array();
	if( $imgs = get_field('caf_gallery')){
		foreach($imgs as $img ){
			$ids[] = $img['id'];
		}

		$imgs_list = implode(',', $ids);
	}

	$sale_item = get_field('caf_sale_price');
	$sale_item = $sale_item ? 'sale-item' : '';

	// Set up title
	if( get_field('caf_year') || get_field('caf_make') || get_field('caf_model')){
		$title = '<span class="caf-year">'.get_field('caf_year').'</span>
        			<span class="caf-make">'.get_field('caf_make').'</span>
        			<span class="caf-model">'.get_field('caf_model').'</span>
        			<span class="caf-trim">'.get_field('caf_trim').'</span>';
	} else{
		$title = '<span class="caf-generic-title">'.get_the_title().'</span>';
	}

	// CarProof URL
	$carproof_link = caf_get_carproof_url($post->ID);

?>
	<!-- CAF single.php -->
	<div class='container_wrap container_wrap_first main_color sidebar_right caf-template-single-inventory-php  caf-location-<?php echo $template_location; ?>'>

		<div class='container template-single-caf single-caf single-vehicle caf-clearfix'>

			<?php if($CAF_Settings['opt-caf-detail-back-buttons']['top']) : ?>
				<div class="caf-back caf-back-top">
					<a href="#"><?php echo $CAF_Settings['opt-caf-detail-back-button-text']; ?></a>
				</div>
			<?php endif; ?>

			<main class='content units nine alpha'>

				<?php

				// check if we got posts to display:
				if (have_posts()) :	while (have_posts()) : the_post(); ?>


					<article class="<?php echo implode(" ", get_post_class('post-entry post-entry-type-'.$post_format . " " . $post_class . " ".$with_slider)); ?>">

						<header>
							<div class="caf-vehicle-intro caf-clearfix">
								<div class="caf-float-left">
									<h1><?php echo $title; ?></h1>
								</div>
								<div class="caf-price caf-float-right <?php echo $sale_item; ?>">
									<h2>
										<span class="caf-sale-price">
											<?php echo caf_cur_format(get_field('caf_sale_price', $post->ID), '<span class="prefix">SALE</span>' ); ?>
										</span>
	        							<span class="caf-regular-price">
	        								<?php echo caf_cur_format(get_field('caf_regular_price', $post->ID) ); ?>
	        							</span>
									</h2>
								</div>
							</div>

							<div class="caf-header-widget">

								<?php
										if( is_active_sidebar( 'caf-inventory-header-page-sidebar' ) ){
							                dynamic_sidebar('caf-inventory-header-page-sidebar');
							            }
						            ?>
						    </div>

						</header>

						<div class="entry-content">

							<?php /* *******************************************

								Section: Main Content

							********************************************** */ ?>

							<?php if($CAF_Settings['opt-caf-detail-show-page-content']) : if( get_field('caf_display_content')): ?>
								<div class="caf-content caf-clearfix">
									<?php the_content(); ?>
								</div>
							<?php endif; endif; ?>

							<?php /* *******************************************

								Section: Large Gallery
								rendermode="background" renderheight="460px"
							********************************************** */ ?>

							<?php if($CAF_Settings['opt-caf-detail-show-large-gallery']) : if( get_field('caf_display_galleries')): ?>
								<div class="caf-gallery-large caf-clearfix">
									<?php echo do_shortcode('[gallery wpowlcarousel_render_mode="wp_owl_carousel"
									fb_theme="light" image_size="owl_slider_large" carouselname="caf_vehicle_main"
									items="1" singleitem="true" fb_caption_pos="outside" openeffect="drop"
									closeeffect="drop" fb_link_class="noLightbox" enablefancybox="true"
									autoheight="true"
									pagination="true" afterinit="caf_parent_afterinit" ids="'.$imgs_list.'"]'); ?>
								</div>
							<?php endif; endif; ?>

							<?php /* *******************************************

								Section: Small Gallery

							********************************************** */ ?>
							<?php if($CAF_Settings['opt-caf-detail-show-small-gallery']) : if( get_field('caf_display_galleries')): ?>

								<div class="caf-gallery-small caf-section caf-clearfix">
									<?php echo do_shortcode('[gallery wpowlcarousel_render_mode="wp_owl_carousel"
									image_size="medium" items="6" fb_theme="light" fb_link_class="noLightbox"
									openeffect="drop" closeeffect="drop" lazyload="true" itemsdesktop="{800,4}"
									itemsdesktopsmall="{600,6}" itemstablet="{400,6}" itemstabletsmall="false"
									itemsmobile="{320,6}" autoplay="false" fb_caption_pos="outside" itemsScaleUp="true"
									parentcarousel="caf_vehicle_main" carouselname="caf_vehicle_thumbs"
									navigation="true" pagination="false" disableclicks="true" afterinit="caf_child_afterinit"
									navigationtext="{<span class=\'avia-font-entypo-fontello\'></span>,<span class=\'avia-font-entypo-fontello\'></span>}" 
									rendermode="background" renderheight="80px" ids="'.$imgs_list.'"]'); ?>
								</div>

							<?php endif; endif; ?>

							<?php /* *******************************************

								Section: CarProof Button

							********************************************** */ ?>

							<?php if($CAF_Settings['opt-caf-enable-carproof']) : if( $carproof_link['url']): ?>
								<div class="caf-carproof">
									<a href="<?php echo $carproof_link['url'] ?>" class="caf-fancybox iframe <?php echo $carproof_link['class']; ?>" <?php echo $carproof_link['data']; ?> title="CarProof Link">
										<img src="<?php echo $CAF_Settings['opt-caf-detail-carproof-logo']['url']; ?>" alt="CarProof Report" />
									</a>
								</div>
							<?php endif; endif; ?>

							<?php /* *******************************************

								Section: Vehicle Description

							********************************************** */ ?>

							<?php if($CAF_Settings['opt-caf-detail-show-description']) : if( get_field('caf_display_description') && get_field('caf_description')): ?>
								<div class="caf-description caf-section">
									<h3>Vehicle Description</h3>
									<?php the_field('caf_description'); ?>
								</div>
							<?php endif; endif; ?>

							<?php /* *******************************************

								Section: Vehicle Specification Slider

							********************************************** */ ?>

							<?php if($CAF_Settings['opt-caf-detail-show-summary']) : if( get_field('caf_display_features_list') && ($summary = get_field('caf_summary_features_list') ) ): ?>
								<div class="caf-features-list caf-section">
									<h3>Features</h3>
									<?php print_r($summary); ?>
								</div>
							<?php endif; endif; ?>

							<?php /* *******************************************

								Section: Vehicle Summary

							********************************************** */ ?>

							<?php if($CAF_Settings['opt-caf-detail-show-summary']) : if( get_field('caf_display_summary') && ($summary = get_field('caf_summary_features') ) ): ?>
								<div class="caf-summary caf-section caf-clearfix">
									<h3>Summary Features</h3>

									<ul>
										<?php $dualparity = 0; $count = 0;
											foreach($summary as $k=>$summ):
												$parity = ($count%2) ? 'odd' : 'even';
												if($count%2 == 0) $dualparity++;
												$dpstr = ($dualparity%2) ? 'odd' : 'even';
												if( $summ['text']):
													/* TODO - CONVERT THIS TO A UL */
										?>
											<li class="sum-item item-<?php echo $count; ?> p-<?php echo $parity; ?> dp-<?php echo $dpstr; ?>">
												<label><?php echo $summ['feature']; ?>:</label>
												<span><?php echo $summ['text']; ?></span>
											</li>
										<?php $count++; endif; endforeach; ?>
									</ul>

								</div>
							<?php endif; endif; ?>



							<?php /* *******************************************

								Section: Vehicle Call To Action

							********************************************** */ ?>

							<?php if($CAF_Settings['opt-caf-detail-show-cta']) : if( get_field('caf_display_cta') ): ?>
								<div class="caf-cta  caf-section caf-clearfix">

									<div class="cta-wrap">
										<div class="cta-content">
											<a href="<?php echo $CAF_Settings['opt-caf-detail-cta-link']; ?>" class="caf-cta-icon">
												<?php echo $CAF_Settings['opt-caf-detail-cta-icon']; ?>
											</a>
											<?php echo apply_filters('the_content', $CAF_Settings['opt-caf-detail-cta-contents']); ?>
										</div>
									</div>

								</div>
							<?php endif; endif; ?>


							<?php /* *******************************************

								Section: Additional Features

							********************************************** */ ?>

							<?php if($CAF_Settings['opt-caf-detail-show-additional']) : if( get_field('caf_display_features') && ($features = get_field('caf_additional_features'))): ?>
								<div class="caf-features caf-section caf-clearfix">
									<h3>Additional Features</h3>
									<ul>
										<?php foreach($features as $k=>$feat): $parity = ($k%2) ? 'even' : 'odd';?>
											<li class="item-<?php echo $k; ?> p-<?php echo $parity; ?>">
												<?php echo $CAF_Settings['opt-caf-detail-additional-list-icon']; ?>
												<span class="feature"><?php echo $feat['feature']; ?></span>
											</li>
										<?php endforeach; ?>
									</ul>
								</div>
							<?php endif; endif; ?>

						</div>

						<footer>
							<?php if($CAF_Settings['opt-caf-detail-back-buttons']['bottom']) : ?>
								<div class="caf-back caf-back-bottom">
									<a href="#"><?php echo $CAF_Settings['opt-caf-detail-back-button-text']; ?></a>
								</div>
							<?php endif; ?>
						</footer>



					</article>

				<?php endwhile; else:?>

					<article class="entry">
						<header class="entry-content-header">
							<h1 class='post-title entry-title'><?php _e('Nothing Found', 'caorda_autofeeds'); ?></h1>
						</header>

						<p class="entry-content" ><?php _e('Sorry, no posts matched your criteria', 'caorda_autofeeds'); ?></p>

						<footer class="entry-footer"></footer>
					</article>

				<?php endif; // end loop! ?>





			<!--end content-->
			</main>

			<?php
				// get sidebar
				if( $template_location == 'plugin'){
					include('sidebar-caf-detailpage.php');
				} else{
					get_template_part('caf-templates/sidebar-caf-detailpage');
				}
			?>


		</div><!--end container-->

	</div><!-- close default .container_wrap element -->


<?php get_footer(); ?>