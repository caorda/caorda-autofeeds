<aside class="sidebar caf-sidebar caf-sidebar-detailpage sidebar_ three units">
    <div class='inner_sidebar extralight-border'>

        <?php

            if( is_active_sidebar( 'caf-list-page-sidebar' ) ){
                dynamic_sidebar('caf-list-page-sidebar');
            } else{
                echo '<!-- empty sidebar -->';
            }

        ?>

    </div>
</aside>