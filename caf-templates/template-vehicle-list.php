<?php
global $CAF_Settings;

	wp_enqueue_script('fancybox3', plugins_url().'/caorda-autofeeds/assets/fancybox3/jquery.fancybox.js', array('jquery'), '3.0', true);
	//wp_print_scripts('fancybox3');


	/*
	 * get_header is a basic wordpress function, used to retrieve the header.php file in your theme directory.
	 */
	add_action('wp_enqueue_scripts', 'caf_add_scripts');
	get_header();


	 ?>

		<div class='container_wrap container_wrap_first main_color caf-template caf-template-vehicle-list-php caf-child-theme'>

			<div class='container caf-clearfix'>
				<div class="caf-inventory-header caf-clearfix">

					<?php if( $caf_page_title = $CAF_Settings['opt-caf-vehicle-list-page-title']): ?>
						<h1 class="caf-page-title"><?php echo $caf_page_title; ?></h1>
					<?php endif; ?>

					<div class="caf-inventory-navigation">
						<?php
							echo do_shortcode('[facetwp counts="true"]');
							echo do_shortcode('[facetwp pager="true"]');
							echo do_shortcode('[caf_facetwp itemsperpage="true"]');
							echo do_shortcode('[facetwp sort="true"]');

						?>
					</div>
				</div>

				<main class='template-page content units'>

					<div class="caf-fwp-current-filters caf-clearfix">
						<?php
							echo do_shortcode('[facetwp selections="true"]');
							//echo do_shortcode('[facetwp counts="true"]');
						?>
					</div>
					<?php
					/* Run the loop to output the posts.
					* If you want to overload this in a child theme then include a file
					* called loop-page.php and that will be used instead.
					*/

					echo do_shortcode('[facetwp template="caf_inventory"]');

					?>

					<div class="caf-inventory-footer">
						<?php echo do_shortcode('[facetwp pager="true"]'); ?>
					</div>

				<!--end content-->
				</main>

				<?php

				//get the sidebar
				include('sidebar-caf-listpage.php');

				?>

			</div><!--end container-->

		</div><!-- close default .container_wrap element -->



<?php get_footer(); ?>