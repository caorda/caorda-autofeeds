<?php

// Plugin files
include_once('CaordaAutofeeds_API.php');
include_once('CaordaAutofeeds_Ajax.php');
include_once('CaordaAutofeeds_Misc.php');
include_once('CaordaAutofeeds_FacetWP.php');
include_once('CaordaAutofeeds_LifeCycle.php');
include_once('CaordaAutofeeds_PostTypes.php');
include_once('CaordaAutofeeds_TaxMetaClass.php');
include_once('CaordaAutofeeds_Cron.php');
include_once('CaordaAutofeeds_Templater.php');
include_once('CaordaAutofeeds_ImportVehicles.php');
include_once('CaordaAutofeeds_WidgetSearch.php');
include_once('CaordaAutofeeds_WidgetFilters.php');
include_once('CaordaAutofeeds_WidgetDealerLinks.php');
include_once('CaordaAutofeeds_WidgetSharingLinks.php');
include_once('CaordaAutofeeds_WidgetRecentlyViewed.php');

class CaordaAutofeeds_Plugin extends CaordaAutofeeds_LifeCycle {

	/**
	 * See: http://plugin.michael-simpson.com/?page_id=31
	 * @return array of option meta data.
	 */
	public function getOptionMetaData() {
		//  http://plugin.michael-simpson.com/?page_id=31
		return array(
			//'_version' => array('Installed Version'), // Leave this one commented-out. Uncomment to test upgrades.
			'ATextInput' => array(__('Enter in some text', 'caorda-autofeeds')),
			'Donated' => array(__('I have donated to this plugin', 'caorda-autofeeds'), 'false', 'true'),
			'CanSeeSubmitData' => array(__('Can See Submission data', 'caorda-autofeeds'),
										'Administrator', 'Editor', 'Author', 'Contributor', 'Subscriber', 'Anyone')
		);
	}

//    protected function getOptionValueI18nString($optionValue) {
//        $i18nValue = parent::getOptionValueI18nString($optionValue);
//        return $i18nValue;
//    }

	protected function initOptions() {
		$options = $this->getOptionMetaData();
		if (!empty($options)) {
			foreach ($options as $key => $arr) {
				if (is_array($arr) && count($arr > 1)) {
					$this->addOption($key, $arr[1]);
				}
			}
		}
	}

	public function getPluginDisplayName() {
		return 'Caorda AutoFeeds';
	}

	protected function getMainPluginFileName() {
		return 'caorda-autofeeds.php';
	}

	/**
	 * See: http://plugin.michael-simpson.com/?page_id=101
	 * Called by install() to create any database tables if needed.
	 * Best Practice:
	 * (1) Prefix all table names with $wpdb->prefix
	 * (2) make table names lower case only
	 * @return void
	 */
	protected function installDatabaseTables() {
		//        global $wpdb;
		//        $tableName = $this->prefixTableName('mytable');
		//        $wpdb->query("CREATE TABLE IF NOT EXISTS `$tableName` (
		//            `id` INTEGER NOT NULL");
	}

	/**
	 * See: http://plugin.michael-simpson.com/?page_id=101
	 * Drop plugin-created tables on uninstall.
	 * @return void
	 */
	protected function unInstallDatabaseTables() {
		//        global $wpdb;
		//        $tableName = $this->prefixTableName('mytable');
		//        $wpdb->query("DROP TABLE IF EXISTS `$tableName`");
	}


	/**
	 * Perform actions when upgrading from version X to version Y
	 * See: http://plugin.michael-simpson.com/?page_id=35
	 * @return void
	 */
	public function upgrade() {
	}

	public function addActionsAndFilters() {
		global $CAF_Settings;
		if(!$CAF_Settings) $CAF_Settings = get_option('CAF_Settings', array());

		// Switch to manual cron job if set
		if($CAF_Settings['opt-caf-wp-cron-disable']){
			if( !defined('DISABLE_WP_CRON') ) 		define('DISABLE_WP_CRON', 'true');
			if( !defined('WP_CRON_LOCK_TIMEOUT') ) 	define('WP_CRON_LOCK_TIMEOUT', 60*3);
		}


		// Add options administration page
		// http://plugin.michael-simpson.com/?page_id=47
		add_action('admin_menu', array(&$this, 'addSettingsSubMenuPage'));

		// Example adding a script & style just for the options administration page
		// http://plugin.michael-simpson.com/?page_id=47
		//        if (strpos($_SERVER['REQUEST_URI'], $this->getSettingsSlug()) !== false) {
		//            wp_enqueue_script('my-script', plugins_url('/js/my-script.js', __FILE__));
		//            wp_enqueue_style('my-style', plugins_url('/css/my-style.css', __FILE__));
		//        }

		//add_action('wp_enqueue_scripts', array(&$this, 'caf_add_scripts') ); // generates warning if inside class, why?
		add_action('wp_enqueue_scripts', 'caf_add_scripts');
		add_action('admin_enqueue_scripts', 'caf_add_admin_scripts');


		wp_enqueue_style('caf', plugins_url('/css/caf.css', __FILE__));
		wp_enqueue_style('caf_max', plugins_url('/css/caf_max.css', __FILE__));

		// Add Sidebars
		add_action('init', array(&$this, 'addSidebars') );


		// Add custom capabilities
		add_action( 'admin_init', array(&$this, 'add_caf_caps') );

		// Add Actions & Filters
		// http://plugin.michael-simpson.com/?page_id=37
		add_image_size( 'caf_vehicle_bar', 85, 50, false );
		add_image_size( 'caf_vehicle_list', 300, 260, false );


		// Add custom post status
		add_action( 'init', array(&$this, 'caf_custom_post_status' ) );
		add_action('admin_footer-post.php', array(&$this, 'caf_append_post_status_list') );
		add_filter( 'display_post_states', array(&$this, 'caf_display_archive_state' ) );


		// Post types - CaordaSiteMonitor_PostTypes.php
		add_action( 'init', 'caf_featured_vehicle_post_type', 0 );
		add_action( 'init', 'caf_inventory_post_type', 0 );
		add_action( 'init', 'tax_caf_tag_init'  );
		add_action( 'init', 'tax_caf_category_init'  );
		add_action( 'init', 'tax_caf_media_init'  );
		add_action( 'init', array(&$this, 'caf_dealership_redux_tax_init' ) );



		// Cron tasks - CaordaAutofeeds_Cron.php


		// Handle the dynamic back buttons
		add_action( 'init', array(&$this, 'dynamic_back_buttons') );


		// Custom query adjustments
		add_action( 'posts_request', array(&$this, 'caf_query_alter') );

		// Custom templates for post types
		add_filter( 'template_include', array(&$this, 'caf_template_chooser') );


		// Adding scripts & styles to all pages
		add_action( 'wp_enqueue_scripts', array(&$this, 'add_header_styles' ) );


		// Register short codes
		// http://plugin.michael-simpson.com/?page_id=39
		include_once('CaordaAutofeeds_ShortCode_featured_vehicles.php');
			$sc = new CaordaAutofeeds_ShortCode_featured_vehicles();
			$sc->register('caf_featured_vehicles');

		include_once('CaordaAutofeeds_ShortCode_featured_vehicles_list.php');
			$sc = new CaordaAutofeeds_ShortCode_featured_vehicles_list();
			$sc->register('caf_vehicles_list');

		include_once('CaordaAutofeeds_ShortCode_facet_used_vehicle_list.php');
			$sc = new CaordaAutofeeds_ShortCode_facet_used_vehicle_list();
			$sc->register('caf_facet_used_vehicle_list');


		// Register AJAX hooks
		// http://plugin.michael-simpson.com/?page_id=41
		add_action('wp_ajax_caf_api_get_dealers', 'caf_ajax_get_dealers');
		add_action('wp_ajax_caf_api_get_vehicles', 'caf_ajax_get_vehicles');
		add_action('wp_ajax_caf_api_import_controller', 'caf_ajax_import_controller');
		add_action('wp_ajax_caf_api_reset_import', 'caf_ajax_reset_import');
		add_action('wp_ajax_caf_delete_images', 'caf_ajax_delete_images');
		add_action('wp_ajax_caf_purge_images', 'caf_ajax_purge_images');
		add_action('wp_ajax_caf_delete_inventory', 'caf_ajax_delete_inventory');
		add_action('wp_ajax_caf_repopulate_inventory', 'caf_ajax_repopulate_inventory');
		add_action('wp_ajax_caf_reload_data', 'caf_ajax_reload_data');


		// Init API Vehicles text dump file, if needed
		add_action('init', array(&$this, 'init_dump_file') );

		// Custom admin columns for various things
		add_filter('manage_caf_inventory_posts_columns', array(&$this, 'caf_inventory_columns_head') );
		add_action('manage_caf_inventory_posts_custom_column', array(&$this, 'caf_inventory_columns_content'), 10, 2);

		// Retroactive get_posts hook primarily for adding vehicles to recent view list
		add_action('wp', array(&$this, 'caf_add_recent_view') );

	} // addActionsAndFilters

	public function addSidebars(){

		$sidebars = array(
			array(
				'name'			=> __( 'CAF General Sidebar', 'caorda-autofeeds' ),
				'id'			=> 'caf-general-sidebar',
				'description'	=> 'Visible on all CAF pages',
				'class'			=> '',
				'before_widget' => '<div id="%1$s" class="widget clearfix %2$s">',
				'after_widget' => '<span class="seperator extralight-border"></span></div>',
				'before_title' => '<h3 class="widgettitle">',
				'after_title' => '</h3>',
			),
			array(
				'name'			=> __( 'CAF List Page', 'caorda-autofeeds' ),
				'id'			=> 'caf-list-page-sidebar',
				'description'	=> 'Visible on CAF list pages',
				'class'			=> '',
				'before_widget' => '<div id="%1$s" class="widget clearfix %2$s">',
				'after_widget' => '<span class="seperator extralight-border"></span></div>',
				'before_title' => '<h3 class="widgettitle">',
				'after_title' => '</h3>',
			),
			array(
				'name'			=> __( 'CAF Detail Page', 'caorda-autofeeds' ),
				'id'			=> 'caf-detail-page-sidebar',
				'description'	=> 'Visible on CAF detail pages',
				'class'			=> '',
				'before_widget' => '<div id="%1$s" class="widget clearfix %2$s">',
				'after_widget' => '<span class="seperator extralight-border"></span></div>',
				'before_title' => '<h3 class="widgettitle">',
				'after_title' => '</h3>',
			),
			array(
				'name'			=> __( 'CAF Featured List Page', 'caorda-autofeeds' ),
				'id'			=> 'caf-featured-list-page-sidebar',
				'description'	=> 'Visible on CAF Featured Vehicles list pages',
				'class'			=> '',
				'before_widget' => '<div id="%1$s" class="widget clearfix %2$s">',
				'after_widget' => '<span class="seperator extralight-border"></span></div>',
				'before_title' => '<h3 class="widgettitle">',
				'after_title' => '</h3>',
			),
			array(
				'name'			=> __( 'CAF Featured Detail Page', 'caorda-autofeeds' ),
				'id'			=> 'caf-featured-detail-page-sidebar',
				'description'	=> 'Visible on CAF Featured Vehicles detail pages',
				'class'			=> '',
				'before_widget' => '<div id="%1$s" class="widget clearfix %2$s">',
				'after_widget' => '<span class="seperator extralight-border"></span></div>',
				'before_title' => '<h3 class="widgettitle">',
				'after_title' => '</h3>',
			),
			array(
				'name'			=> __( 'CAF Inventory Header', 'caorda-autofeeds' ),
				'id'			=> 'caf-inventory-header-page-sidebar',
				'description'	=> 'Visible on CAF Inventory detail pages',
				'class'			=> '',
				'before_widget' => '<div id="%1$s" class="widget clearfix %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h3 class="widgettitle">',
				'after_title' => '</h3>',
			),
		); // sidebars

		foreach($sidebars as $sb){
			register_sidebar($sb);
		}

	} // addSidebars

	public function caf_template_chooser($template){

		// Post ID
		$post_id = get_the_ID();
		$ptype = get_post_type( $post_id );

		// For all other CPT
		if ( ($ptype != 'caf_featured_vehicle') && ($ptype != 'caf_inventory') ) {
			return $template;
		}

		// Else use custom template
		if ( is_single() && (get_post_type( $post_id ) == 'caf_featured_vehicle') ) {
			return $this->caf_get_template_hierarchy( 'single-featured' );
		}

		if ( is_single() && (get_post_type( $post_id ) == 'caf_inventory') ) {
			return $this->caf_get_template_hierarchy( 'single-inventory' );
		}


	} // caf_template_chooser

	public function caf_get_template_hierarchy( $template ) {

		// Get the template slug
		$template_slug = rtrim( $template, '.php' );
		$template = $template_slug . '.php';

		// Check if a custom template exists in the theme folder, if not, load the plugin template file
		if ( $theme_file = locate_template( array( 'caf-templates/' . $template ) ) ) {
			$file = $theme_file;
		}
		else {
			$file = CAF_BASE_DIR . '/caf-templates/' . $template;
		}


		//return apply_filters( 'rc_repl_template_' . $template, $file );
		return $file;
	}

	public function add_caf_caps(){
		$role = get_role('administrator');
		$role->add_cap('manage_caf_options');
	}

	public function add_header_styles(){

		global $CAF_Settings;
		if( $bg = array_filter($CAF_Settings['opt-caf-detail-cta-background']) ){
			$output = '.caf-cta .cta-wrap{ ';

			// Removing media to help that foreach along
			unset($bg['media']);

			foreach($bg as $k=>$set){
				if( $k == 'background-image'){
					$set = 'url('.$set.')';
				}
				$output .= $k.':'.$set.'; ';
			}

			$output .= '}';
		}

		wp_add_inline_style( 'caf', $output );

	} // add_header_styles

	public function dynamic_back_buttons(){
		return false;
		/*
		$pages = is_string( $_COOKIE['caf_pages'] ) ? unserialize(stripslashes($_COOKIE['caf_pages'])) : array();

		if( !is_array($pages)) $pages = array();

		$url = 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];

		if( strpos($url, 'wp-admin') === false){
			array_unshift( $pages, $url);
		}

		$pages = array_slice($pages, 0, 3);
		setcookie('caf_pages', serialize($pages), 0, '/');
		*/
	}

	/*================================================
	=            Dealership Taxonomy Init            =
	================================================*/
	public function caf_dealership_redux_tax_init(){
		global $CAF_Settings;
		$dealers = CAF_API::get_dealers();
		if( !taxonomy_exists('caf_dealerships' )){
			$tax = array(
				'hierarchical'	=> true,
				'labels' => array(
					'name'              => _x( 'CAF Dealerships', 'taxonomy general name' ),
					'singular_name'     => _x( 'CAF Dealership', 'taxonomy singular name' ),
					'search_items'      => __( 'Search Dealerships' ),
					'all_items'         => __( 'All Dealerships' ),
					'parent_item'       => __( 'Parent Dealerships' ),
					'parent_item_colon' => __( 'Parent Dealership:' ),
					'edit_item'         => __( 'Edit Dealership' ),
					'update_item'       => __( 'Update Dealership' ),
					'add_new_item'      => __( 'Add New Dealership' ),
					'new_item_name'     => __( 'New Dealership Name' ),
					'menu_name'         => __( 'Dealership' ),
				),
				'show_ui' => true,
				'show_admin_column' => true,
				'query_var' => true,
				'rewrite' => array('slug' => 'dealership')
			);

			register_taxonomy('caf_dealerships', array('caf_featured_vehicle', 'caf_inventory'), $tax);
		} // if taxonomy exists

		//echo '<pre>'.print_r($dealers, true).'</pre>';
		// Add custom meta fields to dealerships
		if( !empty($CAF_Settings['opt-caf-api-dealer-ids'])){
			foreach($CAF_Settings['opt-caf-api-dealer-ids'] as $did){

				$term = term_exists( $dealers[$did]['DealerName'], 'caf_dealerships');
				if(!$term){

					WP_Logging::add( $title = 'CAF Event', $message = 'Event: Adding new Dealer to taxonomy ('.$dealers[$did]['DealerName'].')', $parent = 0, $type = 'event' );

					$term = wp_insert_term( $dealers[$did]['DealerName'], 'caf_dealerships', array(
						'description' => $dealers[$did]['WebSite'],
					));

					if( $term && get_class($term) != 'WP_Error'){
						$term_id = $term['term_id'];

						// Update each meta field from dealer data
						foreach($dealers[$did] as $k=>$field){

							// Handle nested vehicle data
							if( is_array($field)){
								foreach($field as $j=>$sub){
									update_tax_meta($term_id, 'caf_'.$k.'_'.$j, $sub);
								}
							} else{
								update_tax_meta($term_id, 'caf_'.$k, $field);
							}

						} // foreach update tax meta fields
					} else {
						WP_Logging::add( $title = 'CAF Error', $message = 'Error: Adding new Dealer to taxonomy ('.$dealers[$did]['DealerName'].')', $parent = 0, $type = 'error' );
					}

				} else{
					//$term_id = $term['term_id'];
				}

			} // foreach dealers
		} // if dealer IDs set
		else {
			WP_Logging::add( $title = 'CAF Error', $message = 'Error: No dealer selected', $parent = 0, $type = 'error' );
		}

	} // caf_dealership_redux_tax_init

	public function init_dump_file(){
		global $CAF_Settings;

		$file = $CAF_Settings['opt-caf-api-inventory-data-location'];
		$root = $_SERVER['DOCUMENT_ROOT'];

		if( !file_exists(untrailingslashit($root).$file)){
			WP_Logging::add( $title = 'CAF Event', $message = 'Event: Creating vehicle dump file '.$file, $parent = 0, $type = 'event' );

			// Explode directory structure and create if necessary
			$dir = array_values(array_filter(explode('/', $file)));

			// Remove last entry as that is our file
			unset($dir[count($dir)-1]);

			// Work through directories, append to this each time
			$base = $root;
			foreach($dir as $d){
				$base .= '/'.$d;
				if( !is_dir($base)) mkdir($base);
			}

			// Create file
			file_put_contents($root.$file, "");
		}
	}

	/*===================================================
	=            Terminal Custom Post Status            =
	===================================================*/

	public function caf_custom_post_status(){
		$args = array(
			'label'                     => _x( 'terminal', 'Status General Name', 'caf' ),
			'label_count'               => _n_noop( 'Scheduled for Deletion (%s)',  'Scheduled for Deletion (%s)', 'caf' ),
			'public'                    => false,
			'show_in_admin_all_list'    => true,
			'show_in_admin_status_list' => true,
			'exclude_from_search'       => true,
		);
		register_post_status( 'terminal', $args );

	}

	public function caf_append_post_status_list(){
		 global $post;
		 $complete = '';
		 $label = '';
		 if($post->post_type == 'caf_inventory'){
			if($post->post_status == 'terminal'){
				$complete = ' selected=\"selected\"';
				$label = '<span id=\"post-status-display\"> Scheduled for Deletion</span>';
			}
			echo '<script>
				jQuery(document).ready(function($){
				$("select#post_status").append("<option value=\"terminal\" '.$complete.'>Scheduled for Deletion</option>");
				$(".misc-pub-section label").append("'.$label.'");
				});
			</script>';
		}
	}
	public function caf_display_archive_state( $states ) {
		global $post;
		$status = get_query_var( 'post_status' );
		if($status != 'terminal'){
			if($post->post_status == 'terminal'){
				return array('Scheduled for Deletion');
			}
		}
		return $states;
	}

	/*======================================
	=            Custom Columns            =
	======================================*/

	public function caf_inventory_columns_head($defaults) {
		$terminal = strpos($_SERVER['REQUEST_URI'], 'terminal');

		if( $terminal !== FALSE){
	    	$defaults['expires_on'] = 'Expiry Date';
	    }

	    // Inventory image count
    	$defaults['caf_obj_images'] = '# Images';

	    return $defaults;
	}

	// SHOW THE FEATURED IMAGE
	public function caf_inventory_columns_content($column_name, $post_ID) {

		$terminal = strpos($_SERVER['REQUEST_URI'], 'terminal');

		if( $terminal !== FALSE){
		    if ($column_name == 'expires_on') {
		    	$post = get_post($post_ID);
		    	$one_month = 2629743;
		    	$now = date('U');

		    	$modified_date = strtotime($post->post_modified);
		    	$expiry_date = $modified_date + $one_month;
		    	$days_away = ($expiry_date - $now) / 60 / 60 / 24;

		        echo date('M d, Y', $expiry_date);
		        echo '<br /><em> ('.(int) $days_away.' days)</em>';
		    } // expires_on col
		} // if status is terminal

		switch($column_name){

			case 'caf_obj_images':
				$obj = get_field('caf_autofeeds_object', $post_ID);
				if( $json = json_decode($obj, true)) echo count( $json['Photos']);
				else echo "0";
				break;

		} // swtich

	} // caf_inventory_columns_content


	public function caf_add_recent_view($wp){
		global $post;

		// Add vehicle to recent view list
		if( isset($wp->query_vars['post_type']) && ($wp->query_vars['post_type'] == 'caf_inventory')){

			$url = $_SERVER['REQUEST_URI'];

			// Check if cookie exists
			if( isset($_COOKIE['caf_recentview']) && !is_null($_COOKIE['caf_recentview']) ){
				$recent = json_decode( stripslashes( $_COOKIE['caf_recentview']), true);

				// Remove from list (we'll prepend next)
				if( $pos = array_search(urlencode($url), (array)$recent)) unset($recent[$pos]);

				// Add current url to top of list
				$path = array( base64_encode( $post->post_title) => urlencode($url) );
				$recent = (array) $path + (array)$recent;

				// Possibly redundant... no time to check
				setcookie('caf_recentview',  json_encode($recent ), time()+(86700*7), '/' );
				$_COOKIE['caf_recentview'] = json_encode($recent);

			} else{

				// No cookie? Create!
				$path = array( base64_encode( $post->post_title) => urlencode($url) );
				setcookie('caf_recentview',  json_encode($path ), time()+(86700*7), '/' );
			}

		} // if caf_inventory post type

	} // caf_wp_action

	public function caf_query_alter($request){

		// NOT USED ANY MORE, archived
		//echo '<pre>Query: '.print_r($request, true).'</pre>';
		/*
		if( strpos($request, 'caf_custom_query')){
			$request = "SELECT ID FROM
				(SELECT wp_posts.ID, COUNT(*) AS Count
				FROM wp_postmeta
				INNER JOIN wp_posts
				on wp_postmeta.post_id = wp_posts.ID
				WHERE wp_posts.post_type = 'caf_inventory'
				AND (
						(wp_postmeta.meta_key = 'caf_sold_status' AND wp_postmeta.meta_value != 1)
						OR wp_postmeta.meta_key = '_thumbnail_id'
					)
				AND wp_posts.post_status = 'publish'
				GROUP BY wp_postmeta.post_id
				HAVING Count > 0
				ORDER BY Count DESC) AS inputs
				INNER JOIN wp_postmeta ON inputs.ID = wp_postmeta.post_id where
				wp_postmeta.meta_key = 'caf_sold_status' AND wp_postmeta.meta_value != 1;";
		}

		//if( 'caf_inventory' == $q->query_vars['post_type']) echo '<pre>'.print_r($q, true).'</pre>';
		*/

		return $request;
	}


} // class

function caf_add_scripts(){
	global $CAF_Settings;
	if(!$CAF_Settings) $CAF_Settings = get_option('CAF_Settings', array());

	wp_enqueue_script('caf_facetwp', plugins_url('/js/caf-facetwp.js', __FILE__), array('jquery'));
	wp_enqueue_script('caf_custom', plugins_url('/js/caf-custom.js', __FILE__), array('jquery'));

	if( $CAF_Settings['opt-caf-enable-fontawesome']){
		wp_enqueue_style('fontawesome', plugins_url('/assets/fontawesome/css/font-awesome.min.css', __FILE__) );
	}
} // caf_add_scripts


function caf_add_admin_scripts(){

	wp_enqueue_script('caf_admin', plugins_url('/js/caf-admin.js', __FILE__), array('jquery'));

} // caf_add_scripts