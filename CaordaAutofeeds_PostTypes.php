<?php

// Register Custom Post Type
function caf_featured_vehicle_post_type() {

	$labels = array(
		'name'                => _x( 'CAF Featured Vehicles', 'Post Type General Name', 'caorda-autofeeds' ),
		'singular_name'       => _x( 'Featured Vehicle', 'Post Type Singular Name', 'caorda-autofeeds' ),
		'menu_name'           => __( 'CAF Featured Vehicles', 'caorda-autofeeds' ),
		'parent_item_colon'   => __( 'Parent Vehicle:', 'caorda-autofeeds' ),
		'all_items'           => __( 'All Vehicles', 'caorda-autofeeds' ),
		'view_item'           => __( 'View Vehicle', 'caorda-autofeeds' ),
		'add_new_item'        => __( 'Add New Vehicle', 'caorda-autofeeds' ),
		'add_new'             => __( 'Add New', 'caorda-autofeeds' ),
		'edit_item'           => __( 'Edit Vehicle', 'caorda-autofeeds' ),
		'update_item'         => __( 'Update Vehicle', 'caorda-autofeeds' ),
		'search_items'        => __( 'Search Vehicle', 'caorda-autofeeds' ),
		'not_found'           => __( 'Not found', 'caorda-autofeeds' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'caorda-autofeeds' ),
	);
	$args = array(
		'label'               => __( 'caf_featured_vehicle', 'caorda-autofeeds' ),
		'description'         => __( 'Remote Websites', 'caorda-autofeeds' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'revisions', 'thumbnail', 'custom-fields'),
		'taxonomies'          => array( 'caf_category', 'caf_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-performance',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'rewrite' => array( 'slug'=>'vehicles'),
	);
	register_post_type( 'caf_featured_vehicle', $args );

} // caf_featured_vehicle_post_type
function caf_inventory_post_type() {

	$labels = array(
		'name'                => _x( 'CAF Inventory', 'Post Type General Name', 'caorda-autofeeds' ),
		'singular_name'       => _x( 'Vehicle', 'Post Type Singular Name', 'caorda-autofeeds' ),
		'menu_name'           => __( 'CAF Inventory', 'caorda-autofeeds' ),
		'parent_item_colon'   => __( 'Parent Vehicle:', 'caorda-autofeeds' ),
		'all_items'           => __( 'All Vehicles', 'caorda-autofeeds' ),
		'view_item'           => __( 'View Vehicle', 'caorda-autofeeds' ),
		'add_new_item'        => __( 'Add New Vehicle', 'caorda-autofeeds' ),
		'add_new'             => __( 'Add New', 'caorda-autofeeds' ),
		'edit_item'           => __( 'Edit Vehicle', 'caorda-autofeeds' ),
		'update_item'         => __( 'Update Vehicle', 'caorda-autofeeds' ),
		'search_items'        => __( 'Search Vehicles', 'caorda-autofeeds' ),
		'not_found'           => __( 'Not found', 'caorda-autofeeds' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'caorda-autofeeds' ),
	);
	$args = array(
		'label'               => __( 'caf_inventory', 'caorda-autofeeds' ),
		'description'         => __( 'Premium Plugins', 'caorda-autofeeds' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'revisions', 'thumbnail', 'custom-fields'),
		'taxonomies'          => array( 'caf_category', 'caf_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-dashboard',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'rewrite' => array( 'slug'=>'inventory'),
	);
	register_post_type( 'caf_inventory', $args );

} // caf_featured_vehicle_post_type	

function tax_caf_category_init() {
	// create a new taxonomy
	register_taxonomy(
		'caf_category',
		array('caf_inventory', 'caf_featured_vehicle'),
		array(
			'label' => __( 'Categories' ),
			'rewrite' => array( 'slug' => 'caf-category' ),

			'labels' => array(
				'name'                       => _x( 'Categories', 'taxonomy general name' ),
				'singular_name'              => _x( 'Category', 'taxonomy singular name' ),
				'search_items'               => __( 'Search Categories' ),
				'popular_items'              => __( 'Popular Categories' ),
				'all_items'                  => __( 'All Categories' ),
				'parent_item'                => null,
				'parent_item_colon'          => null,
				'edit_item'                  => __( 'Edit Category' ),
				'update_item'                => __( 'Update Category' ),
				'add_new_item'               => __( 'Add New Category' ),
				'new_item_name'              => __( 'New Category Name' ),
				'separate_items_with_commas' => __( 'Separate Categories with commas' ),
				'add_or_remove_items'        => __( 'Add or remove Categories' ),
				'choose_from_most_used'      => __( 'Choose from the most used Categories' ),
				'not_found'                  => __( 'No Categories found.' ),
				'menu_name'                  => __( 'Categories' ),
			), // labels
			'show_ui' => true,
			'hierarchical' => true,
		)
	);
}


function tax_caf_tag_init() {
	// create a new taxonomy
	register_taxonomy(
		'caf_tag',
		array('caf_inventory', 'caf_featured_vehicle'),
		array(
			'label' => __( 'Tags' ),
			'rewrite' => array( 'slug' => 'caf-tag' ),

			'labels' => array(
				'name'                       => _x( 'Tags', 'taxonomy general name' ),
				'singular_name'              => _x( 'Tag', 'taxonomy singular name' ),
				'search_items'               => __( 'Search Tags' ),
				'popular_items'              => __( 'Popular Tags' ),
				'all_items'                  => __( 'All Tags' ),
				'parent_item'                => null,
				'parent_item_colon'          => null,
				'edit_item'                  => __( 'Edit Tag' ),
				'update_item'                => __( 'Update Tag' ),
				'add_new_item'               => __( 'Add New Tag' ),
				'new_item_name'              => __( 'New Tag Name' ),
				'separate_items_with_commas' => __( 'Separate Tags with commas' ),
				'add_or_remove_items'        => __( 'Add or remove Tags' ),
				'choose_from_most_used'      => __( 'Choose from the most used Tags' ),
				'not_found'                  => __( 'No Tags found.' ),
				'menu_name'                  => __( 'Tags' ),
			), // labels
			'show_ui' => true,
			'hierarchical' => false,
		) // args
	); // reg tax
}

function tax_caf_media_init() {
	// create a new taxonomy
	register_taxonomy(
		'caf_media_cat',
		array('attachment'),
		array(
			'label' => __( 'Media Category' ),
			'rewrite' => array( 'slug' => 'caf-media-cat' ),

			'labels' => array(
				'name'                       => _x( 'Categories', 'taxonomy general name' ),
				'singular_name'              => _x( 'Category', 'taxonomy singular name' ),
				'search_items'               => __( 'Search Categories' ),
				'popular_items'              => __( 'Popular Categories' ),
				'all_items'                  => __( 'All Categories' ),
				'parent_item'                => null,
				'parent_item_colon'          => null,
				'edit_item'                  => __( 'Edit Category' ),
				'update_item'                => __( 'Update Category' ),
				'add_new_item'               => __( 'Add New Category' ),
				'new_item_name'              => __( 'New Category Name' ),
				'separate_items_with_commas' => __( 'Separate Categories with commas' ),
				'add_or_remove_items'        => __( 'Add or remove Categories' ),
				'choose_from_most_used'      => __( 'Choose from the most used Categories' ),
				'not_found'                  => __( 'No Categories found.' ),
				'menu_name'                  => __( 'CAF Vehicle Images' ),
			), // labels
			'show_ui' => true,
			'hierarchical' => false,
		) // args
	); // reg tax
}