<?php
/**
*
* CaordaAutofeeds_Ajax.php
* Contains ajax calls and responses for various functions
*
**/

global $caf_import_status;
$caf_import_status = array(
	'current' => 'stopped',
	'states' => array(
		'stopped',
		'running',
		'process vehicles',
		'process images',
		'complete'
	),
	'time' => date('c'),
	'output' => null,
);


/*==================================================
=            AJAX Response Control Loop            =
==================================================*/

function caf_ajax_import_controller($cron=false, $status_override=false){
	global $caf_import_status;
	$CAF_Settings = get_option('CAF_Settings', array());
	$status = json_decode($CAF_Settings['opt-caf-api-import-progress'], true);
	$status = array_merge($caf_import_status, (array) $status);

	$stat_key = array_search($status['current'], $status['states']);

	if($status_override) $status['current'] = $status_override;


	$debug = isset($_REQUEST['debug']) ? true : false;

	// Clear previous output
	//$status['output'] = NULL;

	if($debug) echo '<pre>start: '.print_r($status, true).'</pre>';

	switch($status['current']){

		case 'stopped':
			$stat_key++;
			$status['current'] = $status['states'][$stat_key];
			break;

		case 'running':
			$stat_key++;
			$status['current'] = $status['states'][$stat_key];
			break;

		case 'process vehicles':

			$status['output'] = caf_import_vehicles_action($cron);

			if($cron){
				// If this is cron, we're done after processing vehicles
				$status['current'] = 'complete';
			} else {
				// If not cron (manual update), move on to image processing
				$stat_key++;
				$status['current'] = $status['states'][$stat_key];
			}
			break;

		case 'process images':

			$status = caf_handle_image_import($status, $cron);
			//$status['current'] = $img_status;
			break;

		case 'complete':
			$status['current'] = 'stopped';
			$status['output'] = null;	// clean out images data
			break;

		case 'busy':
			// Don't change status here - we'll update this from another fn
			break;

		default:
			break;
	} // switch

	$status['time'] = date('c');

	if($debug) echo '<pre>end: '.print_r($status, true).'</pre>';

	// Re-get CAF settings, they might have changed from within the import fn's
	// *grumble grumble PHP multi-thread applications*
	$CAF_Settings = get_option('CAF_Settings', array());
	$CAF_Settings['opt-caf-api-import-progress'] = json_encode( $status);
	update_option('CAF_Settings', $CAF_Settings);



	// End of life
	if(!$cron){
		// If we're from AJAX, return json
		echo json_encode( $status ); die();
	} else{
		// Otherwise this is a cron job, so report back
		return $status;
	}

} // caf_ajax_import_controller


/*===============================================
=            AJAX Response Functions            =
===============================================*/

// Resets import status
function caf_ajax_reset_import(){
	global $wp_query;

	if( isset($_REQUEST['reset']) ){

		$CAF_Settings = get_option('CAF_Settings', array() );
		$status['current'] = 'stopped';
		$CAF_Settings['opt-caf-api-import-progress'] = json_encode( $status);
		update_option('CAF_Settings', $CAF_Settings);
		echo json_encode(array('current'=>'resetting import state') );
		die();
	}
}


function caf_ajax_get_dealers($api_url = CAF_API_URL){

	$dealers = caf_get_dealers($force=true);
	echo json_encode( $dealers );
	die();
}


function caf_ajax_get_vehicles(){

	$vehicles = caf_get_vehicles();
	echo json_encode($vehicles);
	die();
}
function caf_ajax_repopulate_inventory(){

	$output = caf_repopulate_inventory();
	echo json_encode($output);
	die();
}

function caf_ajax_delete_images(){

	$images = caf_delete_images();
	echo json_encode($images);
	die();
}

function caf_ajax_purge_images(){

	$images = caf_purge_images();
	echo json_encode($images);
	die();
}

function caf_ajax_delete_inventory(){

	$vehicles = caf_delete_vehicles();
	echo json_encode($vehicles);
	die();
}

function caf_ajax_reload_data($data){

	if(!isset($_REQUEST['post_id'])){
		echo json_encode(array());
		die();
	}

	$vehicle = caf_reload_data($_REQUEST['post_id'], $data);
	echo json_encode( $vehicle);
	die();
}
/*=================================================
=            API Interaction Functions            =
=================================================*/

function caf_get_dealers($force=false, $api_url = CAF_API_URL){

	$dealers = CAF_API::get_dealers($force_update=$force);
	$dealers_clean = array();

	if( !empty($dealers)){
		foreach($dealers as $dealer){
			$dealers_clean[$dealer['DealerID']] = '['.$dealer['DealerID'].'] '.$dealer['DealerName'];
		}
		$output = $dealers_clean;
	}

	return $output;
}

function caf_get_vehicles($force=false, $api_url = CAF_API_URL){

	$vehicles = CAF_API::get_vehicles($force);

	return $vehicles;
}


/*=======================================
=            Import Images              =
=======================================*/
function caf_handle_image_import($status, $is_cron=false){
	global $count;
	$CAF_Settings = get_option('CAF_Settings', array() );
	$images = json_decode($CAF_Settings['opt-caf-api-img-import'], true );
	$img_import_output = null;

	if(empty($images)){
		$status['current'] = 'complete';
		return $status;
	}

	// quick exit
	if($status['current'] == 'busy') return $status;

	// Set up ACF field associations
	$acf_fields = array();
	foreach($CAF_Settings['opt-caf-acf-field-pairs'] as $pair){
		$pair = explode(':', $pair);
		$acf_fields[$pair[1]] = $pair[0];
		$acf_fields[$pair[0]] = $pair[1];
	}


	// Determine post to start importing at
	$start_key = isset($status['output']['start_key']) ? $status['output']['next_key'] : $images[0];

	// Set next key
	$next_int = array_search($start_key, $images) + 1;
	$next_key = isset($images[$next_int]) ? $images[$next_int] : $start_key;

	// If we've hit the end, nullify next_key
	if( $start_key == $next_key){
		$next_key = null;
	}

	$status['output']['start_key'] = $start_key;
	$status['output']['next_key'] = $next_key;

	// Begin import if start_key exists
	if( !empty( $start_key ) ){

		// We're going to start the bulk import here, so let's block the script from multiple instances
		//$status['current'] = 'busy';
		$CAF_Settings['opt-caf-api-import-progress'] = json_encode( $status);
		update_option('CAF_Settings', $CAF_Settings);

		$img_import_output = caf_import_images($start_key, $is_cron);

		// Handle errors
		if( !empty($img_import_output['images']->errors)){
			foreach($img_import_output['images']->errors as $error){
				WP_Logging::add( $title = 'CAF Image Import', 'Image Import error: '.$error[0], $parent = 0, $type = 'error' );
			}
		} else{

			// No errors - add to gallery!
			update_field( $acf_fields['caf_gallery'], $img_import_output['images'], $start_key);
			update_post_meta($start_key, '_thumbnail_id', $img_import_output['images'][0]);

			// Update image counts, init if needed
			if( !empty($status['output']['images']) ){
				$status['output']['images']['parsed'] ++; //= count($img_import_output['images']);
				$status['output']['images']['ignored'] = $img_import_output['images_ignored'];
				$status['output']['images']['added'] = $img_import_output['images_added'];
				$status['output']['images']['deleted'] = $img_import_output['images_deleted'];
			} else{
				$status['output']['images'] = array(
					'parsed'		=> 1,
					'count' 		=> 0,
					'ignored' 		=> $img_import_output['images_ignored'],
					'added' 		=> $img_import_output['images_added'],
					'deleted' 		=> $img_import_output['images_deleted'],
					'time_elapsed' 	=> $img_import_output['time_elapsed']
				);

				//foreach($images as $img) $status['output']['images']['count'] += count( $img['photos']);
				$status['output']['images']['count'] += count( $images);
			}

			unset($images[ array_search($start_key, $images) ]);
			$CAF_Settings['opt-caf-api-img-import'] = json_encode($images);
			update_option('CAF_Settings', $CAF_Settings);

		} // if no errors

	} // if start_key has images

	// Use next_key to determine whether to re-run or not
	if(!$next_key) $status['current'] =  'complete';
	else $status['current'] = 'process images';
	return $status;
}


/*=================================================
=            Repopulate Inventory Data            =
=================================================*/
function caf_repopulate_inventory(){

	// Clear out checksum fields, forcing a mismatch next time sync is run
	$vehicles = get_posts(array(
		'numberposts' => -1,
		'post_type' => 'caf_inventory',
		'post_status' => 'any'
	));

	foreach($vehicles as $v){
		//echo '<pre>test:'.print_r(caf_get_field('caf_checksum'), true).'</pre>';
		caf_update_field( 'caf_checksum', 'cleared', $v->ID);
	}

	$output = array('status'=>'success', 'message'=>'Successfully cleared keys for '.count($vehicles).' inventory items');
	WP_Logging::add( $title = 'CAF Data Repop', 'Clearing checksums for '.count($vehicles).' inventory items', $parent = 0, $type = 'event' );

	return $output;

} // caf_repopulate_inventory


/*===========================================
=            Reload Vehicle Data            =
===========================================*/
function caf_reload_data($post_id, $data){

	$updated = caf_vehicle_update_fields($post_id, $_POST, $update_title=TRUE);
	$image_out = caf_import_images($post_id, $is_cron=true);	// set is_cron to true to associate images with post
	$output_data = array($post_id, $updated, $image_out, $_POST);

	if($updated) $output = array('status'=>'success', 'message'=>'Successfully updated info for vehicle');
	else $output = array('status'=>'error', 'message'=>'Something went wrong during the info udpate.');

	$output['data'] = $output_data;

	return $output;
}




/*=============================================
=            Delete vehicle images            =
=============================================*/
function caf_delete_images($images=null){

	$output = array('status'=>'init', 'message'=>'Starting image delete', 'data'=>array('deleted'=>array(), 'error'=>array() ) );

	// If a list of images isn't provided, wipe the whole damn thing
	if(!$images){
		global $wpdb;
		$images = $wpdb->get_results("select p1.*
			FROM {$wpdb->posts} p1, {$wpdb->posts} p2
			WHERE p1.post_parent = p2.ID
			   AND p1.post_mime_type LIKE 'image%'
			   AND p2.post_type = 'caf_inventory'
			ORDER BY p2.post_date
			LIMIT 1000;"
		);
	}

	// Perform delete, track progress
	foreach($images as $image){
		$id = $image->ID;
		if( false === wp_delete_attachment($id) ) $output['data']['error'][] = $id;
		else $output['data']['deleted'][] = $id;
	}

	$output['status'] = 'success';
	$output['message'] = 'Success - deleted '.count($output['data']['deleted']).' images with '.count($output['data']['error']).' errors.';
	WP_Logging::add( $title = 'CAF Image Delete', 'Deleting images for posts: '.json_encode($output), $parent = 0, $type = 'event' );

	return $output;
}

// CAUTION: Deletes images like a boss
function caf_purge_images(){

	$output = array('status'=>'init', 'message'=>'Starting image purge', 'data'=>array('deleted'=>array(), 'error'=>array() ) );
	global $wpdb;

	$query = "DELETE p, pm FROM {$wpdb->posts} p
		LEFT JOIN {$wpdb->postmeta} pm ON pm.post_id = p.ID
		WHERE p.post_title REGEXP '^[0-9]{4,5}'
		AND p.post_type = 'attachment'
	";
	$results = $wpdb->query($wpdb->prepare($query));

	$output['status'] = 'success';
	$output['message'] = 'Success - purged images.';
	WP_Logging::add( $title = 'CAF Image Purge', 'Purging all vehicle images: '.json_encode($output).'<pre>'.print_r($results, true).'</pre>', $parent = 0, $type = 'event' );

	return $output;
}

/*=============================================
=           Delete vehicle inventory          =
=============================================*/
function caf_delete_vehicles($vehicles=null){

	$output = array('status'=>'init', 'message'=>'Starting image delete', 'data'=>array('deleted'=>array(), 'error'=>array() ) );

	// If a list of images isn't provided, wipe the whole damn thing
	if(!$vehicles){
		$vehicles = get_posts(array(
			'numberposts' => -1,
			'post_type' => 'caf_inventory',
			'post_status' => get_post_stati(),
		));
	}

	// Perform delete, track progress
	foreach($vehicles as $vehicle){
		$id = $vehicle->ID;
		if( false === wp_delete_post($id, true) ) $output['data']['error'][] = $id;
		else $output['data']['deleted'][] = $id;
	}

	$output['status'] = 'success';
	$output['message'] = 'Success - deleted '.count($output['data']['deleted']).' vehicles with '.count($output['data']['error']).' errors.';
	WP_Logging::add( $title = 'CAF Inventory Delete', 'Deleting Inventory: '.json_encode($output), $parent = 0, $type = 'event' );

	return $output;
}



/*====================================
=            Misc Utility            =
====================================*/

// Utility script for displaying buffered API data
function caf_get_vehicles_dump($charlimit = 0){
	$CAF_Settings = get_option('CAF_Settings', array());
	$file = untrailingslashit(ABSPATH).$CAF_Settings['opt-caf-api-inventory-data-location'];

	if( file_exists($file)){
		$dump = file_get_contents($file);
	}

	if( $charlimit){
		$dump = substr($dump, 0, $charlimit);
	}

	return $dump;
}

