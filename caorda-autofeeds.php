<?php
/*
   Plugin Name: Caorda AutoFeeds
   Plugin URI: http://caorda.com/
   Version: 1.2
   Author: Caorda Web Solutions
   Description: Provides integration with the Caorda AutoFeeds system
   Text Domain: caorda-autofeeds
   License: GPLv3
  */

$CaordaAutofeeds_minimalRequiredPhpVersion = '5.0';
define('CAF_API_URL', 'http://autofeeds.ca/bst');


/**
 * Check the PHP version and give a useful error message if the user's version is less than the required version
 * @return boolean true if version check passed. If false, triggers an error which WP will handle, by displaying
 * an error message on the Admin page
 */
function CaordaAutofeeds_noticePhpVersionWrong() {
    global $CaordaAutofeeds_minimalRequiredPhpVersion;
    echo '<div class="updated fade">' .
      __('Error: plugin "Caorda AutoFeeds" requires a newer version of PHP to be running.',  'caorda-autofeeds').
            '<br/>' . __('Minimal version of PHP required: ', 'caorda-autofeeds') . '<strong>' . $CaordaAutofeeds_minimalRequiredPhpVersion . '</strong>' .
            '<br/>' . __('Your server\'s PHP version: ', 'caorda-autofeeds') . '<strong>' . phpversion() . '</strong>' .
         '</div>';
}


function CaordaAutofeeds_PhpVersionCheck() {
    global $CaordaAutofeeds_minimalRequiredPhpVersion;
    if (version_compare(phpversion(), $CaordaAutofeeds_minimalRequiredPhpVersion) < 0) {
        add_action('admin_notices', 'CaordaAutofeeds_noticePhpVersionWrong');
        return false;
    }
    return true;
}


/**
 * Initialize internationalization (i18n) for this plugin.
 * References:
 *      http://codex.wordpress.org/I18n_for_WordPress_Developers
 *      http://www.wdmac.com/how-to-create-a-po-language-translation#more-631
 * @return void
 */
function CaordaAutofeeds_i18n_init() {
    $pluginDir = dirname(plugin_basename(__FILE__));
    load_plugin_textdomain('caorda-autofeeds', false, $pluginDir . '/languages/');
}


//////////////////////////////////
// Run initialization
/////////////////////////////////

// First initialize i18n
CaordaAutofeeds_i18n_init();


// Next, run the version check.
// If it is successful, continue with initialization for this plugin
if (CaordaAutofeeds_PhpVersionCheck()) {
    // Only load and run the init function if we know PHP version can parse it

  if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/ReduxFramework/ReduxCore/framework.php' ) ) {
    require_once( dirname( __FILE__ ) . '/ReduxFramework/ReduxCore/framework.php' );
  }
  if ( file_exists( dirname( __FILE__ ) . '/CaordaAutofeeds_ReduxInit.php' ) ) {
      require_once( dirname( __FILE__ ) . '/CaordaAutofeeds_ReduxInit.php' );
  }


    include_once('caorda-autofeeds_init.php');
    CaordaAutofeeds_init(__FILE__);
}

